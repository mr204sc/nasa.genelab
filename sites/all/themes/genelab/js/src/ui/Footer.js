function Footer(node) {

	var _instance = {};

	var _btn;

	function init() {
		addListeners();
	}

	function addListeners() {
		var backToTopContainer = node.querySelector('.back-to-top-container');
		_btn = backToTopContainer.querySelector('a');
		_btn.addEventListener('click', backToTopClick);
	}

	function removeListeners() {
		_btn.removeEventListener('click', backToTopClick);
	}

	function backToTopClick(e) {
		e.preventDefault();

		WindowScroll.scroll({
			y: 0
		});
	}

	_instance.kill = function() {
		removeListeners();
	};

	init();

	return _instance;

}