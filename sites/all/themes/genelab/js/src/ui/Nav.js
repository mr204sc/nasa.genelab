function Nav(node) {
/*
	var _instance = {};

	var _navContainer;
	var _mobileButton
	var _mobileButtonAnimation;
	var _isOpen = false;

	function init() {

		_mobileButton = node.querySelector('.mobile-nav-button');
		_navContainer = new SmartObject(node.querySelector('.nav-container')); 

		_mobileButtonAnimation = new SpriteAnimation(_mobileButton, [
			{x: 0, y: 0},
			{x: 0, y: -71},
			{x: 0, y: -142},
			{x: 0, y: -213},
			{x: 0, y: -284},
			{x: 0, y: -355},
			{x: 0, y: -426},
			{x: 0, y: -497},
			{x: 0, y: -568},
			{x: 0, y: -639},
			{x: 0, y: -710},
			{x: 0, y: -781},
			{x: 0, y: -852}
		], {
			speed: 60,
			loop: false
		});

		addListeners();
		resize();
	}

	function addListeners() {
		
		window.addEventListener('resize', resize);
		_mobileButton.addEventListener('click', mobileClick);
	}

	function mobileClick(e) {
		toggleMobileNav();
	}

	function toggleMobileNav() {
		if (_isOpen == false) {

			_isOpen = true;

			if (BrowserDetect.MOBILE) {
				_navContainer.style.height = window.innerHeight - 70 + 'px';
			}

			// window.scroll = function(e) {
			// 	e.preventDefault();
			// }

			_navContainer.style.display = 'block';
			//_mobileButton.classList.add('open');

			_mobileButtonAnimation.play();

			var h = _navContainer.clientHeight;

			_navContainer._y = -h + 70;

			TweenMax.to(_navContainer, .3, {
				_y: 70,
				ease: Quad.easeOut
			});

		} else {
			//_mobileButton.classList.remove('open');

			_isOpen = false;

			_mobileButtonAnimation.reverse();

			var h = _navContainer.clientHeight;
			TweenMax.to(_navContainer, .3, {
				_y: -h + 70,
				ease: Quad.easeIn,
				onComplete: function() {
					_navContainer.style.display = 'none';
				}
			});
		}


	}

	function resize(e) {
		if (window.innerWidth <= 940) {
			// _mobileButton.classList.remove('open');

			_mobileButtonAnimation.reset();

			_navContainer.style.display = 'none';
		} else {
			_navContainer.style.display = 'block';
		}
	}

	_instance.update = function(num) {
		
		var buttons = _navContainer.querySelectorAll('li');
		var path = window.location.pathname;

		_navContainer.querySelector('.selected-button').classList.remove('selected-button');
		buttons[num].children[0].classList.add('selected-button');

	};

	_instance.close = function() {
		if (_isOpen) {
			toggleMobileNav();
		}
	};

	_instance.getWidth = function() {
		var w = 184;

		if (window.innerWidth >= 1600) {
			w = 250;
		}
		if (window.innerWidth <= 940) {
			w = 0;
		}

		return w;
	};

	_instance.getElement = function() {
		return node;
	};

	init();

	return _instance;
	*/
}