function AboutColumns(node) {

	var _instance = {};

	var _columns;

	function init() {
		_columns = node.querySelectorAll('.empty-column');

		addListeners();

		resize();
	}

	function addListeners() {
		window.addEventListener('resize', resize);
	}

	function removeListeners() {
		window.removeEventListener('resize', resize);
	}

	function resize(e) {
		for (var j = 0; j < _columns.length; j++) {

			var col = _columns[j];
			var colSibling = col.parentNode.querySelector('.empty-column-match');

			var h = colSibling.clientHeight;
			if (col.classList.contains('empty-column-minus')) {
				h -= 42;
			}
			col.style.height = h + 'px';			

		}
	}

	_instance.kill = function() {
		removeListeners();
		
		_columns = null;
	};

	init();

	return _instance;

}