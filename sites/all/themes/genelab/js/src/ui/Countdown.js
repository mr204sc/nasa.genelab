function Countdown(node) {

	var _instance = {};

	var LIGHT_COLOR = '#909ea4';
	var DARK_COLOR = '#839198';

	var _lines;
	var _timer;
	var _currentLine = 0;
	var _daysText;
	var _hoursText;
	var _minutesText;
	var _secondsText;

	var _topContainer;
	var _topContainerCols;
	
	function init() {

		_topContainer = node.querySelector('.top-container');
		_topContainerCols = _topContainer.querySelectorAll('.col-1');

		buildLines();
		resize();
		setTextFields();
		reset();
		startTimer();
		timerTick();

		addListeners();

	}

	function addListeners() {
		window.addEventListener('resize', resize);
	}

	function removeListeners() {
		window.removeEventListener('resize', resize);
	}

	function buildLines() {
		var lineContainer = node.querySelector('.line-container');
		_lines = [];
		
		for (var j = 0; j <= 59; j++) {
			var line = document.createElement('div');
			line.style.position = 'absolute';
			line.style.width = '100%';
			line.style.height = 1 + 'px';
			line.style.top = (j * 2) + 1 + 'px';
			lineContainer.appendChild(line);

			_lines.push(line);
		}
	}

	function setTextFields() {
		_daysText = node.querySelector('.days-text');
		_hoursText = node.querySelector('.hours-text');
		_minutesText = node.querySelector('.minutes-text');
		_secondsText = node.querySelector('.seconds-text');
	}

	function startTimer() {
		_timer = setInterval(timerTick, 1000);
	}

	function stopTimer() {
		if (_timer) {
			clearInterval(_timer);
			_timer = null;
		}
	}

	function timerTick() {
		
		if (_currentLine >= _lines.length) {
			reset();
			updateText();
		}

		_lines[_currentLine].style.backgroundColor = LIGHT_COLOR

		var secondsStr = 59 - _currentLine;
		_secondsText.innerHTML = (secondsStr < 10) ? '0' + secondsStr : secondsStr;
		
		_currentLine++;

	}

	function reset() {
		_currentLine = 0;
		for (var j = 0; j < _lines.length; j++) {
			_lines[j].style.backgroundColor = DARK_COLOR
		}
	}

	function updateText() {
		var minutes = Number(_minutesText.innerHTML);
		var hours = Number(_hoursText.innerHTML);
		var days = Number(_daysText.innerHTML);

		minutes -= 1;

		if (minutes < 0) {
			_minutesText.innerHTML = '59';

			hours -= 1;

			if (hours < 0) {
				_hoursText.innerHTML = '23';

				days -= 1;

				if (days < 0) {
					stopTimer();
				} else {
					if (days < 10) {
						_daysText.innerHTML = '0' + days;
					} else {
						_daysText.innerHTML = days;
					}
				}

			} else {
				if (hours < 10) {
					_hoursText.innerHTML = '0' + hours;
				} else {
					_hoursText.innerHTML = hours;	
				}
			}

		} else {
			if (minutes < 10) {
				_minutesText.innerHTML = '0' + minutes;
			} else {
				_minutesText.innerHTML = minutes;	
			}
			
		}
	}

	function resize(e) {
		
		if (window.innerWidth <= 760) {
		
			for (var j = 0; j < _lines.length; j++) {
				_lines[j].style.top = (j * 3) + 2 + 'px';
				_lines[j].style.height = 1 + 'px';
			}

			var colSize = window.innerWidth - 40 - 36;

			if (BrowserDetect.BROWSER_NAME == 'Explorer') {
				colSize -= 20;
			}

			for (var j = 0; j < _topContainerCols.length; j++) {
				if (j > 1) {
					_topContainerCols[j].style.width = colSize / 4 + 'px';
				}
			}

		} else {

			for (var j = 0; j < _lines.length; j++) {
				_lines[j].style.top = (j * 2) + 1 + 'px';
				_lines[j].style.height = 1 + 'px';
			}

		}

	}

	_instance.kill = function() {
		stopTimer();
		removeListeners();

		_lines = null;
	};

	init();

	return _instance;

}