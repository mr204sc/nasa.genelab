function Events(node) {

	var _instance = {};

	var _selectedFilter = 0;
	var _buttons;
	var _bgHighlight;
	var _sectionTitles;
	var _isAnimatingScroll = false;


	function init() {
		setPastEventFilters();
		addFilterListeners();
		addListeners();
	}

	function setPastEventFilters() {

		// update the overlay opacity for browsers that dont support greyscale
		if (BrowserDetect.BROWSER_NAME == 'Firefox' || BrowserDetect.BROWSER_NAME == 'Explorer') {
			var filterDivs = document.querySelectorAll('.image-filter');
			for (var j = 0; j < filterDivs.length; j++) {
				var img = filterDivs[j].querySelector('.overlay-filter');
				img.style.opacity = .45;
			}
		}

	}

	function addFilterListeners() {	
		_buttons = document.querySelector('.event-filter-options').children;
		_bgHighlight = document.querySelector('.bg-highlight');
		_sectionTitles = document.querySelectorAll('.section-title-block');

		var filterButtonClick = function(e) {
			e.preventDefault();

			updateFilters(e.target);

			// update the displayed content or scroll the page down

			// scroll to the correct section
			_isAnimatingScroll = true;

			var destinY = _sectionTitles[_selectedFilter].offsetTop + 1 - 107; // 107 is the header height
			WindowScroll.scroll({
				y: destinY,
				onComplete: function() {
					_isAnimatingScroll = false;
				}
			});

		};

		for (var j = 0; j < _buttons.length - 1; j++) {
			var btn = _buttons[j];
			btn.num = j;
			btn.addEventListener('click', filterButtonClick);
		}
	
		
	}

	function updateFilters(btn) {
		for (var j = 0; j < _buttons.length - 1; j++) {
			_buttons[j].classList.remove('selected');
		}

		btn.classList.add('selected');

		_selectedFilter = btn.num;

		var destinX = (_selectedFilter === 0) ? 3 : (_selectedFilter * btn.clientWidth);
		_bgHighlight.style.left = destinX + 'px';
	}

	function addListeners() {
		window.addEventListener('resize', resize);
		window.addEventListener('scroll', scroll);
	}

	function removeListeners() {
		window.removeEventListener('resize', resize);
		window.removeEventListener('scroll', scroll);
	}

	function scroll(e) {
		if (_isAnimatingScroll) {
			return;
		}

		var scrollPos = WindowScroll.getPositionY();
		var hasFoundSection = false;
		for (var j = 0; j < _sectionTitles.length; j++) {

			if (scrollPos >= _sectionTitles[j].offsetTop - 107) {
				hasFoundSection = true;
			} else {
				break;
			}

		}

		j -= 1;
		
		if (hasFoundSection && j != _selectedFilter) {
			updateFilters(_buttons[j]);
		}
	}

	function resize(e) {
		if (_bgHighlight) {
			var destinX = (_selectedFilter === 0) ? 3 : (_selectedFilter * _buttons[_selectedFilter].clientWidth);
			_bgHighlight.style.left = destinX + 'px';
		}
	}

	_instance.kill = function() {
		removeListeners();
	}

	init();

	return _instance;

}