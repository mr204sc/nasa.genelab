function ISSModule(node) {

	var _instance = {};

	var _lensContainer;
	var _flares = [
		{
			src: './assets/images/home/flare-1.png',
			width: 496,
			height: 503,
			origX: 20,
			origY: 0,
			xRange: 10,
			yRange: 10,
			opacity: 1,
			element: null
		},
		{
			src: './assets/images/home/flare-2.png',
			width: 196,
			height: 197,
			origX: 180,
			origY: 180,
			xRange: 100,
			yRange: 100,
			opacity: 1,
			element: null
		},
		{
			src: './assets/images/home/flare-3.png',
			width: 295,
			height: 295,
			origX: 50,
			origY: 0,
			xRange: 150,
			yRange: 100,
			opacity: 1,
			element: null
		},
		// {
		// 	src: './assets/images/home/flare-4.png',
		// 	width: 324,
		// 	height: 324,
		// 	origX: 120,
		// 	origY: 60,
		// 	xRange: 250,
		// 	yRange: 100,
		// 	opacity: 1,
		// 	element: null
		// },
		{
			src: './assets/images/home/flare-5.png',
			width: 283,
			height: 282,
			origX: 170,
			origY: 120,
			xRange: 150,
			yRange: 180,
			opacity: 1,
			element: null
		},
		{
			src: './assets/images/home/flare-6.png',
			width: 235,
			height: 235,
			origX: 200,
			origY: 130,
			xRange: 100,
			yRange: 150,
			opacity: 1,
			element: null
		},
		{
			src: './assets/images/home/flare-7.png',
			width: 207,
			height: 207,
			origX: 270,
			origY: 150,
			xRange: 100,
			yRange: 180,
			opacity: 1,
			element: null
		},
		{
			src: './assets/images/home/flare-8.png',
			width: 196,
			height: 196,
			origX: 260,
			origY: 160,
			xRange: 280,
			yRange: 200,
			opacity: 1,
			element: null
		}
	];

	var _hasTimerStarted = false;
	var _countBlocks;
	var _animatingCountBlocks;
	var _ticker;

	function init() {
		// 530 x 560

		if (BrowserDetect.MOBILE) {
			return;
		}

		buildLensContainer();
		buildFlares();
		setupCountBlocks();

		addListeners();

		scroll();
	}

	function buildLensContainer() {
		_lensContainer = document.createElement('div');
		_lensContainer.classList.add('lens-container');

		_countBlocks = node.querySelectorAll('.count-up');

		_animatingCountBlocks = [];

		node.appendChild(_lensContainer);
	}

	function buildFlares() {
		for (var j = 0; j < _flares.length; j++) {
			var img = new SmartObject(document.createElement('img'));
			img.width = _flares[j].width;
			img.height = _flares[j].height;
			img._x = _flares[j].origX;
			img._y = _flares[j].origY;
			// img._opacity = _flares[j].opacity;
			img.src = _flares[j].src;
			_lensContainer.appendChild(img);

			_flares[j].element = img;
		}
	}

	function addListeners() {
		window.addEventListener('scroll', scroll);
	}

	function removeListeners() {
		window.removeEventListener('scroll', scroll);
	}

	function setupCountBlocks() {
		for (var j = 0; j < _countBlocks.length; j++) {
			var elem = _countBlocks[j];
			elem.finalValue = elem.getAttribute('data-final-value');
			elem.currentValue = 0;
			elem.isComplete = false;
			elem.hasPlayed = false;
			elem.innerHTML = '0' + ' ' + elem.getAttribute('data-text');
		}
	}

	function startInterval() {
		
		_ticker = setInterval(function() {
			
			var completeCounter = 0;
			for (var j = 0; j < _animatingCountBlocks.length; j++) {
				var elem = _animatingCountBlocks[j];
				
				if (!elem.isComplete) {
					var amount = Math.ceil(Math.random() * parseInt(elem.getAttribute('data-range')));
					var oldValue = elem.currentValue;
					
					if (amount + oldValue < elem.finalValue) {
						elem.currentValue = amount + oldValue;
						elem.innerHTML = numberWithCommas(elem.currentValue) + ' ' + elem.getAttribute('data-text');
					} else {
						elem.innerHTML = numberWithCommas(elem.finalValue) + ' ' + elem.getAttribute('data-text');
						elem.isComplete = true;
					}
				} else {
					completeCounter++;
				}

				if (completeCounter == _countBlocks.length ) {
					
					clearInterval(_ticker);
					_ticker = null;
				}
				
			}
		}, 30);
	}


	/*
		TODO: move this to a util function
	*/
	function numberWithCommas(x) {
    	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	function scroll(e) {
		
		// var parentPosY = getPosition(node); //.offsetTop;
		var containerYPos = getPosition(_lensContainer); //node.offsetTop + _lensContainer.offsetTop;
		var scrollPos = WindowScroll.getPositionY();
		//console.log(scrollPos);

		var scrollBottom = scrollPos + window.innerHeight;
		if (scrollBottom > containerYPos) {

			if (!_hasTimerStarted) {
				_hasTimerStarted = true;
				startInterval();
			}

			// container is in view
			var startY = containerYPos;
			var endY = scrollBottom + window.innerHeight + _lensContainer.clientHeight;
			var innerPos = scrollBottom - startY;
			var range = endY - scrollBottom;

			var perc = innerPos / range;

			if (perc > 1) {
				perc = 1;
			}

			for (var j = 0; j < _flares.length; j++) {
				var img = _flares[j].element;
				img._x = _flares[j].origX + (_flares[j].xRange * perc);
				img._y = _flares[j].origY + (_flares[j].yRange * perc);
				// img._opacity = _flares[j].opacity + (perc / 2);
			}

		}

		// check if any of the count blocks are in view
		for (var j = 0; j < _countBlocks.length; j++) {
			var elem = _countBlocks[j];
			var blockY = getPosition(elem);
			var scrollBottom = scrollPos + window.innerHeight - 100;

			if (scrollBottom > blockY && !elem.hasPlayed) {
				elem.hasPlayed = true;
				_animatingCountBlocks.push(elem);
			}
		}

	}

	/*
		TODO: move this to a util function
	*/
	function getPosition(element) {
	    var yPosition = 0;
	  
	    while(element) {
	        yPosition += element.offsetTop;
	        element = element.offsetParent;
	    }

	    return yPosition;
	}

	_instance.kill = function() {
		removeListeners();

		if (_ticker) {
			clearInterval(_ticker);
			_ticker = null;
		}
	};

	init();

	return _instance;

}