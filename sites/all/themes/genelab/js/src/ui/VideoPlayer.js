function VideoPlayer(callbacks) {

	var _instance = {};

	var _video;

	function init() {

		_video = document.createElement('video');

		addListeners();

	}

	function addListeners() {
		_video.addEventListener('loadedmetadata', onLoadedMetaData);
		_video.addEventListener('ended', onEnded);
		_video.addEventListener('pause', onPause);
		_video.addEventListener('play', onPlay);
	}

	function removeListeners() {
		_video.addEventListener('loadedmetadata', onLoadedMetaData);
		_video.addEventListener('ended', onEnded);
		_video.addEventListener('pause', onPause);
		_video.addEventListener('play', onPlay);
	}

	function onLoadedMetaData(e) {
		if (callbacks.onLoadedMetaData) {
			callbacks.onLoadedMetaData(e);
		}
	}

	function onEnded(e) {
		if (callbacks.onEnded) {
			callbacks.onEnded(e);
		}
	}

	function onPause(e) {
		if (callbacks.onPause) {
			callbacks.onPause(e);
		}
	}

	function onPlay(e) {
		if (callbacks.onPlay) {
			callbacks.onPlay(e);
		}
	}

	_instance.kill = function() {
		removeListeners();
	}

	init();

	_instance.video = _video;

	return _instance;
}

VideoPlayer.supportsFullscreen = function() {

	var isSupported = false;
	var v = document.createElement('video');

	if (v.requestFullscreen) {
		isSupported = true;
	} else if (v.msRequestFullscreen) {
		isSupported = true;
	} else if (v.mozRequestFullScreen) {
		isSupported = true;
	} else if (v.webkitRequestFullscreen) {
		isSupported = true;
	}

	if (!BrowserDetect.DESKTOP) {
		isSupported = false;
	}

	return isSupported;
}

VideoPlayer.supportsMP4 = function() {
	var isSupported = false;
	var v = document.createElement('video');
	var mpeg4;
	var h264;

	if (v.canPlayType) {
		// Check for MPEG-4 support
		mpeg4 = "" !== v.canPlayType('video/mp4; codecs="mp4v.20.8"');

		// Check for h264 support
		h264 = "" !== ( v.canPlayType('video/mp4; codecs="avc1.42E01E"') || v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"'));
	}

	if (mpeg4 || h264) {
		isSupported = true;
	}

	if (BrowserDetect.BROWSER_NAME.toLowerCase() == 'explorer') {
		isSupported = false;
	}

	return isSupported;
};

VideoPlayer.supportsOGG = function() {
	/**
		temporarily shut off ogg support

	*/
	return false;
	
	var isSupported = false;
	var v = document.createElement('video');
	var ogg;

	if (v.canPlayType) {
		ogg = "" !== v.canPlayType('video/ogg; codecs="theora, vorbis"');
	}

	if (ogg) {
		isSupported = true;
	}

	if (BrowserDetect.BROWSER_NAME.toLowerCase() == 'explorer') {
		isSupported = false;
	}
	
	
	return isSupported;
};

