function HomeHeader(node, hasIntroPlayed) {

	var _instance = {};

	var _signupInput;
	var _videoButton;
	var _videoContainer;
	var _videoplayer;
	var _videoControls;
	var _closeButton;
	var _imageSequence;
	var _cols;
	var _starsContainer;
	var _starsContainer2;

	function init() {
		if (BrowserDetect.TABLET) {
            hasIntroPlayed = true;
        }

		_cols = node.querySelectorAll('.col, .bottom-container');
		_signupInput = node.querySelector('[type="text"]');
		_videoButton = node.querySelector('.watch-story-button');
		_starsContainer = node.querySelector('.stars-container');
		_starsContainer2 = new SmartObject(node.querySelector('.stars-container-2'));
		
		_starsContainer2.style.position = 'fixed';

		buildImageSequence();
		animateInCols();

		resize();
		addListeners();

		if (VideoPlayer.supportsMP4()) {
			_videoButton.style.display = 'block';
		}

		if (!hasIntroPlayed) {
			//animateNav();
		}
		
	}

	// function animateNav() {
	// 	var leftColumn = document.querySelector('.site-column-left');
	// 	leftColumn.style.backgroundColor = '#080B10';

	// 	var navContainer = window.nav.getElement();//document.querySelector('.nav');

	// 	navContainer.style.width = 368 + 'px';
	// 	navContainer.style.marginLeft = -368 + 'px';

	// 	TweenLite.to(navContainer, .6, {
	// 		delay: .5,
	// 		marginLeft: -184,
	// 		ease: Quad.easeOut
	// 	});

	// 	TweenLite.to(navContainer, .6, {
	// 		delay: .8,
	// 		marginLeft: 0,
	// 		width: 184,
	// 		ease: Quad.easeOut
	// 	});
	// }

	function addListeners() {
		window.addEventListener('resize', resize);
		window.addEventListener('scroll', scroll);
		node.querySelector('.down-arrow').addEventListener('click', scrollDown);

		_signupInput.addEventListener('focus', signupFocus);
		_signupInput.addEventListener('blur', signupBlur);

		_videoButton.addEventListener('click', videoButtonClick);
	}

	function buildImageSequence() {
		_imageSequence = new HomeHeaderSequence(node, hasIntroPlayed);
		node.appendChild(_imageSequence);
	}

	function animateInCols() {
		var del = (hasIntroPlayed) ? 1 : 1;//5.7;
		for (var j = 0; j < _cols.length; j++) {

			if (BrowserDetect.MOBILE) { 
				_cols[j].style.opacity = 1;
			} else {
				_cols[j].style.opacity = 0;

				TweenLite.to(_cols[j], 1.3, {
					delay: del + (j * .2),
					css: {
						opacity: 1
					},
					ease: Linear.easeNone
				});
			}
		}
	}

	function signupFocus(e) {
		if (window.innerWidth > 1230) {
			TweenLite.to(_signupInput, .3, {
				css: {
					width: '215%'
				},
				ease: Quad.easeInOut
			});
		}
	}

	function signupBlur(e) {
		TweenLite.to(_signupInput, .3, {
			css: {
				width: '100%'
			},
			ease: Quad.easeInOut
		});
	}

	function removeListeners() {
		window.removeEventListener('resize', resize);
		window.removeEventListener('scroll', scroll);
		node.querySelector('.down-arrow').removeEventListener('click', scrollDown);

		_signupInput.removeEventListener('focus', signupFocus);
		_signupInput.removeEventListener('blur', signupBlur);

		_videoButton.removeEventListener('click', videoButtonClick);
	}

	function videoButtonClick(e) {

		if (BrowserDetect.MOBILE) {

			window.open('assets/videos/genelab.mp4?123', '_blank');

		} else {
			// create a black overlay
			_videoContainer = document.createElement('div');
			_videoContainer.classList.add('video-container');

			node.appendChild(_videoContainer);

			_videoContainer.style.opacity = 0;
			TweenLite.to(_videoContainer, .4, {
				css: {
					opacity: 1
				},
				ease: Linear.easeNone,
				onComplete: buildVideo
			});
		}
	}

	function buildVideo() {
		_closeButton = document.createElement('div');
		_closeButton.classList.add('close-button');
		_videoContainer.appendChild(_closeButton);

		_closeButton.addEventListener('click', closeVideo);


		_videoplayer = new VideoPlayer({
			onLoadedMetaData: videoMetaDataLoaded
		});
		_videoplayer.video.src = './assets/videos/genelab.mp4?123';
		_videoplayer.video.style.width = '100%';
		_videoplayer.video.style.height = 'auto';
		// _videoplayer.video.muted = true;
		_videoContainer.appendChild(_videoplayer.video);

	
		_videoplayer.video.style.opacity = 0;

		TweenLite.to(_videoplayer.video, .8, {
			css: {
				opacity: 1
			},
			ease: Linear.easeNone,
		});
		
		_videoControls = new VideoPlayerControls(_videoplayer, showControls, hideControls);
		_videoContainer.appendChild(_videoControls);
	}

	function showControls() {
		_closeButton.style.display = 'block';
		_closeButton.style.opacity = 0;

		TweenLite.to(_closeButton, .2, {
			css: {
				opacity: 1
			},
			ease: Linear.easeNone
		});
	}

	function hideControls() {
		TweenLite.to(_closeButton, .2, {
			css: {
				opacity: 0
			},
			ease: Linear.easeNone,
			onComplete: function() {
				_closeButton.style.display = 'none';
			}
		});	
	}

	function closeVideo() {
		// kill all listeners
		_videoControls.kill();
		_videoplayer.kill();

		// hide controls
		_videoControls.hide();

		// fade out video
		TweenLite.to(_videoplayer.video, .2, {
			css: {
				opacity: 0
			},
			ease: Linear.easeNone
		});

		// fade out videoContainer
		TweenLite.to(_videoContainer, .2, {
			delay: .2,
			css: {
				opacity: 0
			},
			ease: Linear.easeNone,
			onComplete: function() {
				node.removeChild(_videoContainer);
				_videoContainer = null;
				_videoplayer = null;
				_videoControls = null;
			}
		});
		
	}

	function videoMetaDataLoaded(e) {
		resize();

		if (BrowserDetect.DESKTOP) {
			_videoplayer.video.play();	
		}
	}

	function scrollDown(e) {
		var destinY = node.clientHeight
		WindowScroll.scroll({
			y: destinY
		});
	}

	function resize(e) {
		if (window.innerWidth <= 940) {
			node.style.height = window.innerHeight - 70 + 'px';
		} else {
			node.style.height = window.innerHeight + 'px';
		}

		if (_videoplayer) {
			_videoplayer.video.style.width = '100%';
			_videoplayer.video.style.height = 'auto';

			_videoplayer.video.style.marginTop = (_videoplayer.video.parentNode.clientHeight - _videoplayer.video.clientHeight) / 2 + 'px';
			_videoplayer.video.style.marginLeft = 0;

			if (_videoplayer.video.clientHeight < _videoplayer.video.parentNode.clientHeight) {
				_videoplayer.video.style.width = 'auto';
				_videoplayer.video.style.height = '100%';

				_videoplayer.video.style.marginTop = 0 + 'px';
				_videoplayer.video.style.marginLeft = (_videoplayer.video.parentNode.clientWidth - _videoplayer.video.clientWidth) / 2 + 'px';
			}
			// var videoSpace = _videoplayer.video.parentNode.clientHeight - _videoplayer.video.clientHeight;
			// if (BrowserDetect.TABLET) {
			// 	videoSpace -= 54;
			// }
			// _videoplayer.video.style.marginTop = videoSpace / 2 + 'px';	
		}
	}

	function scroll(e) {
		var scrollPos = WindowScroll.getPositionY();

		if (scrollPos < node.clientHeight) {
			var perc = scrollPos / node.clientHeight;
			var pos = Math.round(perc * 100);
			_starsContainer.style.backgroundPositionY = -pos + 'px';


			pos = Math.round(perc * 150);
			//_starsContainer2.style.backgroundPositionY = -pos + 'px';
			_starsContainer2._y = -pos;
		}
	}

	_instance.kill = function() {
		removeListeners();

		if (_imageSequence) {
			_imageSequence .kill();
			_imageSequence = null;
		}

		if (_videoControls) {
			_videoControls.kill();
			_videoControls = null;
		}
	}

	init();

	return _instance;

}