function AboutHeader(node) {

	var _instance = {};

	var _img;
	var _videoplayer;

	function init() {
		
		_img = node.querySelector('img');

		if (BrowserDetect.DESKTOP && (VideoPlayer.supportsMP4() || VideoPlayer.supportsOGG())) {
			buildVideo();
		}

		addListeners();

		resize();
	}

	function addListeners() {
		window.addEventListener('resize', resize);
		window.addEventListener('scroll', scroll);
	}

	function removeListeners() {
		window.removeEventListener('resize', resize);
		window.removeEventListener('scroll', scroll);
	}

	function buildVideo() {
		var videoPath = '';

		if (VideoPlayer.supportsMP4()) {
			videoPath = 'assets/videos/about.mp4?123';
		} else if (VideoPlayer.supportsOGG()) {
			videoPath = 'assets/videos/about.ogv';
		}

		_videoplayer = new VideoPlayer({
			onLoadedMetaData: videoMetaDataLoaded
		});
		_videoplayer.video.src = videoPath;
		_videoplayer.video.loop = true;
		_videoplayer.video.style.opacity = 0;
		node.appendChild(_videoplayer.video);
	}

	function videoMetaDataLoaded() {
		resize();
		_videoplayer.video.play();

		TweenLite.to(_videoplayer.video, .5, {
			css: {
				opacity: 1
			},
			ease: Linear.easeNone
		});
	}

	function resize(e) {
		var h = (window.innerHeight < 514) ? 514 : window.innerHeight;

		if (h > 970) {
			h = 970;
		}

		node.style.height = h + 'px';


		_img.style.width = '100%';
		_img.style.height = 'auto';
		
		if (_img.clientHeight == 0) {
			setTimeout(resize, 50);
		} else {

			if (_img.clientHeight < h) {
				_img.style.height = '100%';
				_img.style.width = 'auto';
			}
		}

		if (_videoplayer) {
			_videoplayer.video.style.width = _img.style.width;
			_videoplayer.video.style.height = _img.style.height;
		}
		
	}

	function scroll(e) {
		var scrollPos = WindowScroll.getPositionY();

		if (scrollPos < node.clientHeight) {
			var perc = scrollPos / node.clientHeight;
			var pos = Math.round(perc * 300);
			if (_videoplayer) {
				_videoplayer.video.style.top = -pos + 'px';
			} else {
				_img.style.top = -pos + 'px';	
			}
		}
	}

	_instance.kill = function() {
		removeListeners();

		_img = null;

		if (_videoplayer) {
			_videoplayer.video.pause();
			_videoplayer.kill();
			_videoplayer = null;
		}
	}

	init();

	return _instance;

}