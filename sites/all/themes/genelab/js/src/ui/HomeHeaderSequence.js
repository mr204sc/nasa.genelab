function HomeHeaderSequence(parentNode, hasIntroPlayed) {
	
	var _instance = new SmartObject(document.createElement('div'));

	var WIDTH = 649;
	var HEIGHT = 628;
	var FIRST_IMAGE_PATH = 'assets/images/home/header-earth-1.png';
	var SEQUENCE_IMAGE_PATHS = [
		'assets/images/home/header-earth-2.png',
		'assets/images/home/header-earth-3.png',
		'assets/images/home/header-earth-4.png',
		'assets/images/home/header-earth-5.png',
		'assets/images/home/header-earth-6.png',
		'assets/images/home/header-earth-7.png',
		'assets/images/home/header-earth-8.png'
	];

	var _firstImage;
	var _starsBg;
	var _starsBg2;
	var _sequenceLoadCounter = 0;
	var _sequenceAnimationCounter = 0;
	var _sequenceImages = [];
	var _animationInterval;

	function init() {
		_instance.classList.add('header-image-container');
		_instance.style.left = 'initial';
		_instance.style.width = WIDTH + 'px';
		_instance.style.height = HEIGHT + 'px';
		_instance._x = (hasIntroPlayed) ? 0 : 270;
		_instance._y = 0;

		if (BrowserDetect.MOBILE) {
			_instance._x = 0;

			_starsBg = parentNode.querySelector('.stars-container');
			_starsBg.style.opacity = 1;

			_starsBg2 = parentNode.querySelector('.stars-container-2');

			_starsBg2.style.display = 'none';

			var img = document.createElement('img');
			img.classList.add('header-sequence-image');
			_instance.appendChild(img);

			img.onload = function() {
				resize();
				img.style.opacity = 1;
			}

			img.src = 'assets/images/home/header-earth-8.png';
		} else {
			fadeInStars();	
		}

		addListeners();
	}

	function addListeners() {
		window.addEventListener('resize', resize);

		if (BrowserDetect.DESKTOP) {
			window.addEventListener('scroll', scroll);	
		}
	}

	function removeListeners() {
		window.removeEventListener('resize', resize);

		if (BrowserDetect.DESKTOP) {
			window.removeEventListener('scroll', scroll);
		}
	}

	function fadeInStars() {
		_starsBg = parentNode.querySelector('.stars-container');
		_starsBg2 = parentNode.querySelector('.stars-container-2');

		TweenLite.to(_starsBg, 3, {
			css: {
				opacity: 1
			},
			ease: Linear.easeNone
		});

		TweenLite.to(_starsBg2, 3, {
			css: {
				opacity: 1
			},
			ease: Linear.easeNone
		});
		
		loadFirstImage();
	}

	function loadFirstImage() {
		_firstImage = document.createElement('img');
		_firstImage.classList.add('header-sequence-image');
		_instance.appendChild(_firstImage);

		_firstImage.onload = function() {
			_firstImage.onload = null;

			resize();

			TweenLite.to(_firstImage, .5, {
				css: {
					opacity: 1
				},
				ease: Linear.easeNone
			});

			setTimeout(loadSequenceImages, 10);
		};

		_firstImage.src = FIRST_IMAGE_PATH;
	}

	function loadSequenceImages() {

		for (var j = 0; j < SEQUENCE_IMAGE_PATHS.length; j++) {

			var img = document.createElement('img');
			img.classList.add('header-sequence-image');
			img.style.zIndex = j;
			_instance.appendChild(img);

			img.onload = function() {
				sequenceImageLoaded();
			};

			img.src = SEQUENCE_IMAGE_PATHS[j] + '?' + Math.random();

			_sequenceImages.push(img);

		}
	}

	function sequenceImageLoaded() {
		_sequenceLoadCounter ++;

		if (_sequenceLoadCounter >= SEQUENCE_IMAGE_PATHS.length) {
			animateSequence();
		}
	}

	function animateSequence() {
		var del = (hasIntroPlayed) ? .5 : 2.5;
		setTimeout(function () {
			fadeInImage();
			_animationInterval = setInterval(fadeInImage, 400);
		}, del * 1000);

		if (!hasIntroPlayed) {
			TweenLite.to(_instance, 6, {
				_x: 0,
				ease: Quad.easeInOut
			});

			TweenLite.to(_starsBg, 6, {
				css: {
					backgroundPosition: '-225px 0px'
				},
				ease: Quad.easeInOut
			});

			TweenLite.to(_starsBg2, 6, {
				_x: -280,
				// css: {
				// 	backgroundPosition: '-280px 0px'
				// },
				ease: Quad.easeInOut
			});
		}
		
	}

	function fadeInImage() {
		
		TweenLite.to(_sequenceImages[_sequenceAnimationCounter], 2, {
			css: {
				opacity: 1
			},
			ease: Linear.easeNone
		});

		_sequenceAnimationCounter ++;

		if (_sequenceAnimationCounter >= _sequenceImages.length) {
			clearInterval(_animationInterval);
			_animationInterval = null;

			// remove unnecessary images
			
			setTimeout(function() {
				_instance.removeChild(_firstImage);

				for (var j = 0; j < _sequenceImages.length; j++) {
					var img = _sequenceImages[j];
					if (j != _sequenceImages.length - 1) {
						_instance.removeChild(img);
					}
				}	
			}, 2000);
			
		}
	}

	function resize() {
		var parentHeight = window.innerHeight;

		if (parentHeight < 508) {
			parentHeight = 508;
		}
		if (parentHeight > 970) {
			parentHeight = 970;
		}

		var destinHeight = parentHeight * 0.70;
		
		if (window.innerWidth <= 760) {
			_instance.style.left = '70%';
		} else {
			_instance.style.left = 'initial';
		}

		if (window.innerWidth > 1230) {
			destinHeight = parentHeight * 0.68;
		} else if (window.innerWidth > 760 && window.innerWidth <= 1230) {
			destinHeight = parentHeight * 0.57;
		} else if (window.innerWidth <= 760) {
			destinHeight = parentHeight * 1;
		}
		
		if (destinHeight < 300) {
			destinHeight = 300;
		}

		if (destinHeight > 750) {
			destinHeight = 750;
		}

		_instance.style.height = destinHeight + 'px';
		_instance.style.width = WIDTH * (destinHeight / HEIGHT) + 'px';

		

	};

	function scroll(e) {
		var scrollPos = WindowScroll.getPositionY();

		if (scrollPos > parentNode.clientHeight) {
			scrollPos = parentNode.clientHeight;
		}

		var destin = scrollPos / 8;

		if (_instance._y != destin) {
			_instance._y = destin;
		}
	}

	_instance.kill = function() {
		removeListeners();

		if (_animationInterval) {
			clearInterval(_animationInterval);
			_animationInterval = null;
		}
	};

	init();

	return _instance;

}