function AboutAccordion(node) {

	var _instance = {};

	var _headlines;
	var _bodies;

	function init() {
		_headlines = node.querySelectorAll('.headline-container');
		_bodies = node.querySelectorAll('.body-container');

		addListeners();
	}

	function addListeners() {
		window.addEventListener('resize', resize);

		for (var j = 0; j < _headlines.length; j++) {
			_headlines[j].num = j;
			_headlines[j].addEventListener('click', toggleAccordion);
		}
	}

	function removeListeners() {
		window.removeEventListener('resize', resize);

		for (var j = 0; j < _headlines.length; j++) {
			_headlines[j].removeEventListener('click', toggleAccordion);
		}
	}

	function toggleAccordion(e) {
		var bodyContainer = _bodies[e.currentTarget.num];
		var innerContainer = bodyContainer.children[0];

		if (parseInt(bodyContainer.style.height) == 0 || bodyContainer.style.height == '') {
			e.currentTarget.classList.add('active');

			TweenLite.to(bodyContainer, .3, {
				css: {
					
					height: innerContainer.clientHeight
				},
				ease: Quad.easeInOut
			});
		} else {
			e.currentTarget.classList.remove('active');

			TweenLite.to(bodyContainer, .3, {
				css: {
					height: 0
				},
				ease: Quad.easeInOut
			});
		}

		
	}

	function resize(e) {
		for (var j = 0; j < _bodies.length; j++) {
			var bodyContainer = _bodies[j];
			var innerContainer = bodyContainer.children[0];

			if (parseInt(bodyContainer.style.height) == 0 || bodyContainer.style.height == '') {
				
			} else {
				_bodies[j].style.height = innerContainer.clientHeight + 'px'
			}
		}
	}

	_instance.kill = function() {
		removeListeners();

		_headlines = null;
		_bodies = null;
	}

	init();

	return _instance;

}