function VideoPlayerControls(videoplayer, callbackShow, callbackHide) {

	var _instance = document.createElement('div');

	var _isPlaying = true;
	var _isHovering = false;
	var _isFullscreen = false;
	var _playButton;
	var _fullscreenButton;
	var _videoContainer;
	var _progressTrack;
	var _progressBar;
	var _movementTimeout;
	var _progressInterval;

	function init() {

		_instance.classList.add('controls-container');

		_videoContainer = videoplayer.video.parentNode;

		buildPlayButton();

		if (VideoPlayer.supportsFullscreen()) {
			buildFullscreenButton();
		}

		buildProgressTrack();
		buildProgressBar();
		addListeners();
		setTimeout(resize, 50);

		show();

		if (BrowserDetect.DESKTOP) {
			setTimeout(function() {
				mouseMove();

				_videoContainer.addEventListener('mousemove', mouseMove);
				_instance.addEventListener('mouseover', mouseOverControls);
				_instance.addEventListener('mouseout', mouseOutControls);
			}, 3000);

			_progressInterval = setInterval(progress, 50);
		}
	}

	function addListeners() {
		window.addEventListener('resize', resize);
		_playButton.addEventListener('click', playButtonClick);

		if (_fullscreenButton) {
			_fullscreenButton.addEventListener('click', fullscreenButtonClick);
		}		
	}

	function removeListeners() {
		window.removeEventListener('resize', resize);
		_playButton.removeEventListener('click', playButtonClick);

		if (_fullscreenButton) {
			_fullscreenButton.removeEventListener('click', fullscreenButtonClick);
		}

		_videoContainer.removeEventListener('mousemove', mouseMove);
		_instance.removeEventListener('mouseover', mouseOverControls);
		_instance.removeEventListener('mouseout', mouseOutControls);
	}

	function buildPlayButton() {
		_playButton = document.createElement('div');
		_playButton.classList.add('play-button');
		_instance.appendChild(_playButton);

		_isPlaying = false;

		if (BrowserDetect.DESKTOP) {
			_playButton.classList.add('pause-button');
			_isPlaying = true;
		}
	}

	function buildFullscreenButton() {
		_fullscreenButton = document.createElement('div');
		_fullscreenButton.classList.add('fullscreen-button');
		_instance.appendChild(_fullscreenButton);
	}

	function buildProgressTrack() {
		_progressTrack = document.createElement('div');
		_progressTrack.classList.add('progress-container');
		_instance.appendChild(_progressTrack);
	}

	function buildProgressBar() {
		_progressBar = document.createElement('div');
		_progressBar.classList.add('progress-bar');
		_progressTrack.appendChild(_progressBar);
	}

	function mouseOverControls(e) {
		if (_movementTimeout) {
			clearTimeout(_movementTimeout);
			_movementTimeout = null;
		}

		_isHovering = true;
	}

	function mouseOutControls(e) {
		_isHovering = false;

		if (_movementTimeout) {
			clearTimeout(_movementTimeout);
			_movementTimeout = null;
		}

		_movementTimeout = setTimeout(mouseStopped, 1000);
	}

	function mouseMove(e) {
		if (_movementTimeout) {
			clearTimeout(_movementTimeout);
			_movementTimeout = null;
		}

		if (_isHovering) {
			return;
		}

		if (parseInt(_instance.style.bottom) === -54) {
			show();
		}

		_movementTimeout = setTimeout(mouseStopped, 1000);
	}

	function mouseStopped() {
		if (_movementTimeout) {
			clearTimeout(_movementTimeout);
			_movementTimeout = null;
		}

		hide();
	}

	function playButtonClick() {
		if (_isPlaying) {
			_isPlaying = false;
			_playButton.classList.remove('pause-button');
			videoplayer.video.pause();
		} else {
			_isPlaying = true;
			_playButton.classList.add('pause-button');
			videoplayer.video.play();
		}
	}

	function fullscreenButtonClick() {
		if (_isFullscreen) {
			_isFullscreen = false;

			_fullscreenButton.classList.remove('fullscreen');

			if (document.exitFullscreen) {
				document.exitFullscreen();
		    } else if (document.msExitFullscreen) {
		    	document.msExitFullscreen();
		    } else if (document.mozCancelFullScreen) {
		    	document.mozCancelFullScreen();
		    } else if (document.webkitExitFullscreen) {
		    	document.webkitExitFullscreen();
		    }
		} else {
			_isFullscreen = true;

			_fullscreenButton.classList.add('fullscreen');
		
			if (videoplayer.video.parentNode.requestFullscreen) {
				videoplayer.video.parentNode.requestFullscreen();
			} else if (videoplayer.video.parentNode.msRequestFullscreen) {
				videoplayer.video.parentNode.msRequestFullscreen();
			} else if (videoplayer.video.parentNode.mozRequestFullScreen) {
				videoplayer.video.parentNode.mozRequestFullScreen();
			} else if (videoplayer.video.parentNode.webkitRequestFullscreen) {
				videoplayer.video.parentNode.webkitRequestFullscreen();
			}
		}
	}

	function progress() {
		var perc = videoplayer.video.currentTime / videoplayer.video.duration;
		_progressBar.style.width = _progressTrack.clientWidth * perc + 'px';
	}

	function show() {
		TweenLite.to(_instance, .3, {
			css: {
				bottom: 0
			},
			ease: Quad.easeInOut
		});

		callbackShow();
	}

	function hide() {
		if (BrowserDetect.TABLET) {
			return;
		}

		TweenLite.to(_instance, .3, {
			css: {
				bottom: -54
			},
			ease: Quad.easeInOut
		});

		callbackHide();
	}

	_instance.hide = function() {
		hide();
	};

	function resize(e) {
		if (_isFullscreen) {
			_instance.style.width = window.innerWidth + 'px';
		} else {
			_instance.style.width = window.innerWidth - window.nav.getWidth() + 'px';
		}

		_progressTrack.style.width = _instance.clientWidth - (165 * 2) + 'px';
	}

	_instance.kill = function() {
		removeListeners();

		if (_progressInterval) {
			clearInterval(_progressInterval);
			_progressInterval = null;
		}
	};

	init();

	return _instance;

}