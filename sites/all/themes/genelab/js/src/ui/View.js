function View() {

	var _instance = {};

	var _nav;
	var _moduleList;
    var _hasHomeIntroPlayed = false;

	function init() {
		_moduleList = [];

		buildNav();
		buildModules();
		updateLinks();
		addListeners();
	}

	function addListeners() {
		if (history && history.pushState) {

            // hack. this fixes a bug where ios triggers the event immediately
            setTimeout(function() {
                window.addEventListener('popstate', popstateTriggered);
            }, 100);
        
        }
	}

	function popstateTriggered(e) {
		killModules();
        
        TweenLite.to(document.querySelector('.site-column-right'), .2, {
            css: {
                opacity: 0
            },
            ease: Linear.easeNone,
            onComplete: loadPage
        });
	}

	function buildNav() {
		_nav = new Nav(document.querySelector('.nav'));

        window.nav = _nav;
	}

	function buildModules() {
		var modules = document.querySelectorAll('.module');

        for (var j = 0; j < modules.length; j++) {
            var node = modules[j];
            var moduleName = node.getAttribute('data-module');
            var moduleClass;
            
            switch (moduleName.toLowerCase()) {
                case 'countdown':
                    moduleClass = new Countdown(node);
                    break;
                case 'home-header':
                    moduleClass = new HomeHeader(node, _hasHomeIntroPlayed);
                    _hasHomeIntroPlayed = true;
                    break;
                case 'about-header':
                    moduleClass = new AboutHeader(node);
                    break;
                case 'about-columns':
                    moduleClass = new AboutColumns(node);
                    break;
                case 'about-accordion':
                    moduleClass = new AboutAccordion(node);
                    break;
                case 'iss-module':
                    moduleClass = new ISSModule(node);
                    break;
                case 'data-slider':
                    moduleClass = new DataSlider(node);
                    break;
                case 'events':
                    moduleClass = new Events(node);
                    break;
                case 'footer':
                    moduleClass = new Footer(node);
                    break;
            }

            if (moduleClass) {
                _moduleList.push(moduleClass);
            }
            
        }
	}

	function killModules() {
		for (var j = 0; j < _moduleList.length; j++) {
            if (_moduleList[j].kill) {
                _moduleList[j].kill();
            }

            _moduleList[j] = null;
        }
        _moduleList = [];
	}

	/* this should not be in the View */
	function loadPage() {
		var path = window.location.href;

        AJAX.load(path, function(response) {
            
            var frag = document.createElement('div');
            frag.innerHTML = response;

            updateRightSide(frag);

        }, 'get');
	}

	function updateRightSide(newPage) {
		var newContent = newPage.querySelector('.site-column-right');
        var currentRightCol = document.querySelector('.site-column-right');
        
        currentRightCol.innerHTML = newContent.innerHTML;
        currentRightCol.className = newContent.className;
        
        ColumnManager.update();
        buildModules();
        updateLinks();

        var navNum = newPage.querySelector('.selected-button');
        var ul = navNum.parentNode.parentNode;

        for (var j = 0; j < ul.children.length; j++) {
            if (ul.children[j].children[0] == navNum) {
                break;
            }
        }

        _nav.update(j);

        WindowScroll.scroll({
            y: 0,
            animate: false
        });

        TweenLite.to(currentRightCol, .3, {
            delay: .35,
            css: {
                opacity: 1
            },
            ease: Linear.easeNone
        });
	}

	function updateLinks() {
        if (history && history.pushState) {
    		var links = document.querySelectorAll('a');
            for (var j = 0; j < links.length; j++) {

                if (links[j].classList.contains('ignore-history')) {
                    
                } else {
                    links[j].onclick = function(e) {
                        e.preventDefault();

                        // if the nav is open, close it
                        _nav.close();

                        history.pushState('data', e.currentTarget.textContent, e.currentTarget.href);

                        killModules();
                        TweenLite.to(document.querySelector('.site-column-right'), .3, {
                            css: {
                                opacity: 0
                            },
                            ease: Linear.easeNone,
                            onComplete: loadPage
                        });

                    }
                }

            }
        }
	}

	init();

	return _instance;

}