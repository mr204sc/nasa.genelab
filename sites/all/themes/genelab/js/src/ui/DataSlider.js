function DataSlider(node) {

	var _instance = {};

	var _grabber;
	var _frontImage;
	var _isAnimating = true;

	function init() {
		_frontImage = node.querySelector('.front-image');
		_grabber = node.querySelector('.grabber');

		addListeners();

		if (!BrowserDetect.MOBILE) {
			loop();
		}
	}

	function addListeners() {
		if (BrowserDetect.TABLET || BrowserDetect.MOBILE) {
			_grabber.addEventListener('touchstart', grabberMouseDown);
			window.addEventListener('touchend', grabberMouseUp);
		} else {
			_grabber.addEventListener('mousedown', grabberMouseDown);
			window.addEventListener('mouseup', grabberMouseUp);
			node.addEventListener('mouseover', modulemouseOver);
		}
	}

	function removeListeners() {
		if (BrowserDetect.TABLET || BrowserDetect.MOBILE) {
			_grabber.removeEventListener('touchstart', grabberMouseDown);
			window.removeEventListener('touchend', grabberMouseUp);
		} else {
			_grabber.removeEventListener('mousedown', grabberMouseDown);
			window.removeEventListener('mouseup', grabberMouseUp);
			node.removeEventListener('mouseover', modulemouseOver);
		}
	}

	function modulemouseOver() {
		// stop the animation
		_isAnimating = false;
	}

	function grabberMouseDown(e) {
		e.preventDefault(); // prevents text selection
		
		_grabber.classList.add('active');

		if (BrowserDetect.TABLET || BrowserDetect.MOBILE) {
			_isAnimating = false;
			TweenLite.killTweensOf(_grabber);
			TweenLite.killTweensOf(_frontImage);
			node.addEventListener('touchmove', grabberMouseMove);
		} else {
			node.addEventListener('mousemove', grabberMouseMove);	
		}
	}

	function grabberMouseUp(e) {
		_grabber.classList.remove('active');

		if (BrowserDetect.TABLET || BrowserDetect.MOBILE) {
			node.removeEventListener('touchmove', grabberMouseMove);	
		} else {
			node.removeEventListener('mousemove', grabberMouseMove);	
		}
	}

	function grabberMouseMove(e) {
		
		// fixes a strange offsetLeft bug where the correct value isnt always returned
		var offset = getSliderOffset();
		var w = (e.clientX) ? e.clientX - offset : e.touches[0].pageX - offset;
		var perc = Math.round((w / node.clientWidth) * 100);

		if (perc < 5) {
			perc = 5;
		}

		if (perc > 95) {
			perc = 95;
		}

		TweenLite.to(_grabber, .25, {
			css: {
				left: perc + '%'
			},
			ease: Quad.easeOut
		});

		TweenLite.to(_frontImage, .25, {
			css: {
				width: perc + '%'
			},
			ease: Quad.easeOut
		});
	}

	function loop() {
		if (!_isAnimating) {
			return;
		}

		var randomPerc = (Math.random() * 80) + 10;
		
		if (_grabber) {
			TweenLite.to(_grabber, 1.3, {
				css: {
					left: randomPerc + '%'
				},
				ease: Quad.easeInOut
			});
		}

		if (_frontImage) {
			TweenLite.to(_frontImage, 1.3, {
				css: {
					width: randomPerc + '%'
				},
				ease: Quad.easeInOut
			});

			setTimeout(loop, 2300);
		}

		
	}

	function getSliderOffset() {
		var x = 0;

		if (window.innerWidth > 1230) {
			x = node.offsetLeft + nav.getWidth();
		} else if (window.innerWidth <= 1230 && window.innerWidth >= 1080) {
			x = 204;
		} else if (window.innerWidth <= 1079 && window.innerWidth > 940) {
			x = 204;
		} else if (window.innerWidth <= 940) {
			x = 20;
		}

		return x;
	}

	_instance.kill = function() {
		removeListeners();

		_grabber = null;
		_frontImage = null;
	};

	init();

	return _instance;

}