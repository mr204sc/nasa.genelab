function SpriteAnimation(image, animationObj, opt) {

	var _instance = {};

	var _img = image;
	var _timer = null;
	var _currentFrame = 0;
	var _reversedFrame = animationObj.length - 1;

	var defaultOpt = {
		speed: 7,
		loop: true,
	};


	function init() {
		mergeOptions();
		_instance.reset();
	}


	function mergeOptions() {
		if (opt) {
			for (var o in opt) {
				defaultOpt[o] = opt[o];
			}
		}
	}

	_instance.reset = function() {
		_currentFrame = 0;

		_img.style.backgroundPosition = animationObj[_currentFrame].x + 'px ' + animationObj[_currentFrame].y + 'px';
	} 

	_instance.play = function() {		
		_instance.stopAnimation();
		
		updateAnimation(false);
		_timer = setInterval(function() {
			updateAnimation(false);
		}, (1000 / defaultOpt.speed));
	}

	_instance.stopAnimation = function() {
		if (_timer) {
			clearInterval(_timer);
			_timer = null;
		}		
	}

	_instance.reverse = function() {
		_currentFrame = animationObj.length - 1;
		_instance.stopAnimation();

		updateAnimation(true);
		_timer = setInterval(function() {
			updateAnimation(true);
		}, (1000 / defaultOpt.speed));	
	}

	function updateAnimation(isReversed) {
		if (isReversed == true && _currentFrame == animationObj.length) {
			_currentFrame--;
		}
		
		var frame = animationObj[_currentFrame];
		
		_img.style.backgroundPosition = frame.x + 'px ' + frame.y + 'px';

		if (isReversed == true) {
			_currentFrame--;

		} else {
			_currentFrame++;	
		}

		if (_currentFrame == animationObj.length || _currentFrame == -1) {

			if (defaultOpt.loop == false) {
				if (_currentFrame == -1) {
					_currentFrame = 0;
				}
				if (_currentFrame == animationObj.length) {
					_currentFrame = animationObj.length - 1;
				}
				_instance.stopAnimation();
			} else if (_currentFrame == animationObj.length) {
				_currentFrame = 0;
			} else if (_currentFrame == -1) {
				_currentFrame = 0;
				_currentFrame = animationObj.length - 1;
			}
		} 

	}

	init();

	return _instance;
}