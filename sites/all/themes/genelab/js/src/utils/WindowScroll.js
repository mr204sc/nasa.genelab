(function WindowScroll(window) {

	var _instance = {};

	var _obj = {};

	_instance.scroll = function(opts) {
		var options = {
			x: 0,
			y: 0,
			speed: .8,
			ease: Quad.easeInOut,
			animate: true,
			onComplete: function() {}
		}

		for (var o in opts) {
			options[o] = opts[o];
		}

		_obj.x = _instance.getPositionX();
		_obj.y = _instance.getPositionY();

		if (options.animate) {
			TweenLite.to(_obj, options.speed, {
				onUpdate: update,
				x: options.x,
				y: options.y,
				ease: options.ease,
				onComplete: options.onComplete
			});
		} else {
			window.scrollTo(options.x, options.y);
		}
	};

	function update() {
		window.scrollTo(_obj.x, _obj.y);
	}

	_instance.getPositionX = function() {
		return Math.max(document.body.scrollLeft, window.pageXOffset);
	};

	_instance.getPositionY = function() {
		var y = Math.max(document.body.scrollTop, window.pageYOffset);

		if (y < 0) {
			y = 0;
		}
		return y;
	};

	window.WindowScroll = _instance;

})(window);