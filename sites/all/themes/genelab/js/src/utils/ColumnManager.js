(function ColumnManager(window) {

	var _instance = {};

	var col1List;
	var col2List;
	var col3List;
	var col4List;
	var col5List;
	var col1PlusList;
	var col2PlusList;
	var col3PlusList;
	var col4PlusList;
	var col5PlusList;
	var contentContainerList;
	var colLists;
	var colPlusLists;

	function init() {
		_instance.update();
	    addListeners();
	}

	function addListeners() {
		window.addEventListener('resize', resize);
	}

	function  resize(e) {
		// 184 is the nav width
        // 232 is the right margin
        // 8 * 20 is the col margins
        var w = window.innerWidth;

        // set the min width
        if (window.innerWidth < 1444) {
            w = 1444;
        }

        // set the max width
        if (w > 1660) {
            w = 1660;
        }
		
        if (window.innerWidth < 760) {
        	
        	for (var j = 0; j < colLists.length; j++) {
	            var list = colLists[j];
	            var plusList = colPlusLists[j];
	            
	            for (var i = 0; i < list.length; i++) {
	                list[i].style.width = '';
	            }

	            for (var i = 0; i < plusList.length; i++) {
	                plusList[i].style.width = ''
	            }

	        }

        } else {

	        var baseColWidth = Math.floor((w - 184 - 232 - (8 * 20)) / 7);
	        var currentColWidth = baseColWidth;
	        for (var j = 0; j < colLists.length; j++) {
	            var list = colLists[j];
	            var plusList = colPlusLists[j];
	            
	            for (var i = 0; i < list.length; i++) {
	                list[i].style.width = currentColWidth + 'px';
	            }

	            for (var i = 0; i < plusList.length; i++) {
	                plusList[i].style.width = currentColWidth + 20 + 'px';
	            }

	            currentColWidth += 20 + baseColWidth; 
	        }

	        for (var j = 0; j < contentContainerList.length; j++) {
	            contentContainerList[j].style.width = w - 184 - 232 + 'px';
	        }

    	}
	}

	_instance.update = function() {
		col1List = document.querySelectorAll('.col-1');
	    col2List = document.querySelectorAll('.col-2');
	    col3List = document.querySelectorAll('.col-3');
	    col4List = document.querySelectorAll('.col-4');
	    col5List = document.querySelectorAll('.col-5');

	    col1PlusList = document.querySelectorAll('.col-1-plus');
	    col2PlusList = document.querySelectorAll('.col-2-plus');
	    col3PlusList = document.querySelectorAll('.col-3-plus');
	    col4PlusList = document.querySelectorAll('.col-4-plus');
	    col5PlusList = document.querySelectorAll('.col-5-plus');

	    contentContainerList = document.querySelectorAll('.content-container');

	    colLists = [col1List, col2List, col3List, col4List, col5List];
	    colPlusLists = [col1PlusList, col2PlusList, col3PlusList, col4PlusList, col5PlusList];

		resize();
	}

	init();


	window.ColumnManager = _instance;

})(window);