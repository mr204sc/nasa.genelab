(function FlyoutManager() {

	var _instance = {};

	var _nav;
	var _oldSelectedNavButton;
	var _currentSectionContainer;
	var _currentButton;
	var _flyoutContainer;
	var _innerContainer;
	
	var _isOpen = false;

	_instance.init = function() {
		_nav = document.querySelector('.nav');
		_flyoutContainer = document.querySelector('.flyout-container');
		_innerContainer = new SmartObject(_flyoutContainer.querySelector('.inner-container'));

		addListeners();
	}

	function addListeners() {
		window.addEventListener('resize', resize);

		// find the necessary buttons and add listeners
		var buttons = document.querySelectorAll('[data-flyout]');
		
		for (var j = 0; j < buttons.length; j++) {
			buttons[j].addEventListener('click', buttonClick);
		}
	}

	function buttonClick(e) {
		e.preventDefault();

		var newSectionContainer = _flyoutContainer.querySelector('.' + e.target.getAttribute('data-flyout'));

		if (e.target.classList.contains('active')) {
			// this button is already selected

			_currentButton.classList.remove('active');
	 		_currentButton.classList.remove('selected-button');
	 		_currentButton = null;

	 		if (_oldSelectedNavButton) {
	 			_oldSelectedNavButton.classList.add('selected-button');
	 			_oldSelectedNavButton = null;
	 		}

	 		if (_isOpen) {
	 			closeFlyout();
	 		}

		} else {
			// this is a new button clicked

			if (_currentButton) {
				_currentButton.classList.remove('active');
				_currentButton.classList.remove('selected-button');
			} else if (_nav.querySelector('.selected-button')) {
				_oldSelectedNavButton = _nav.querySelector('.selected-button');
				_oldSelectedNavButton.classList.remove('selected-button');
			}

			_currentButton = e.target;

			_currentButton.classList.add('active');
	 		_currentButton.classList.add('selected-button');
	 		
	 		if (!_isOpen) {
	 			// hide all other section containers
	 			hideAllSectionContainers();

	 			_currentSectionContainer = newSectionContainer;
	 			newSectionContainer.style.display = 'block';
	 			newSectionContainer.style.opacity = 1;
	 			openFlyout();

	 		} else {
	 			// fade out old form container
	 			
	 			TweenLite.to(_currentSectionContainer, .2, {
	 				css: {
	 					opacity: 0
	 				},
	 				ease: Linear.easeNone,
	 				onComplete: hideAllSectionContainers
	 			});

	 			newSectionContainer.style.opacity = 0;
	 			TweenLite.to(newSectionContainer, .2, {
	 				onStart: function() {
	 					newSectionContainer.style.display = 'block';
	 				},
	 				delay: .3,
	 				css: {
	 					opacity: 1
	 				},
	 				ease: Linear.easeNone
	 			});

	 			_currentSectionContainer = newSectionContainer;
	 			
	 		}
		}
	}

	function hideAllSectionContainers() {
		for (var j = 0; j < _innerContainer.children.length; j++) {
			_innerContainer.children[j].style.display = 'none';
		}
	}

	function openFlyout() {
		_isOpen = true;

		_flyoutContainer.style.display = 'block';
		_flyoutContainer.style.width = window.innerWidth - _nav.clientWidth + 'px';
		

		_innerContainer._x = -window.innerWidth;
		TweenLite.to(_innerContainer, .6, {
			_x: 0,
			ease: Quad.easeInOut
		});
	}

	function closeFlyout() {
		_isOpen = false;

		TweenLite.to(_innerContainer, .6, {
			_x: -window.innerWidth,
			ease: Quad.easeInOut,
			onComplete: function() {
				_flyoutContainer.style.display = 'none';
			}
		});
	}

	function resize(e) {
		if (_isOpen) {
			_flyoutContainer.style.width = window.innerWidth - _nav.clientWidth + 'px';	
		}
	}

	window.FlyoutManager = _instance;

})();