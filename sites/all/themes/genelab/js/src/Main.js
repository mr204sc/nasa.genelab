(function Main(window) {
	var _instance = {};
    
    _instance.init = function() {
        setDeviceSettings();
        setMetaData();
        buildManagers();
        buildView();
    }

    function setDeviceSettings() {

        // BrowserDetect.MOBILE = true;
        // BrowserDetect.TABLET = false;
        // BrowserDetect.DESKTOP = false;


        if (BrowserDetect.TABLET) {
            document.body.classList.add('tablet');
            document.querySelector('.html').style.overflow = 'hidden';
        } else if (BrowserDetect.MOBILE) {
            document.body.classList.add('mobile');
        } else {
            document.body.classList.add('desktop');
            document.querySelector('.html').style.overflow = 'hidden';
        }

        document.body.style.height = 'auto';
    }

    function setMetaData() {
        if (BrowserDetect.MOBILE || BrowserDetect.TABLET)
        {
            var calcScale = BrowserDetect.MOBILE ? 1.0 : 0.948;
            var createMeta = document.createElement("meta");
            createMeta.name = "viewport";
            createMeta.content = "width=device-width, user-scalable=0, initial-scale=" + calcScale + ", maximum-scale=" + calcScale + ", minimum-scale=" + calcScale;
            document.head.appendChild(createMeta);
        }
    }

    function buildManagers() {
        FlyoutManager.init();
    }

    function buildView() {
        window.View = new View();
    }
	
	window.Main = _instance;

})(window);


window.onload = Main.init;