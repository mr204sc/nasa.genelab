/**
 * @fileoverview The Minify script gathers all scripts on the page - and runs
 * them through the closure compiler
 * 	Usage Examples:
 * 		Create the output of the minify script by clicking "CTRL + M".
 * 		This will output all "script" tags "src". If there are any scripts that
 * should not be included - give them
 * 		an attribute with data-minify="no"
 * 		<script data-minify="no"
 * src="js/do_not_include_in_minification.js"></script>
 *
 * 		Inline script tags are NOT being added as a part of the minification
 *
 * 		Per default the closure compiler uses the "SIMPLE_OPTIMIZATIONS" settings.
 * This can be changed by changing:
 * 		Minify.compilationLevel		= Minify.SIMPLE_OPTIMIZATIONS
 * 		To either: Minify.WHITESPACE_ONLY, Minify.SIMPLE_OPTIMIZATIONS,
 * Minify.ADVANCED_OPTIMIZATIONS
 *
 * 		The Minifier will per default out to the filename:
 * 			minified.js
 * 		To changes this:
 * 			Minify.outputFileName = "fileNameToOutput.js";
 *
 * @dependencies none
 * @author Anders
 * @browsers: Internal tool and the output is only made to work in: Chrome,
 * Safari and Firefox
 */

function Minify()
{
}

// Public variables
Minify.outputFileName = "minified.js";
Minify.compilationLevel = "SIMPLE_OPTIMIZATIONS";

Minify.compileInlineScripts = false;

// Constant variables
Minify.WHITESPACE_ONLY = "WHITESPACE_ONLY";
Minify.SIMPLE_OPTIMIZATIONS = "SIMPLE_OPTIMIZATIONS";
Minify.ADVANCED_OPTIMIZATIONS = "ADVANCED_OPTIMIZATIONS";

// Private variables
Minify._storeAllScripts = [];
Minify._dialogueBG = null;
Minify._dialogue = null;
// Reference to the dialogue that opens up
Minify._dialogueText = null;
// Reference to the textfield within the dialogue box

/**
 * Check for key down
 */
Minify.init = function()
{
    // Store all the "script" tags
    Minify._storeAllScripts = document.getElementsByTagName("script");

    // Check for key down
    var _currentKeys = [];
    var _keyIsDown = false;

    detectKeyUp = function(event)
    {
        _currentKeys = [];
        _keyIsDown = false;
    };
    detectKeyDown = function(event)
    {
        _keyIsDown = true;
        _currentKeys.push(event.keyCode);
        //console.log(event.keyCode);

        var matchCount = 0;
        for (var i = 0; i < _currentKeys.length; i++)
        {
            var currItem = _currentKeys[i];
            if (currItem == 17 || currItem == 77)
            {
                matchCount++;
            }
        }

        if (_currentKeys.length == 2 && matchCount == 2)
        {
            Minify.minifyAllScripts();
        }
        // M = 77
        // CTRL = 17
    };

    // Key detection
    window.addEventListener("keyup", detectKeyUp, false);
    window.addEventListener("keydown", detectKeyDown, false);
};

Minify.minifyAllScripts = function()
{
    var allScriptsAsText = "";
    var allScriptsArray = Minify._storeAllScripts;
    var _length = allScriptsArray.length - 1;
    var _currentScriptNumber = 0;

    // Show the dialogue
    Minify._dialogueBG = document.createElement("div");
    Minify._dialogueBG.style.position = "fixed";
    Minify._dialogueBG.style.width = window.innerWidth + "px";
    Minify._dialogueBG.style.height = window.innerHeight + "px";
    Minify._dialogueBG.style.backgroundColor = "#000000";
    Minify._dialogueBG.style.opacity = 0.3;
    Minify._dialogueBG.style.top = 0 + "px";
    Minify._dialogueBG.style.left = 0 + "px";
    Minify._dialogueBG.style.zIndex = 999999999998;

    Minify._dialogue = document.createElement("div");
    Minify._dialogue.style.position = "fixed";
    Minify._dialogue.style.width = "200px";
    Minify._dialogue.style.height = "150px";
    Minify._dialogue.style.backgroundColor = "#ffffff";
    Minify._dialogue.style.top = window.innerHeight / 2 - 200 / 2 + "px";
    Minify._dialogue.style.left = window.innerWidth / 2 - 150 / 2 + "px";
    Minify._dialogue.style.borderRadius = "3px";
    Minify._dialogue.style.zIndex = 999999999999;

    Minify._dialogueText = document.createElement("div");
    Minify._dialogueText.style.position = "absolute";
    Minify._dialogueText.style.top = "10px";
    Minify._dialogueText.style.left = "10px";
    Minify._dialogueText.style.width = 200 - 20 + "px";
    Minify._dialogueText.style.height = 150 - 20 + "px";
    Minify._dialogueText.style.color = "#000000";
    Minify._dialogueText.style.fontFamily = "arial";
    Minify._dialogueText.style.fontSize = "10px";

    var _dialogueClose = document.createElement("div");
    _dialogueClose.style.position = "absolute";
    _dialogueClose.style.top = 150 - 40 + "px";
    _dialogueClose.style.left = "10px";
    _dialogueClose.style.width = 200 - 40 + "px";
    _dialogueClose.style.fontFamily = "arial";
    _dialogueClose.style.textAlign = "center";
    _dialogueClose.style.fontSize = "10px";
    _dialogueClose.style.color = "#ff0000";
    _dialogueClose.style.cursor = "pointer";
    _dialogueClose.innerHTML = "Close";
    _dialogueClose.addEventListener("mousedown", removeDialogue, false);

    Minify._dialogue.appendChild(Minify._dialogueText);
    Minify._dialogue.appendChild(_dialogueClose);
    document.body.appendChild(Minify._dialogueBG);
    document.body.appendChild(Minify._dialogue);

    Minify._dialogueText.innerHTML = "<b>Loading scripts</b>";

    function removeDialogue()
    {
        // Close the dialogue
        _dialogueClose.removeEventListener("mousedown", removeDialogue, false);
        document.body.removeChild(Minify._dialogueBG);
        document.body.removeChild(Minify._dialogue);
        Minify._dialogueBG = null;
        Minify._dialogue = null;
        Minify._dialogueText = null;
    }

    // Load all the scripts on the page
    function loadScript(loadScriptNumber)
    {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState == 4 && xhr.status == 200)
            {
                var response = xhr.responseText;
                // Check the code for common pitfalls:
                if (response.indexOf("style.float") >= 0)
                {
                    alert("Potential error in " + getSRC + ":\nYou are using style.float - please change this to style.cssFloat throughout your code.");
                }

                var sourceCode = response + "\n";
                allScriptsAsText += sourceCode;
                Minify._dialogueText.innerHTML = "<b>Loading scripts:</b><br>" + _currentScriptNumber + " of " + _length;
                checkNext();
            }
        };
        var getSRC = allScriptsArray[loadScriptNumber].src;
        var checkForMinifyAttribute = allScriptsArray[loadScriptNumber].getAttribute("data-minify");
        if(checkForMinifyAttribute === "no" || getSRC === "" || getSRC === null || getSRC === "http://www.google-analytics.com/ga.js" || getSRC.indexOf("https://apis.google.com") > -1 || getSRC.indexOf("Minify.js") > -1)
        {
        	if(Minify.compileInlineScripts && checkForMinifyAttribute !== "no")
        	{
        		allScriptsAsText += allScriptsArray[loadScriptNumber].innerHTML;
        	}
        	
            checkNext();
        }
        else
        {
            xhr.open("GET", getSRC, true);
            xhr.send(null);
        }
    }

    function checkNext()
    {
        if (_currentScriptNumber < _length)
        {
            _currentScriptNumber++;
            loadScript(_currentScriptNumber);
        }
        else
        {
            Minify.minifyCode(allScriptsAsText);
        }
    }

    loadScript(0);
};

Minify.minifyCode = function(codeToMinify)
{
    function xmlhttpPost(strURL)
    {
        var xmlHttpReq = false;
        var self = this;
        // Mozilla/Safari
        if (window.XMLHttpRequest)
        {
            self.xmlHttpReq = new XMLHttpRequest();
        }
        self.xmlHttpReq.open("POST", strURL, true);
        self.xmlHttpReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        self.xmlHttpReq.onreadystatechange = function()
        {
            if (self.xmlHttpReq.readyState == 4)
            {
                var foundError = false;
                // console.log(self.xmlHttpReq.responseText);
                var getResponse = self.xmlHttpReq.responseText;
                var parser = new DOMParser();
                var xmlDoc = parser.parseFromString(getResponse, "text/xml");

                // generate output:
                
                var originalSize = Number(xmlDoc.getElementsByTagName("originalSize")[0].firstChild.nodeValue);
                var compressedSize = Number(xmlDoc.getElementsByTagName("compressedSize")[0].firstChild.nodeValue);

                var calculatedPercentage = (100 - Math.round((compressedSize / originalSize) * 100)) + "%";

                var checkForError = xmlDoc.getElementsByTagName("error");
                
                
                
                if (checkForError.length > 0)
                {
                    foundError = true;
                }

                if (foundError == false)
                {
                    var compiledCode = xmlDoc.getElementsByTagName("compiledCode")[0].firstChild.nodeValue;
                    if (compiledCode.indexOf("}(") >= 0)
                    {
                        alert("Potential error when merging:\nWhen the code gets merged all white space gets removed. It looks like you have not ended one of your functions() {}; with a semi colon ;");
                    }
                    var output = "<b>Done:</b><br>Original size: " + originalSize + "<br>Compiled size: " + compressedSize + "<br>Saved " + calculatedPercentage + " off the original size";
                    var getResultURL = "http://www.hellomonday.net/_tools/minify/ClosureCompiler.php?downloadURL=" + xmlDoc.getElementsByTagName("outputFilePath")[0].firstChild.nodeValue + "&saveAs=" + Minify.outputFileName;
                    Minify._dialogueText.innerHTML = output + "<br><br>Download the file <a href='" + getResultURL + "'>here</a>";
                }
                else
                {
                    var errorOutput = "";
                    for (var i = 0; i < checkForError.length; i++)
                    {
                        trace("checkForError : " + checkForError[i].toString())
                        errorOutput += checkForError[i].firstChild.nodeValue;
                    }

                    Minify._dialogueText.innerHTML = "The Closure Compiler returned with the following errors: <br><br>" + errorOutput;
                }
                // Open the file autmatically in a new window
                // window.open(getResultURL, "_blank");
                // document.location.href = codeToMinify;

                //document.write(codeToMinify);
            }
        };

        

        Minify._dialogueText.innerHTML = "<b>Scripts loaded:</b><br>Sending to Closure Compuler";
        self.xmlHttpReq.send("compilation_level=" + Minify.compilationLevel + "&output_format=xml&output_info=compiled_code&output_info=errors&output_info=statistics&output_file_name=" + Minify.outputFileName + "&js_code=" + encodeURIComponent(codeToMinify));
    }

    // Post to the Closure Compuler
    xmlhttpPost("http://closure-compiler.appspot.com/compile");
};

if (window.addEventListener)
{
    window.addEventListener("load", Minify.init, false);
}