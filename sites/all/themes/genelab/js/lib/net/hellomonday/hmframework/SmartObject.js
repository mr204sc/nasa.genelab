/*
 * SmartObject
 *
 * Makes it possible to access new properties of the HTMLElement - automatically uses 3D transform if that is available
 * *
 * If the function "SmartObjectRenderer.renderOnTweenMaxTicker()" is run - all rendering will ONLY take place inside TweenMax's render ticks (experimental)
 *
 * GETTER AND SETTERS:
 *  _x                   : Moves the item on the X position
 *  _y                   : Moves the item on the Y position
 *  _z                   : Moves the item on the Y position
 *  _scaleX
 *  _scaleY
 *  _scale               : Scales both scaleX and scaleY with the same value
 *  _rotation            : Rotates the item X degress
 *  _rotationX           : Rotates the item on the X axis
 *  _rotationY           : Rotates the item on the Y axis
 *  _rotationZ           : Rotates the item on the Z axis
 *  _opacity             : Changes the opacity of the element
 *  _transformOriginX    : Defines the registration point of the element
 *  _transformOriginY    : Defines the registration point of the element
 *  _transformOriginZ
 *  _skewX               : Skews the object on the X axis (not available in IE8 and below)
 *  _skewY               : Skews the object on the Y axis (not available in IE8 and below)
 *
 *  _perspective         : set the perspective in pixels
 *  _transformStyle      : can be either "flat" or "preserve-3d"
 *  _backfaceVisibility  : can either be "visible" or "hidden"
 * 
 *  (Note: IE8: Only _x, _y, _rotation, _opacity, _transformOriginX, _transformOriginY is supported)
 * 
 * 
 *  BOOLEANS
 *   ___useRounded       : (default true) - set to false to not "round" the X, Y, Z postions
 *   ___use3D            : (default true) - set to false to NOT have the element use translate3d - but just translate
 *   ___doNotUseTranslate: (default false) - set to true to make it move with "style.left" and "style.top" instead
 * 
 * 
 * CSS FILTERS (only available in Chrome 18+, Safari 6+ - should also be in iOS6+)
 *  _blur                : Distance in pixels 0 - XX pixels
 *  _greyscale           : Greyscale percent 0 - 1
 *  _hue                 : Hue Rotation - -180 - 180
 *  _sepia               : Sepia percent 0 - 1
 *  _saturate            : Saturate 0 - 1
 *  _invert              : Invert (values = ?)
 *  _brightness          : Brightness percent 0 - 1
 *  _contrast            : Contrast percent 0 - 1
 *
 * 
 * FUNCTIONS:
 *  _getWidth()          : Gets the "real" offset width of the object (even if its not added to the DOM)
 *  _getHeight()         : Gets the "real" offset eight of the object (even if its not added to the DOM)
 *
 * 
 * TODO:
 * Make TransformOrigins work with percentages
 *
 * Links:
 * Good resource on extending the dom in IE8 - http://msdn.microsoft.com/en-us/library/dd229916(VS.85).aspx
 * Getter and Setters in IE5.5+ = http://code.google.com/p/vbclass/*
 * // IE Matrix filter: http://msdn.microsoft.com/en-us/library/ms533014%28VS.85,loband%29.aspx*
 *  // CSS Filter - http://davidwalsh.name/css-filters
 */

var SmartObjectRenderer =
{
};

SmartObjectRenderer.turnedOn = false;
SmartObjectRenderer.objectsToMove = null;

SmartObjectRenderer.renderOnTweenMaxTicker = function()
{
    SmartObjectRenderer.objectsToMove = new Array();
    TweenMax.ticker.addEventListener("tick", SmartObjectRenderer.render, true);
    SmartObjectRenderer.turnedOn = true;
}

SmartObjectRenderer.render = function()
{
    var _length = SmartObjectRenderer.objectsToMove.length;
    for (var i = 0; i < _length; i++)
    {
        var updateItem = SmartObjectRenderer.objectsToMove[i];
        SmartObject.update(updateItem);
        updateItem.___firstRun = false;
        updateItem.___isSetToMove = false;
    }
    SmartObjectRenderer.objectsToMove = [];
}
/**
 * Creates an animateable DIV
 * @param {target} Is optional - but converts an existing object to a "SmartObject"
 **/
function SmartObject(target)
{
    var defaultSmartObjectValues = SmartObject.defaults;
    if (target == null)
    {
        var mainClip = document.createElement("div");
       mainClip.style.position = "absolute";
        mainClip.style.left = null;
        mainClip.style.top = null;
    }
    else
    {
        mainClip = target;
       mainClip.style.position = "absolute";
        // Extract the current values and pass that to the SmartObject
        if (mainClip.style.left)
        {
            defaultSmartObjectValues._x = parseInt(mainClip.style.left);
        }

        if (mainClip.style.top)
        {
            defaultSmartObjectValues._y = parseInt(mainClip.style.top);
        }

        if (mainClip.style.opacity)
        {
            defaultSmartObjectValues._opacity = mainClip.style.opacity;
        }

        if (mainClip.style[SmartObject.prefix + "Perspective"])
        {
            defaultSmartObjectValues._perspective = mainClip.style[SmartObject.prefix + "Perspective"];
        }

        if (mainClip.style[SmartObject.prefix + "TransformStyle"])
        {
            defaultSmartObjectValues._transformStyle = mainClip.style[SmartObject.prefix + "TransformStyle"];
        }

        if (mainClip.style[SmartObject.prefix + "BackfaceVisibility"])
        {
            defaultSmartObjectValues._backfaceVisibility = mainClip.style[SmartObject.prefix + "BackfaceVisibility"];
        }
        mainClip.style.left = null;
        mainClip.style.top = null;
    }
    mainClip.classList.add('smart-object');
    return SmartObject.convert(mainClip, defaultSmartObjectValues);
}

SmartObject.defaults =
{
    _x: 0, _y: 0, _z: 0, _rotation: 0, _rotationX: 0, _rotationY: 0, _rotationZ: 0, _scaleX: 1, _scaleY: 1, _opacity: 1, _skewX: 0, _skewY: 0, _perspective: null, _transformStyle: "flat", _backfaceVisibility: "hidden", _transformOriginX: 50, _transformOriginY: 50, _transformOriginZ: 50
};

// Converts an object to an animateable object
SmartObject.convert = function(mainClip, defaultSmartObjectValues)
{
    mainClip.___isSmartObject = true;
    mainClip.___firstRun = true;
    mainClip.___hasHadAdvancedStyle = false;
    mainClip.___currTranslate = null;
    mainClip.___isSetToMove = false;

    // Rounded
    mainClip.___useRounded = true;
    mainClip.___use3D = true;
    mainClip.___doNotUseTranslate = false;

    // Add the different funcitons to the object
    // Get width
    mainClip._getWidth = SmartObject.getWidth;
    // Get height
    mainClip._getHeight = SmartObject.getHeight;
    //
    mainClip._dispatchCustomEvent = SmartObject.dispatchCustomEvent;

    // IE7 specific fix
    // if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 7)
    // {
    //       // Used for IE7 and IE8 rotation compensation (being changed and set through FixIE.js)
    //     mainClip.___compensateX = 0;
    //     mainClip.___compensateY = 0;
    //     // Used to handle the IE7 "setters"
    //     mainClip._setter = new Array();
    //     // problem: on propertychange is only fired when the object is on the stage
    //     mainClip.attachEvent("onpropertychange", update);
    // }

    // if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 8)
    // {
          // Used for IE7 and IE8 rotation compensation (being changed and set through FixIE.js)
        // mainClip.___compensateX = 0;
        // mainClip.___compensateY = 0;
        // SmartObject.setProperty(mainClip, "_x", defaultSmartObjectValues._x, SmartObject.updateIE8);
        // SmartObject.setProperty(mainClip, "_y", defaultSmartObjectValues._y, SmartObject.updateIE8);
        // SmartObject.setProperty(mainClip, "_rotation", defaultSmartObjectValues._rotation, SmartObject.updateIE8);
        // SmartObject.setProperty(mainClip, "_scaleX", defaultSmartObjectValues._scaleX, SmartObject.updateIE8);
        // SmartObject.setProperty(mainClip, "_scaleY", defaultSmartObjectValues._scaleY, SmartObject.updateIE8);
        // SmartObject.setProperty(mainClip, "_opacity", defaultSmartObjectValues._opacity, SmartObject.updateIE8);
        // // Transform Origin
        // SmartObject.setProperty(mainClip, "_transformOriginX", defaultSmartObjectValues._transformOriginX, SmartObject.updateIE8);
        // SmartObject.setProperty(mainClip, "_transformOriginY", defaultSmartObjectValues._transformOriginY, SmartObject.updateIE8);
    // }
    // else
    // {
        // Positions
        SmartObject.setProperty(mainClip, "_x", defaultSmartObjectValues._x, SmartObject.setToUpdate);
        SmartObject.setProperty(mainClip, "_y", defaultSmartObjectValues._y, SmartObject.setToUpdate);
        SmartObject.setProperty(mainClip, "_z", defaultSmartObjectValues._z, SmartObject.setToUpdate);
        // Rotation
        SmartObject.setProperty(mainClip, "_rotationX", defaultSmartObjectValues._rotationX, SmartObject.setToUpdate);
        SmartObject.setProperty(mainClip, "_rotationY", defaultSmartObjectValues._rotationY, SmartObject.setToUpdate);
        SmartObject.setProperty(mainClip, "_rotationZ", defaultSmartObjectValues._rotationZ, SmartObject.setToUpdate);
        SmartObject.setProperty(mainClip, "_rotation", defaultSmartObjectValues._rotation, SmartObject.setToUpdate);
        // Scaling
        SmartObject.setProperty(mainClip, "_scaleX", defaultSmartObjectValues._scaleX, SmartObject.setToUpdate);
        SmartObject.setProperty(mainClip, "_scaleY", defaultSmartObjectValues._scaleY, SmartObject.setToUpdate);
        SmartObject.setProperty(mainClip, "_scale", 1, SmartObject.setScale);

        // Skew
        SmartObject.setProperty(mainClip, "_skewX", defaultSmartObjectValues._skewX, SmartObject.setToUpdate);
        SmartObject.setProperty(mainClip, "_skewY", defaultSmartObjectValues._skewY, SmartObject.setToUpdate);
        // Opacity
        SmartObject.setProperty(mainClip, "_opacity", defaultSmartObjectValues._opacity, SmartObject.setOpacity);
        // Transform Origin
        SmartObject.setProperty(mainClip, "_transformOriginX", defaultSmartObjectValues._transformOriginX, SmartObject.setTransformOrigin);
        SmartObject.setProperty(mainClip, "_transformOriginY", defaultSmartObjectValues._transformOriginY, SmartObject.setTransformOrigin);
        SmartObject.setProperty(mainClip, "_transformOriginZ", defaultSmartObjectValues._transformOriginZ, SmartObject.setTransformOrigin);
        // 3D speficic settings
        // SmartObject.setProperty(mainClip, "_perspective", defaultSmartObjectValues._perspective, SmartObject.setPerspective);
        // SmartObject.setProperty(mainClip, "_transformStyle", defaultSmartObjectValues._transformStyle, SmartObject.setTransformStyle);
        // SmartObject.setProperty(mainClip, "_backfaceVisibility", defaultSmartObjectValues._backfaceVisibility, SmartObject.setBackfaceVisibility);

        // CSS filters
        // SmartObject.setProperty(mainClip, "_blur", 0, SmartObject.updateFilters);
        // SmartObject.setProperty(mainClip, "_grayscale", 0, SmartObject.updateFilters);
        // SmartObject.setProperty(mainClip, "_hue", 0, SmartObject.updateFilters);
        // SmartObject.setProperty(mainClip, "_sepia", 0, SmartObject.updateFilters);
        // SmartObject.setProperty(mainClip, "_saturate", 1, SmartObject.updateFilters);
        // SmartObject.setProperty(mainClip, "_invert", 0, SmartObject.updateFilters);
        // SmartObject.setProperty(mainClip, "_brightness", 0, SmartObject.updateFilters);
        // SmartObject.setProperty(mainClip, "_contrast", 1, SmartObject.updateFilters);
    //}
    //mainClip.style.overflow = "hidden";

    // Everything set up the object - allow it to be updated
    mainClip.___firstRun = false;
    // Update the SmartObject with all its properties
    SmartObject.update(mainClip);

    return mainClip;
};

SmartObject.prefix = (function()
{
    if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= "8") {
        return "";
    }

    var styles = window.getComputedStyle(document.documentElement, ''), pre = (Array.prototype.slice
    .call(styles)
    .join('')
    .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
    )[1], dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];
    //old return {dom: dom, lowercase: pre, css: '-' + pre + '-', js: pre[0].toUpperCase() + pre.substr(1)}
    if (pre == "moz")
    {
        pre = "Moz";
    }
    return pre;
})();

SmartObject.getWidth = function()
{
    var mainClip = this;
    var returnWidth = 0;
    if (mainClip.parentNode == null)
    {
        document.body.appendChild(mainClip);
        returnWidth = mainClip.offsetWidth;
        document.body.removeChild(mainClip);
    }
    else if (mainClip.offsetWidth == 0)
    {
        var currentParentNode = mainClip.parentNode;
        document.body.appendChild(mainClip);
        returnWidth = mainClip.offsetWidth;
        currentParentNode.appendChild(mainClip);
    }
    else
    {
        returnWidth = mainClip.offsetWidth;
    }
    return returnWidth;
}

SmartObject.getHeight = function()
{
    var mainClip = this;
    var returnHeight = 0;
    if (mainClip.parentNode == null)
    {
        document.body.appendChild(mainClip);
        returnHeight = mainClip.offsetHeight;
        document.body.removeChild(mainClip);
    }
    else if (mainClip.offsetHeight == 0)
    {
        var currentParentNode = mainClip.parentNode;
        document.body.appendChild(mainClip);
        returnHeight = mainClip.offsetHeight;
        // trace("returnWidth : " + returnWidth)
        currentParentNode.appendChild(mainClip);
    }
    else
    {
        returnHeight = mainClip.offsetHeight;
    }
    return returnHeight;
};

/**
 * Function to dispatch a custom event
 */
SmartObject.dispatchCustomEvent = function(event)
{
    var mainClip = this;
    // if (document.createEventObject && BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 8)
    // {
    //     // dispatch for IE < 9
    //     var evt = document.createEventObject();
    //     return mainClip.fireEvent('on' + event, evt)
    // }
    // else
    // {
        // dispatch for firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true);
        // event type,bubbling,cancelable
        return !mainClip.dispatchEvent(evt);
    // }
}

/**
 * Function call when a property
 */
SmartObject.setProperty = function(target, kind, value, callback)
{
    // this[kind] = value;
    SmartObject.extend(target, kind, callback);
    target[kind] = value;
}
// Enable getter and setters
SmartObject.extend = function(target, value, useFunction)
{
    if (Object.defineProperty)
    {
        Object.defineProperty(target, value,
        {
            get: function()
            {
                return value;
            }, set: function(setValue)
            {
                value = setValue;
                useFunction(target, setValue);
            }
        })

    }
    else if (target.__defineGetter__)
    {
        // Mozilla, Webkit and other browsers
        //http://ejohn.org/blog/javascript-getters-and-setters/
        //http://johndyer.name/native-browser-get-set-properties-in-javascript/
        target.__defineGetter__(value, function()
        {
            return value;
        });

        target.__defineSetter__(value, function(setValue)
        {
            value = setValue;
            useFunction(target, setValue);
        });
    }
}

SmartObject.setToUpdate = function(target)
{
    if (SmartObjectRenderer.turnedOn == true)
    {
        // CHECK IF THE ITEM IS ALREADY IN THE ARRAY!
        if (target.___isSetToMove == false)
        {
            target.___isSetToMove = true;
            SmartObjectRenderer.objectsToMove.push(target);
        }
    }
    else
    {
        SmartObject.update(target);
    }
}
// SmartObject.setBackfaceVisibility = function(target, value)
// {
//     target.style[SmartObject.prefix + "BackfaceVisibility"] = value;
// }

// SmartObject.setPerspective = function(target, value)
// {
//     target.style[SmartObject.prefix + "Perspective"] = value + "px";
// }

// SmartObject.setTransformStyle = function(target, value)
// {
//     target.style[SmartObject.prefix + "TransformStyle"] = value;
// }

SmartObject.setOpacity = function(target, value)
{
    if (target._opacity != 1)
    {
        target.style.opacity = target._opacity;
    }
    else
    {
        target.style.opacity = null;
    }
}

SmartObject.setScale = function(target, value)
{
    target._scaleX = value;
    target._scaleY = value;
}

SmartObject.setTransformOrigin = function(target, value)
{
    if (target._transformOriginX != 50 || target._transformOriginY != 50 || target._transformOriginZ != 50)
    {
        // target.style.msTransformOrigin = target.style.mozTransformOrigin = target.style.webkitTransformOrigin = target.style.oTransformOrigin = target.style.transformOrigin = target._transformOriginX + "% " + target._transformOriginY + "%";
        if (BrowserDetect.TRANSLATE3D_SUPPORT == true && target._transformOriginZ != 50 && target.___use3D == true)
        {
            target.style[SmartObject.prefix + "TransformOrigin"] = target._transformOriginX + "% " + target._transformOriginY + "% " + target._transformOriginZ + "%";
        }
        else
        {
            target.style[SmartObject.prefix + "TransformOrigin"] = target._transformOriginX + "% " + target._transformOriginY + "%";
        }
    }
    else
    {
        target.style[SmartObject.prefix + "TransformOrigin"] = null;
    }
}
/*
 * Updates the SmartObjects values - repaint and redraws the element
 */
SmartObject.update = function(mainClip)
{

    if (mainClip.___firstRun == false)
    {
        var useTranslateString;
        var getX = mainClip._x;
        var getY = mainClip._y;

        if (mainClip.___useRounded == true)
        {
            getX = Math.round(mainClip._x);
            getY = Math.round(mainClip._y);
        }

        if (BrowserDetect.TRANSLATE3D_SUPPORT == true && mainClip.___use3D == true)
        {
            useTranslateString = "translate3d(" + getX + "px, " + getY + "px, " + mainClip._z + "px)";
            if (mainClip._rotationX != 0)
            {
                useTranslateString += " rotateX(" + mainClip._rotationX + "deg)";
            }
            if (mainClip._rotationY != 0)
            {
                useTranslateString += " rotateY(" + mainClip._rotationY + "deg)";
            }
            if (mainClip._rotationZ != 0)
            {
                useTranslateString += " rotateZ(" + mainClip._rotationZ + "deg)";
            }
        }
        else
        {
            useTranslateString = "translate(" + getX + "px, " + getY + "px)";
        }

        if (mainClip._rotation != 0)
        {
            useTranslateString += " rotate(" + mainClip._rotation + "deg)";
        }
        if (mainClip._scaleX != 1 || mainClip._scaleY != 1)
        {
            useTranslateString += " scale(" + mainClip._scaleX + ", " + mainClip._scaleY + ")";
        }
        if (mainClip._skewX != 0 || mainClip._skewY != 0)
        {
            useTranslateString += " skew(" + mainClip._skewX + "deg, " + mainClip._skewY + "deg)";
        }

       // trace("useTranslateString : " + useTranslateString)

        // Only apply if the values have actually changed
        if (mainClip.___doNotUseTranslate == true)
        {
            mainClip.style.top = getY + "px";
            mainClip.style.left = getX + "px";
        }
        else if (useTranslateString != mainClip.___currTranslate)
        {
            mainClip.style[SmartObject.prefix + "Transform"] = useTranslateString;
            mainClip.___currTranslate = useTranslateString;
        }
    }
};

// SmartObject.updateFilters = function(target, value)
// {
//     if (target._blur != 0 || target._grayscale != 0 || target._sepia != 0 || target._contrast != 1 || target._brightness != 0 || target._invert != 0 || target._saturate != 1)
//     {
//         var calcNewBrightness = target._brightness;
//         if (BrowserDetect.BROWSER_NAME == "Chrome" && BrowserDetect.TABLET == false && BrowserDetect.MOBILE == false)
//         {
//             calcNewBrightness = (target._brightness + 1);
//         }
//         target.style[SmartObject.prefix + "Filter"] = "blur(" + target._blur + "px) grayscale(" + target._grayscale + ") sepia(" + target._sepia + ") contrast(" + target._contrast + ") brightness(" + calcNewBrightness + ") invert(" + target._invert + ") saturate(" + target._saturate + ")"
//     }
//     else
//     {
//         target.style[SmartObject.prefix + "Filter"] = "";
//     }

// }

// SmartObject.updateIE8 = function(mainClip)
// {
//     if (mainClip._rotation != 0 || mainClip._scaleX != 1 || mainClip._scaleY != 1 || mainClip._opacity != 1)
//     {
//         mainClip.___hasHadAdvancedStyle = true;
//     }

//     if (mainClip.___hasHadAdvancedStyle == true)
//     {
//         var removeItemAgain = false;
//         // If IE7 the item needs to be on the stage to receive "onpropertychange" - therefore we force it to be on the stage
//         var storeParent = null;
//         if (mainClip.parentNode == null)
//         {
//             var tempDiv = document.createElement("div");
//             tempDiv.appendChild(mainClip);
//             document.body.appendChild(tempDiv);
//             removeItemAgain = true;
//         }
//         else
//         {
//             storeParent = mainClip.parentNode;
//         }

//         var filter = mainClip.style.filter;
//         // remove the current filter to get the original offsetWidth and offsetHeight
//         mainClip.style.filter = '';

//         var _offsetWidth = mainClip.offsetWidth;
//         var _offsetHeight = mainClip.offsetHeight;
//         mainClip.style.filter = filter;

//         // if (removeItemAgain == true)
//         // {
//         //storeParent = mainClip.parentNode;
//         mainClip.parentNode.removeChild(mainClip);
//         // re-do the filter

//         // }

//         // FIX for doing transformations in IE8 and IE7
//         var matrix = null;

//         // FIXME --- MOVE OUT - and create FixIE.IEMatrix...
//         function createIEMatrixString(M, mainClip)
//         {
//             var M11 = M.e(1, 1) * mainClip._scaleX;
//             var M12 = M.e(1, 2) * mainClip._scaleY;
//             var M21 = M.e(2, 1) * mainClip._scaleX;
//             var M22 = M.e(2, 2) * mainClip._scaleY;
//             matrix = Matrix.create([[M11, M12, 0], [M21, M22, 0], [0, 0, 1]]);
//             return 'M11=' + M11 + ', M12=' + M12 + ', M21=' + M21 + ', M22=' + M22;
//         }

//         var addOpacityString = " progid:DXImageTransform.Microsoft.Alpha(opacity=" + Math.round(mainClip._opacity * 100) + ")";

//         // var addAlphaFilter = "";
//         /*if (mainClip.___isAlphaPNG == true) {
//          addAlphaFilter = mainClip.___alphaPNGFilter;
//          }*/
//         var addMatrixStr = "";
//         if (mainClip._rotation != 0 || mainClip._scaleX != 1 || mainClip._scaleY != 1)
//         {
//             var matrix_str = createIEMatrixString(Matrix.Rotation(FixIE.degreesToRadians(mainClip._rotation)), mainClip);
//             addMatrixStr = "progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', " + matrix_str + ", filterType='nearest neighbor')";
//         }
//         var filter_str = addMatrixStr + addOpacityString;
//         // + addAlphaFilter;
//         //progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', " + matrix_str + ")" + addOpacityString; // progid:DXImageTransform.Microsoft.gradient(startColorstr=#110000FF,endColorstr=#110000FF)
//         if (mainClip.style.background == null)
//         {
//             mainClip.style.background = "transparent";
//         }
//         mainClip.style.MsFilter = filter_str;
//         // IE 8
//         mainClip.style.filter = filter_str;
//         // IE 6/7
//         mainClip.style["zoom"] = "1";
//         if (addMatrixStr != "")
//         {
//             // document.body.appendChild(mainClip);
//             FixIE.transformOrigin(mainClip, matrix, _offsetWidth, _offsetHeight);
//             FixIE.fixBoundaryBug(mainClip, matrix, _offsetWidth, _offsetHeight);
//             //document.body.removeChild(mainClip);
//         }
//         try
//         {
//             mainClip.style.left = mainClip._x + mainClip.___compensateX + "px";
//             mainClip.style.top = mainClip._y + mainClip.___compensateY + "px";
//         }
//         catch (error)
//         {

//         }
//         //    mainClip.style.display = "block";
//         if (storeParent != null)
//         {
//             storeParent.appendChild(mainClip);
//             if (removeItemAgain == true)
//             {
//                 tempDiv.parentNode.removeChild(tempDiv);
//                 tempDiv = null;
//             }
//         }
//         if (mainClip._rotation == 0 && mainClip._scaleX == 1 && mainClip._scaleY == 1 && mainClip._opacity == 1)
//         {
//             mainClip.___compensateX = 0;
//             mainClip.___compensateY = 0;
//             mainClip.___hasHadAdvancedStyle = false;
//         }
//     }
//     else
//     {
//         var storeParent = null;
//         if (mainClip.parentNode != null)
//         {
//             storeParent = mainClip.parentNode;
//             mainClip.parentNode.removeChild(mainClip);
//         }

//         mainClip.style.left = mainClip._x + mainClip.___compensateX + "px";
//         mainClip.style.top = mainClip._y + mainClip.___compensateY + "px";
//         if (storeParent != null)
//         {
//             storeParent.appendChild(mainClip);
//         }
//     }
// }