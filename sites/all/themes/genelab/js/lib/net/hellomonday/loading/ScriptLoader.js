/**
 * @fileoverview Adds an array of scripts the <head> tag, and calls a function
 *      when they are loaded.  Meant to use as an 'include' for javascript.
 */

/**
 * @constructor
 */
var ScriptLoader =
{
};

/**
 * Loads an array of scripts and calls a function when complete.
 * @param {Array <string>} scriptArray The array of string paths to the scripts
 *      to load.
 * @param {Function} callback The function to call when they are all loaded.
 */
ScriptLoader.loadScripts = function(scriptArray, callback, urlPrefix)
{
    var scriptLoadInt = 0;

    if (!urlPrefix)
    {
        urlPrefix = '';
    }

    for (var i = 0; i < scriptArray.length; i++)
    {
        // add the script tag to the head
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = urlPrefix + scriptArray[i];

        // bind the event to the callback function
        // there are several events for cross browser compatibility
        script.onreadystatechange = callback;
        script.onload = scriptLoaded;

        // fire the loading
        head.appendChild(script);
    }

    function scriptLoaded()
    {
        scriptLoadInt++;
        if (scriptLoadInt == scriptArray.length)
        {
            callback();
        }
    }
};