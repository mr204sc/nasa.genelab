
RemoveEvent = new Object();
RemoveEvent._oldRemove = $.fn.remove;

$.fn.remove = function(prop, value)
{
	// trace("jQuery override of remove();");
	$(this).trigger('remove');
	RemoveEvent._oldRemove.call(this);
}

