function Pinch(target, pinchDistance, direction, callback, fingerCount, callback2)
{
    if (fingerCount == null || typeof (fingerCount) === 'undefined') fingerCount = 2;
    if (typeof callback2 === "undefined") _hasSecondCallback = false;

    var _target = target,
        _pinchDistance = pinchDistance,
        _direction = direction,
        _fingerCount = fingerCount,
        _callback = callback,
        _hasSecondCallback = true,
        _fingerInitDistance = 0,
        _pinchDifference = 0,
        _isPinching = false;
        _initialPoint1 = null,
        _initialPoint2 = null;

    function distanceBetweenPoints(point1, point2)
    {
        var xs = Math.pow(point2.x - point1.x, 2),
            ys = Math.pow(point2.y - point1.y, 2);

        return Math.sqrt(xs + ys);
    }

    function touchStart(event)
    {
        if (event.touches.length == _fingerCount)
        {
            var finger1X = event.touches[0].pageX,
                finger1Y = event.touches[0].pageY,
                finger2X = event.touches[1].pageX,
                finger2Y = event.touches[1].pageY;
            var point1 = new Object(
            {
                x: finger1X,
                y: finger1Y
            });
            var point2 = new Object(
            {
                x: finger2X,
                y: finger2Y
            });
            _fingerInitDistance = distanceBetweenPoints(point1, point2);
            _initialPoint1 = point1;
            _initialPoint2 = point2;
        }
        event.preventDefault();
    }

    function touchMove(event)
    {
        if (event.touches.length == _fingerCount)
        {
            var finger1X = event.touches[0].pageX,
                finger1Y = event.touches[0].pageY,
                finger2X = event.touches[1].pageX,
                finger2Y = event.touches[1].pageY;
            var point1 = new Object(
            {
                x: finger1X,
                y: finger1Y
            });
            var point2 = new Object(
            {
                x: finger2X,
                y: finger2Y
            });
            _pinchDifference = distanceBetweenPoints(point1, point2) - _fingerInitDistance;
            if(_fingerInitDistance >= _pinchDistance && (Math.abs(point1.x - _initialPoint1.x) > 50))
                PINCH_ACTIVE = _isPinching = true;
            else
                PINCH_ACTIVE = _isPinching = false;
            if (_direction == "in")
            {
                if (_pinchDifference <= -_pinchDistance)
                {
                    _pinchDifference = 0;
                    _fingerInitDistance = distanceBetweenPoints(point1, point2);
                    _callback();
                }
                if (_hasSecondCallback && _pinchDifference >= _pinchDistance)
                {
                    _pinchDifference = 0;
                    _fingerInitDistance = distanceBetweenPoints(point1, point2);
                    callback2();
                }
            }
            else if (_direction == "out")
            {
                if (_pinchDifference >= _pinchDistance)
                {
                    _pinchDifference = 0;
                    _fingerInitDistance = distanceBetweenPoints(point1, point2);
                    _callback();
                }
                if (_hasSecondCallback && _pinchDifference <= -_pinchDistance)
                {
                    _pinchDifference = 0;
                    _fingerInitDistance = distanceBetweenPoints(point1, point2);
                    callback2();
                }
            }
        }
        event.preventDefault();
    }

    function touchEnd(event)
    {
        PINCH_ACTIVE = _isPinching = false;
    }

    if (BrowserDetect.TABLET == true || BrowserDetect.MOBILE == true)
    {
        _target.addEventListener(TouchEvent.TOUCH_START, touchStart, false);
        _target.addEventListener(TouchEvent.TOUCH_MOVE, touchMove, false);
        _target.addEventListener(TouchEvent.TOUCH_END, touchEnd, false);
    }
    else
    {
        // _target.addEventListener(TouchEvent.TOUCH_START, touchStart, false);
        // _target.addEventListener(TouchEvent.TOUCH_MOVE, touchMove, false);
        // _target.addEventListener(TouchEvent.TOUCH_END, touchEnd, false);
    }
}