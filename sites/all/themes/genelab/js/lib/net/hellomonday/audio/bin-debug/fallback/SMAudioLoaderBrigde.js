function SMAudioLoaderBrigde()
{
	var _instance                          = {}
    var _que;
    var _busy                              = false;

	function init()
	{
        _que = [];
        window.AudioLoader = _instance;                    	    	    
	}
	
	/**
     * @param {*} data - Array or String containing the path(s) to load 
     */
    _instance.loadAudio = function(data, completeCallback, updateCallback)
    {
        var type = Object.prototype.toString.call(data).indexOf("Array") > -1 ? "Array" : "String";
   
        var audioGroup = {};
        audioGroup.completeCallback = completeCallback;
        audioGroup.updateCallback = updateCallback;
        
        if(type != "Array")
        {
            audioGroup.path = []; 
            audioGroup.path.push(data);
        }
        else
        {
            audioGroup.path = data;
        }
        
        if(!_busy)
        {
            loadAudioGroup(audioGroup);
        }
        else
        {
            _que.push(audioGroup);
        }
    }
    
    function loadAudioGroup(group)
    {
        _busy = true;
        
        _curGroup = group;
        _curGroupTotal = group.path.length;
        
        loadNextSound(_curGroup.path[0]);         
    }
    
    function loadNextSound(path)
    {
        if(soundManager.getSoundById(path) == undefined)
        {
            soundManager.createSound({id: path, url: path, autoLoad: true, stream: false, onload: soundLoaded});
        }
        else
        {
            soundLoaded();
        }
    }
    
    function soundLoaded()
    {
        // Remove the loaded audio path from the group
        _curGroup.path.shift();
        
        var remainingPaths = _curGroup.path.length
        
        if(_curGroup.updateCallback)
        {
            var perc = 1 - (remainingPaths / _curGroupTotal);
            _curGroup.updateCallback(perc)
        }
        
        // Run complete callback if the group is finished loading
        if(remainingPaths < 1)
        {
            _busy = false;
            
            if(_curGroup.completeCallback)
            {
                _curGroup.completeCallback();
            }
            
            // Check if if theres more groups to load in the que
            if(_que.length > 0)
            {
                 loadAudioGroup(_que.shift());
            }
        }
        // Otherwise load the next sound
        else
        {
            loadNextSound(_curGroup.path[0]);
        }
    }
    
    /**
    * @param {Array} data - Array or String containing the path(s) to remove
    */
    _instance.removeAudioBuffer = function(data)
    {
        var type = Object.prototype.toString.call(data).indexOf("Array") > -1 ? "Array" : "String";
        var curPath;
        
        if(type != "Array")
        {
            soundManager.destroySound(data);
        }
        else
        {
            var i = 0;
			var l = data.length
			
			for(i; i < l; i++)
			{
			    soundManager.destroySound(data[i]);
			}
        }
    }
    
	init();

	return _instance;
}