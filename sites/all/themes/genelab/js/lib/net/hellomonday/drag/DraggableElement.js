/**
 * @fileoverview adds drag and drop functionality to an element.
 * @author Hello Monday (mike@hellomonday.com)
 */

/**
 * @constructor
 * @param {HTMLElement} element The DOM element to make draggable.
 */
var DraggableElement = function(element, containWithScreen)
{
	var instance = this;
	var elementOffsetX = 0;
	var elementOffsetY = 0;
	
	if (!containWithScreen) {
		containWithScreen = false;
	}
	
	element.style.position = 'absolute';
	
	this.startDragging = function(e)
	{
		
		var mousePosition = DomUtils.getMousePosition();
		var leftPos = mousePosition[0];
		var topPos = mousePosition[1];
		
		elementOffsetX = parseInt(e.currentTarget.style.left) - leftPos;
		elementOffsetY = parseInt(e.currentTarget.style.top) - topPos;
		
		document.addEventListener('mousemove', instance.onDrag);
		document.addEventListener('mouseup', instance.stopDragging);
		
		e.stopPropagation();
		e.preventDefault();
		return false;
	}
	
	this.stopDragging = function(e)
	{
		document.removeEventListener('mousemove', instance.onDrag);
		document.removeEventListener('mouseup', instance.stopDragging); 
	}
	
	this.onDrag = function()
	{
		var mousePosition = DomUtils.getMousePosition();
		var leftPos = mousePosition[0];
		var topPos = mousePosition[1];
		
		var newLeftPos = (leftPos + elementOffsetX);
		var newTopPos = (topPos + elementOffsetY);
		
		//keep the draggable option within the window if containWithScreen = true
		if (containWithScreen) {
			if ((newLeftPos + element.offsetWidth) > DomUtils.getWindowSize().width) {
				newLeftPos = (DomUtils.getWindowSize().width - element.offsetWidth);
			}
			
			if (newLeftPos < 0) {
				newLeftPos = 0;
			}
			
			if ((newTopPos + element.offsetHeight) > DomUtils.getWindowSize().height) {
				newTopPos = (DomUtils.getWindowSize().height - element.offsetHeight)
			}
			
			if (newTopPos < 0) {
				newTopPos = 0;
			}
		}
		
		element.style.left = newLeftPos + 'px';
		element.style.top = newTopPos + 'px'; 
	}
	
	this.addDragging = function()
	{
		element.addEventListener('mousedown', this.startDragging);
		element.addEventListener('mouseup', this.stopDragging);
	}
	
	this.removeDragging = function()
	{
		element.removeEventListener('mousedown', this.startDragging);
		element.removeEventListener('mouseup', this.stopDragging);
	}
	
	this.addDragging();
}
