
ContentManager = new Object();


// private static vars
ContentManager._activeTemplates         = new Array();
ContentManager._templateRegister        = new Array();
ContentManager._newPath                 = "";
ContentManager._blocked                 = false;
ContentManager._xml                     = null;
ContentManager._defaultPath             = "home";

ContentManager._prevTemplateObj         = null;
ContentManager._passedVariables         = null;

ContentManager._templateGroups          = new Array();



/**
 * Returns the value of the child with data-path name corresponding to the name you provide
 */
ContentManager.getTextByDataName = function(xml, name)
{
    var rVal = $(xml).children('div[data-name="' + name + '"]:first').text();
    return rVal;
}

ContentManager.getXmlByDataName = function(xml, name)
{
    var rVal = $(xml).children('div[data-name="' + name + '"]:first');
    return rVal;
}

ContentManager.getHtmlByDataName = function(xml, name)
{
    var rVal = $(xml).children('div[data-name="' + name + '"]:first').html();
    return rVal;
}

ContentManager.getIndexOfPath = function(xml, path)
{
    var i               = 0;
    var children        = $(xml).children(); 
    var l               = children.length;
    var child           = null;
    var rIndex          = 0;
    
    for(i = 0; i < l; i += 1)
    {
        child = children[i];
        
        if(path == $(child).attr("data-path"))
        {
            rIndex = i;
            break;
        }
    }
    
    return rIndex;
}

ContentManager.composeFullPathFromXML = function(xml)
{
    // //////trace("ContentManager.composeFullPathFromXML();");
    var pathStep        = $(xml).attr("data-path"); 
    var pathSteps       = new Array();
    
    // //////trace(pathStep, "pathStep");
    
    if(pathStep)
    {
        pathSteps.unshift(pathStep);
    }
    
    var parent          = $(xml).parent();
    var loops           = 0;
    
    while(pathStep)
    {
        // //////trace(loops, "loops");
        pathStep = $(parent).attr("data-path"); 
        // //////trace(pathStep, "pathStep");
        
        if(pathStep)
        {
            pathSteps.unshift(pathStep);
        }
        
        parent = $(parent).parent();
        
        loops += 1;
        if(loops > 10)
        {
            break;
        }
    }
    
    var rPath           = pathSteps.join("/");
    return rPath;
}



/**
* Function for adding templates to the ContentManager class
* 
* The new template should be a class that has these public functions
* newTemplate.templateIn();
* newTemplate.templateOut();
*
* @param {string} templateName Is the name of the template
* @param {object} JSClass Is the js class type
*/
ContentManager.addTemplate = function(templateName, JSClass)
{
    ContentManager._templateRegister.push({templateName: templateName, JSClass: JSClass});
}

ContentManager.addTransitionGroup = function(groupName, group)
{
    ContentManager._templateGroups.push({name: groupName, group: group});
}

ContentManager.getTransitionGroup = function(templateName1, templateName2)
{
    //////trace("ContentManager.getTransitionGroup();");
    
    //////trace(templateName1, "templateName1");
    //////trace(templateName2, "templateName2");
    
    var i               = null;
    var l               = ContentManager._templateGroups.length;
    var groupObj        = null;
    var group           = null;
    var rName           = null;
    
    for(i = 0; i < l; i += 1)
    {
        groupObj    = ContentManager._templateGroups[i];
        group       = groupObj.group;
        
        //////trace(groupObj.name, "groupObj.name");
        
        if($.inArray(templateName1, group) != - 1 && $.inArray(templateName2, group) != - 1)
        {
            //////trace("found");
            rName = groupObj.name;  
            break;
        }   
    }
    
    return rName;
}

/**
* Function for initializing the ContentManager
* @param {xml} xml Is the XML used to define the site structure and XML example looks like this <site><page _template="productTemplate" _path="page1"></page><page _template="productTemplate" _path="2"></page></site>
*/
ContentManager.init = function(xml, defaultPath)
{ 
    ////////trace("ContentManager.init();");
    
    ContentManager._xml = xml;
    
    if(defaultPath)
    {
        ContentManager._defaultPath = defaultPath;
    }
    
    //////////trace(ContentManager._xml, "ContentManager._xml");
    
    $(window).bind("hashchange", ContentManager.onHashChange);
    ContentManager.onHashChange();
}

ContentManager.autoCheck = function()
{
    var lastPath            = window.location.hash;
    var checkInterval       = .2;
    
    setTimeout(checkHash, 1000 * checkInterval);
    
    function checkHash()
    {
        var newPath = window.location.hash;
        
        ////trace("checkHash();");
        ////trace(newPath, "newPath");
        ////trace(lastPath, "lastPath");
        
        if(newPath != lastPath)
        {
            lastPath = newPath;
            ContentManager.onHashChange();
        }
        
        setTimeout(checkHash, 1000 * checkInterval);
    }
}


/**
* Function for allowing next template to be added
*/
ContentManager.nextTemplate = function(passedVars)
{
    ////trace("ContentManager.nextTemplate();");
    ContentManager._passedVariables = passedVars;
    
    ContentManager._blocked = false;
    ContentManager.onHashChange();
}

ContentManager.path = function(newPath)
{
    //trace("ContentManager.path();");
    //trace(newPath, "newPath");
    
    window.location.hash = "/" + newPath;
    
    //trace(window.location.hash, "window.location.hash");
}

ContentManager.getPathInLevel = function(level)
{
    var rPath = "";
    if(level < ContentManager._activeTemplates.length)
    {
        rPath = ContentManager._activeTemplates[level].path;
    }
    return rPath;
}

ContentManager.getFullPath = function()
{
    var rPath = "";
    if(ContentManager._activeTemplates.length > 0)
    {
        rPath = ContentManager.composeFullPathFromXML(ContentManager._activeTemplates[ContentManager._activeTemplates.length - 1].xml);
    }
    return rPath;
}

ContentManager.onHashChange = function(e)
{
    if(ContentManager._blocked == false)
    {
        // trace("************************");
        // trace("ContentManager.onHashChange");
        // trace("************************");
        
        ContentManager._newPath         = ContentManager.extractPath(window.location.hash);
        var pathArr                     = ContentManager._newPath.split("/");
        var pathXML                     ;
        var candidates                  = new Array();
        var templateName                ;
        var templateLevel               ;
        
        // trace("ContentManager._newPath : " + ContentManager._newPath);
        
        if(pathArr[0] == "")
        {
            pathArr[0] = ContentManager._defaultPath;
        }
        
        // find the add candidates
        while(pathArr.length > 0)
        {
            ////trace(pathArr, "pathArr");
            pathXML = ContentManager.findContent(pathArr);
            
            if(pathXML)
            {
                templateName = $(pathXML).attr("data-template");
                //trace(templateName, "templateName");
                
                if(templateName)
                {
                    templateLevel = ContentManager.findTemplateLevelFromName(templateName);
                    //trace(templateLevel, "templateLevel");
                    candidates.push({xml: pathXML, path: $(pathXML).attr("data-path"), templateName: templateName, level: templateLevel})
                }
            }
            
            pathArr.pop();
        }
        
        // sort candidates
        candidates.reverse();
        candidates = ContentManager.sortTemplateList(candidates);
        
        var i                   = 0;
        var l                   = candidates.length;
        var candidate           ;
        var type                = "none";
        var activeTemplates     = ContentManager._activeTemplates;
        
        //trace(candidates.length, "candidates.length");
        
        for(i = 0; i < l; i += 1)
        {
            candidate = candidates[i];
            ////trace(candidate.path, "candidate.path");
            ////trace(candidate.level, "candidate.level");
            ////trace(activeTemplates.length, "activeTemplates.length");
            
            if(activeTemplates[candidate.level] == null)
            {
                ////trace("pick this one");
                type = "push";
            }
            else if(activeTemplates[candidate.level].path != candidate.path || activeTemplates[candidate.level].fullPath.indexOf(ContentManager.composeFullPathFromXML(candidate.xml)) == - 1)
            {
                type = "pop";
            }
            if(type != "none")
            {
                break;
            }
        }
        
        if(type == "none")
        {
            if(candidates.length < activeTemplates.length && candidate)
            {
                type = "pop";
            }
        }
        
        // push or pop
        //trace(type, "type");
        if(type != "none")
        {
            ContentManager._blocked = true;
            
            if(type == "pop")
            {
                var oldTemplateObj = activeTemplates.pop();
                
                // pass on some variables
                oldTemplateObj.template.passedVariables = ContentManager._passedVariables;
                oldTemplateObj.template.nextTemplateName = candidate.templateName;
                oldTemplateObj.template.nextTemplatePath = candidate.path;
                oldTemplateObj.template.nextTemplateXML = candidate.xml;
                                
                // save this template 
                ContentManager._prevTemplateObj = oldTemplateObj;
                
                // call template in
                oldTemplateObj.template.templateOut();
            }
            else if(type == "push")
            {
                ////////trace(candidate.path, "candidate.path");
                ////////trace(candidate.templateName, "candidate.templateName");
                ////////trace(candidate.level, "candidate.level");
                    
                var JSClass = ContentManager.findTemplateFromName(candidate.templateName);
                
                if(JSClass)
                {
                    ////////trace(JSClass, "JSClass");
                    var newTemplate = new JSClass(candidate.xml);
                    
                    // pass on some variables
                    if(ContentManager._prevTemplateObj)
                    {
                        newTemplate.prevTemplateName = ContentManager._prevTemplateObj.templateName;
                        newTemplate.prevTemplatePath = ContentManager._prevTemplateObj.path;
                        newTemplate.prevTemplateXML = ContentManager._prevTemplateObj.xml;
                    }
                    
                    newTemplate.thisTemplateName    = candidate.templateName;
                    newTemplate.passedVariables     = ContentManager._passedVariables;
                    
                    var fullPath = ContentManager.composeFullPathFromXML(candidate.xml);
                    
                    // save in the active template list
                    activeTemplates.push({template: newTemplate, path: candidate.path, templateName: candidate.templateName, fullPath: fullPath, xml: candidate.xml});
                    
                    // call template in
                    newTemplate.templateIn();
                    
                    trace(fullPath, "fullPath");
                    _gaq.push(['_trackPageview', fullPath]);
                }
                else
                {
                    ContentManager._blocked = false;
                    ////////trace("unable to find template", "ContentManager");
                }
            }
        }
    }
    else
    {
        ////trace("ContentManager is blocked");
    }
}

ContentManager.extractPath = function(str)
{
    var arr1            = str.split("#");
    var arr2            = arr1[arr1.length - 1].split("/");
    if(arr2[0] == "")
    {
        arr2.shift();
    }
    var newPath         = arr2.join("/");
    return newPath;
}

ContentManager.sortTemplateList = function(templates)
{
    var i               = 0;
    var l               = templates.length;
    var s               = 0;
    var l2              = 0;
    var rArr            = new Array();
    var sortItem        ;
    var insertPos       ;
    var level           = 0;
    
    for(i = 0; i < l; i += 1)
    {
        sortItem = templates[i];
        level = sortItem.level;
        
        //////////trace(sortItem.path, "sortItem.path");
        //////////trace(level, "level");
        
        l2 = rArr.length;
        insertPos = 200;
        
        for(s = 0; s < l2; s += 1)
        {
            if(level > rArr[s].level)
            {
                insertPos = s + 1;
                break;
            }       
        }
        
        if(insertPos == 200)
        {
            insertPos = rArr.length;
        }
        //////////trace(insertPos, "insertPos");
        rArr.splice(insertPos, 0, sortItem);
    }
    
    // test
    /*
    for(i = 0; i < l; i += 1)
    {
        sortItem = rArr[i];
        //////////trace(sortItem.templateName, "sortItem.templateName");
        //////////trace(sortItem.level, "sortItem.level");
    }
    */
    
    return rArr;
}

ContentManager.findTemplateFromName = function(templateName)
{
    var i           ;
    var register    = ContentManager._templateRegister;
    var l           = register.length;
    var regObj      ;
    var JSClass     ;
    
    for(i = 0; i < l; i += 1)
    {
        regObj = register[i];
        
        if(regObj.templateName == templateName)
        {
            JSClass = regObj.JSClass;           
            break;
        }
    }
    
    return JSClass;
}   

ContentManager.composeFullPathFromXML = function(xml)
{
    var rPath       = "/" + $(xml).attr("data-path");
    var parent      = $(xml).parent();
    var loops       = 0;
    
    while($(parent).attr("data-path"))
    {
        rPath = "/" + $(parent).attr("data-path") + rPath;
        
        parent = $(parent).parent();
        
        // prevent eternal loop
        loops += 1;
        if(loops > 10)
        {
            break;
        }
    }
    
    return rPath;
}

ContentManager.findTemplateLevelFromName = function(templateName)
{
    var rLevel                          = 0;
    var levelFromTemplateName           = templateName.match(/\d+/);
    
    if(levelFromTemplateName)
    {
        rLevel = levelFromTemplateName;
    }
    
    // ////////trace(levelFromTemplateName, "levelFromTemplateName");
    
    return rLevel;
}

ContentManager.findContent = function(pathArr)
{
    // ////////trace("ContentManager.findContent();");
    // ////////trace(pathArr, "pathArr");
    
    var searchPath      = pathArr[0];
    var rXML            = $(ContentManager._xml);
    var currPath        = "";
    var currXML         ;
    var i               ;
    var l               ;
    var found           ;
    var xmlChildren     ;
    var loop            = 0;
    
    while(searchPath)
    {
        // ////////trace("---------------------------");
        // ////////trace(searchPath, "searchPath");
        
        xmlChildren = $(rXML).children();
        l = xmlChildren.length;
        
        for(i = 0; i < l; i += 1)
        {
            // ////////trace("-------------");
            // ////////trace(i, "loop");
            
            currXML = xmlChildren[i];
            currPath = $(currXML).attr("data-path");
            
            // ////////trace(currPath, "currPath");
            
            if(currPath == searchPath)
            {
                // ////////trace("path found");
                pathArr.shift();
                searchPath = pathArr[0];
                rXML = currXML;
                found = true;
                break;
            }
        }
            
        if(!found || !searchPath || loop > 10)
        {
            searchPath = null;
        }
        
        loop += 1;
    }
    
    return rXML;
}
