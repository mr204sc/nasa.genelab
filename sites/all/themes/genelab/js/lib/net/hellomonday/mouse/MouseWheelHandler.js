/**
 * 
 * @author Torben
 * 
 * @param {Object} - element to add mousescroll to.
 * @param {Function} - Callback will recieve the delta.
 * @param {Boolean} - Limit the scroll to prevent unintended scrolling with mice that support continuous scrolling. 
 * 
 */
function MouseWheelHandler(target, callback, limit)
{
	var instance = this;
	
	var scrollTimer;
	var scrollAllowed = true;
	
	target.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel, false);
	
	function onMouseWheel(event)
	{
		var delta = 0;
		if (!event)/* For IE. */
			event = window.event;
		if (event.wheelDelta)
		{
			/* IE/Opera. */
			delta = event.wheelDelta / 120;
		}
		else if (event.detail)
		{
			/** Mozilla case. */
			/** In Mozilla, sign of delta is different than in IE.
			 * Also, delta is multiple of 3.
			 */
			delta = -event.detail / 3;
		}
		/** If delta is nonzero, handle it.
		 * Basically, delta is now positive if wheel was scrolled up,
		 * and negative, if wheel was scrolled down.
		 */
		if (delta)
		{
			if(scrollAllowed)
			{
				callback(delta);
			}
			
			// Slow down scroll interval
			if(limit)
			{
				if (scrollTimer)
				{
					clearInterval(scrollTimer);
				}
	
				scrollAllowed = false;
				scrollTimer = setInterval(resetScrollTimer, 30);
			}
		}

		/** Prevent default actions caused by mouse wheel.
		 * That might be ugly, but we handle scrolls somehow
		 * anyway, so don't bother here..
		 */
		if(event.preventDefault)
		{
			event.preventDefault();
			event.returnValue = false;
		}
	}
	
	function resetScrollTimer()
	{
		scrollAllowed = true;
	}
	
	instance.kill = function()
	{
		target.removeEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel, false);
	}
	
	return instance;
}
