/*
* SmartObject
*
* Makes it possible to access new properties of the HTMLElement - automatically uses 3D transform if that is available
*
* Getters and setters:
*  _x                   : Moves the item on the X position
*  _y                   : Moves the item on the Y position
*  _z                   : Moves the item on the Y position
*  _rotation            : Rotates the item X degress
*  _rotationX           : Rotates the item on the X axis
*  _rotationY           : Rotates the item on the Y axis
*  _rotationZ           : Rotates the item on the Z axis
*  _opacity             : Changes the opacity of the element
*  _transformOriginX    : Defines the registration point of the element
*  _transformOriginY    : Defines the registration point of the element
*  _skewX               : Skews the object on the X axis (not available in IE8 and below)
*  _skewY               : Skews the object on the Y axis (not available in IE8 and below)
*
* IE8: Only _x, _y, _rotation, _opacity, _transformOriginX, _transformOriginY is supported
*
*
* CSS Filters (only available in Chrome 18+, Safari 6+ - should also be in iOS6+)
*  _blur                : Distance in pixels 0 - XX pixels
*  _greyscale           : Greyscale percent 0 - 1
*  _hue                 : Hue Rotation - -180 - 180
*  _sepia               : Sepia percent 0 - 1
*  _saturate            : Saturate 0 - 1
*  _invert              : Invert (values = ?)
*  _brightness          : Brightness percent 0 - 1
*  _contrast            : Contrast percent 0 - 1
*
*
*
*
* Functions:
*  _getWidth()          : Gets the "real" offset width of the object (even if its not added to the DOM)
*  _getHeight()         : Gets the "real" offset eight of the object (even if its not added to the DOM)
*
*
*
* TODO:

* Links:
* Good resource on extending the dom in IE8 - http://msdn.microsoft.com/en-us/library/dd229916(VS.85).aspx
* Getter and Setters in IE5.5+ = http://code.google.com/p/vbclass/*
* // IE Matrix filter: http://msdn.microsoft.com/en-us/library/ms533014%28VS.85,loband%29.aspx*
*  // CSS Filter - http://davidwalsh.name/css-filters
*/

/**
 * Creates an animateable DIV
 * @param {target} Is optional - but converts an existing object to a "SmartObject"
 **/
function SmartObject(target)
{
    var defaultSmartObjectValues =
    {
        _x: 0, _y: 0, _z: 0, _rotation: 0, _rotationX: 0, _rotationY: 0, _rotationZ: 0, _scaleX: 1, _scaleY: 1, _opacity: 1
    };
    if (target == null)
    {
        var mainClip = document.createElement("div");
        mainClip.style.position = "absolute";
        mainClip.style.left = "0px";
        mainClip.style.top = "0px";
    }
    else
    {
        mainClip = target;
        mainClip.style.position = "absolute";

        // Extract the current values and pass that to the SmartObject
        if (mainClip.style.left)
        {
            defaultSmartObjectValues._x = parseInt(mainClip.style.left);
        }

        if (mainClip.style.top)
        {
            defaultSmartObjectValues._y = parseInt(mainClip.style.top);
        }

        if (mainClip.style.opacity)
        {
            defaultSmartObjectValues._opacity = mainClip.style.opacity;
        }
    }
    return SmartObject.convert(mainClip, defaultSmartObjectValues);
}

// Converts an object to an animateable object
SmartObject.convert = function(mainClip, defaultSmartObjectValues)
{
    // Used to handle the IE7 "setters"
    mainClip._setter = new Array();

    // Used for IE7 and IE8 rotation compensation (being changed and set through FixIE.js)
    mainClip.___compensateX = 0;
    mainClip.___compensateY = 0;

    mainClip.___setInitialValues = true;
    mainClip.___isSmartObject = true;
    mainClip.___timeoutRunning = false;
    mainClip.___firstRun = true;
    mainClip.___hasHadAdvancedStyle = false;

    // Where should the transformation point be?
    mainClip._transformOriginX = 50;
    mainClip._transformOriginY = 50;

    // Add the different funcitons to the object
    // Get width
    mainClip._getWidth = function()
    {
        var returnWidth = 0;
        if (mainClip.parentNode == null)
        {
            document.body.appendChild(mainClip);
            returnWidth = mainClip.offsetWidth;
            document.body.removeChild(mainClip);
        }
        else if (mainClip.offsetWidth == 0)
        {
            var currentParentNode = mainClip.parentNode;
            document.body.appendChild(mainClip);
            returnWidth = mainClip.offsetWidth;
            currentParentNode.appendChild(mainClip);
        }
        else
        {
            returnWidth = mainClip.offsetWidth;
        }
        return returnWidth;
    };
    // Get height
    mainClip._getHeight = function()
    {
        var returnWidth = 0;
        if (mainClip.parentNode == null)
        {
            document.body.appendChild(mainClip);
            returnWidth = mainClip.offsetHeight;
            document.body.removeChild(mainClip);
        }
        else if (mainClip.offsetWidth == 0)
        {
            var currentParentNode = mainClip.parentNode;
            document.body.appendChild(mainClip);
            returnWidth = mainClip.offsetHeight;
            // trace("returnWidth : " + returnWidth)
            currentParentNode.appendChild(mainClip);
        }
        else
        {
            returnWidth = mainClip.offsetHeight;
        }
        return returnWidth;
    };

    function update()
    {
        SmartObject.updateAllDelayed(mainClip);
    }

    function setProperty(target, kind, value)
    {
        // this[kind] = value;
        SmartObject.extend(target, kind, update);
        target[kind] = value;
    }

    // IE7 specific fix
    if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 7)
    {
        // problem: on propertychange is only fired when the object is on the stage
        mainClip.attachEvent("onpropertychange", update);
    }

    setProperty(mainClip, "_x", defaultSmartObjectValues._x);
    setProperty(mainClip, "_y", defaultSmartObjectValues._y);
    setProperty(mainClip, "_z", defaultSmartObjectValues._z);

    setProperty(mainClip, "_scaleX", defaultSmartObjectValues._scaleX);
    setProperty(mainClip, "_scaleY", defaultSmartObjectValues._scaleY);
    setProperty(mainClip, "_opacity", defaultSmartObjectValues._opacity);

    setProperty(mainClip, "_rotationX", defaultSmartObjectValues._rotationX);
    setProperty(mainClip, "_rotationY", defaultSmartObjectValues._rotationY);
    setProperty(mainClip, "_rotationZ", defaultSmartObjectValues._rotationZ);
    setProperty(mainClip, "_rotation", defaultSmartObjectValues._rotation);

    setProperty(mainClip, "_skewX", 0);
    setProperty(mainClip, "_skewY", 0);

    // CSS filters
    setProperty(mainClip, "_blur", 0);
    setProperty(mainClip, "_grayscale", 0);
    setProperty(mainClip, "_hue", 0);
    setProperty(mainClip, "_sepia", 0);
    setProperty(mainClip, "_saturate", 1);
    setProperty(mainClip, "_invert", 0);
    setProperty(mainClip, "_brightness", 0);
    setProperty(mainClip, "_contrast", 1);

    // Everything set up the object - allow it to be updated
    mainClip.___firstRun = false;
    // Update the SmartObject with all its properties
    SmartObject.update(mainClip);

    return mainClip;
};

// Enable getter and setters in IE7 and 8
SmartObject.extend = function(target, value, useFunction)
{
    if (BrowserDetect.BROWSER_NAME == "Explorer")
    {
        if (BrowserDetect.BROWSER_VERSION >= 8)
        {
            Object.defineProperty(target, value,
            {
                get: function()
                {
                    return value;
                }, set: function(setValue)
                {
                    if (target != "_x" && target != "_y")
                    {
                        target.___hasHadAdvancedStyle = true;
                    }
                    value = setValue;
                    useFunction(setValue);
                }
            })
        }
        else
        {
            // IE7 (might work in IE6 as well - this have not been testet though
            target._setter.push(value);
        }
    }
    else
    {
        // Mozilla, Webkit and other browsers
        target.__defineGetter__(value, function()
        {
            return value;
        });

        target.__defineSetter__(value, function(val)
        {
            if (value != "_x" && value != "_y")
            {
                target.___hasHadAdvancedStyle = true;
            }
            value = val;
            useFunction(val);
        });
    }
}

SmartObject.updateAllDelayed = function(target)
{
    SmartObject.updateAllCheck(target);
}

SmartObject.updateAllCheck = function(target)
{
    target.___timeoutRunning = false;

    SmartObject.update(target);
}
/*
 * Updates the SmartObjects values - repaint and redraws the element
 */
SmartObject.update = function(mainClip)
{
    // update all
    if (mainClip.___firstRun == false)
    {
        var isIE8andBelow = false;
        if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 8)
        {
            isIE8andBelow = true;
        }
        // Set transformation on all browsers except for IE8 and IE7
        if (isIE8andBelow == false)
        {
            var useTranslateString = "translate(" + Math.round(mainClip._x) + "px, " + Math.round(mainClip._y) + "px)";
            // If the browser have 3D support - use translate3d instead
            if (BrowserDetect.TRANSLATE3D_SUPPORT == true)
            {
                useTranslateString = "translate3d(" + Math.round(mainClip._x) + "px, " + Math.round(mainClip._y) + "px, " + Math.round(mainClip._z) + "px) rotateX(" + mainClip._rotationX + "deg) rotateY(" + mainClip._rotationY + "deg) rotateZ(" + mainClip._rotationZ + "deg)";
            }

            var useTransformation = useTranslateString + " rotate(" + mainClip._rotation + "deg) scale(" + mainClip._scaleX + ", " + mainClip._scaleY + ") skewX(" + mainClip._skewX + "deg) skewY(" + mainClip._skewY + "deg)";
            var useTransformOrigin = mainClip._transformOriginX + "% " + mainClip._transformOriginY + "%";

            if (mainClip._blur != 0 || mainClip._grayscale != 0 || mainClip._sepia != 0 || mainClip._contrast != 1 || mainClip._brightness != 0 || mainClip._invert != 0 || mainClip._saturate != 1)
            {
                mainClip.style.webkitFilter = "blur(" + mainClip._blur + "px) grayscale(" + mainClip._grayscale + ") sepia(" + mainClip._sepia + ") contrast(" + mainClip._contrast + ") brightness(" + mainClip._brightness + ") invert(" + mainClip._invert + ") saturate(" + mainClip._saturate + ")"
            }
            else
            {
                mainClip.style.webkitFilter = "";
            }
            mainClip.style.opacity = mainClip._opacity;
            mainClip.style.msTransformOrigin = mainClip.style.MozTransformOrigin = mainClip.style.WebkitTransformOrigin = mainClip.style.OTransformOrigin = mainClip.style.transformOrigin = useTransformOrigin;
            mainClip.style.msTransform = mainClip.style.MozTransform = mainClip.style.WebkitTransform = mainClip.style.OTransform = mainClip.style.transform = useTransformation;
        }
        else
        {
            if (mainClip._rotation != 0 || mainClip._scaleX != 1 || mainClip._scaleY != 1 || mainClip._opacity != 1)
            {
                mainClip.___hasHadAdvancedStyle = true;
            }

            if (mainClip.___hasHadAdvancedStyle == true)
            {
                var removeItemAgain = false;
                // If IE7 the item needs to be on the stage to receive "onpropertychange" - therefore we force it to be on the stage
                var storeParent = null;
                if (mainClip.parentNode == null)
                {
                    var tempDiv = document.createElement("div");
                    tempDiv.appendChild(mainClip);
                    document.body.appendChild(tempDiv);
                    removeItemAgain = true;
                }
                else
                {
                    storeParent = mainClip.parentNode;
                }

                var filter = mainClip.style.filter;
                // remove the current filter to get the original offsetWidth and offsetHeight
                mainClip.style.filter = '';

                var _offsetWidth = mainClip.offsetWidth;
                var _offsetHeight = mainClip.offsetHeight;
                mainClip.style.filter = filter;

                // if (removeItemAgain == true)
                // {
                //storeParent = mainClip.parentNode;
                mainClip.parentNode.removeChild(mainClip);
                // re-do the filter

                // }

                // FIX for doing transformations in IE8 and IE7
                var matrix = null;

                // FIXME --- MOVE OUT - and create FixIE.IEMatrix...
                function createIEMatrixString(M, mainClip)
                {
                    var M11 = M.e(1, 1) * mainClip._scaleX;
                    var M12 = M.e(1, 2) * mainClip._scaleY;
                    var M21 = M.e(2, 1) * mainClip._scaleX;
                    var M22 = M.e(2, 2) * mainClip._scaleY;
                    matrix = Matrix.create([[M11, M12, 0], [M21, M22, 0], [0, 0, 1]]);
                    return 'M11=' + M11 + ', M12=' + M12 + ', M21=' + M21 + ', M22=' + M22;
                }

                var addOpacityString = " progid:DXImageTransform.Microsoft.Alpha(opacity=" + Math.round(mainClip._opacity * 100) + ")";

                // var addAlphaFilter = "";
                /*if (mainClip.___isAlphaPNG == true) {
                 addAlphaFilter = mainClip.___alphaPNGFilter;
                 }*/
                var addMatrixStr = "";
                if (mainClip._rotation != 0 || mainClip._scaleX != 1 || mainClip._scaleY != 1)
                {
                    var matrix_str = createIEMatrixString(Matrix.Rotation(FixIE.degreesToRadians(mainClip._rotation)), mainClip);
                    addMatrixStr = "progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', " + matrix_str + ", filterType='nearest neighbor')";
                }
                var filter_str = addMatrixStr + addOpacityString;
                // + addAlphaFilter;
                //progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', " + matrix_str + ")" + addOpacityString; // progid:DXImageTransform.Microsoft.gradient(startColorstr=#110000FF,endColorstr=#110000FF)
                if (mainClip.style.background == null)
                {
                    mainClip.style.background = "transparent";
                }
                mainClip.style.MsFilter = filter_str;
                // IE 8
                mainClip.style.filter = filter_str;
                // IE 6/7
                mainClip.style["zoom"] = "1";
                if (addMatrixStr != "")
                {
                    // document.body.appendChild(mainClip);
                    FixIE.transformOrigin(mainClip, matrix, _offsetWidth, _offsetHeight);
                    FixIE.fixBoundaryBug(mainClip, matrix, _offsetWidth, _offsetHeight);
                    //document.body.removeChild(mainClip);
                }
                mainClip.style.left = mainClip._x + mainClip.___compensateX + "px";
                mainClip.style.top = mainClip._y + mainClip.___compensateY + "px";

                //    mainClip.style.display = "block";
                if (storeParent != null)
                {
                    storeParent.appendChild(mainClip);
                    if (removeItemAgain == true)
                    {
                        tempDiv.parentNode.removeChild(tempDiv);
                        tempDiv = null;
                    }
                }
                if (mainClip._rotation == 0 && mainClip._scaleX == 1 && mainClip._scaleY == 1 && mainClip._opacity == 1)
                {
                    mainClip.___compensateX = 0;
                    mainClip.___compensateY = 0;
                    mainClip.___hasHadAdvancedStyle = false;
                }
            }
            else
            {
                var storeParent = null;
                if (mainClip.parentNode == null)
                {
                    storeParent = mainClip.parentNode;
                    mainClip.parentNode.removeChild(mainClip);
                }

                mainClip.style.left = mainClip._x + mainClip.___compensateX + "px";
                mainClip.style.top = mainClip._y + mainClip.___compensateY + "px";
                if (storeParent != null)
                {
                    storeParent.appendChild(mainClip);
                }

            }
        }
    }
};
