DOMUtils = new Object();

DOMUtils.getChildIndex = function(p_element)
{
    // trace("DOMUtils.getChildIndex();");
    if (!p_element)
    {
        // trace("p_element is null");
        return -1;
    }

    var useParent = p_element.parentNode;

    if (!useParent)
    {
        // trace("parent was not found");
        return -1;
    }

    var children = useParent.children;
    var length = children.length;
    for (var i = 0; i < length; i++)
    {
        if (children[i] === p_element)
        {
            return i;
        }
    }
};

DOMUtils.getOffset = function(el)
{
    var _x = 0;
    var _y = 0;
    while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop))
    {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        // chrome/safari
        if (el.parentNode)
        {
            el = el.parentNode;
        }
        else
        {
            // firefox/IE
            el = el.offsetParent;
        }
    }

    var returnObject =
    {
        top: _y, left: _x
    };
    return returnObject;
};
/**
 * Returns the window width and height.
 *
 * @return {Object} an object containing the window width and height.
 */
DOMUtils.getWindowSize = function()
{
    var windowSizeObject =
    {
    };
    var windowWidth = 0;
    var windowHeight = 0;

    if ( typeof (window.innerWidth) == 'number')
    {
        windowWidth = window.innerWidth;
        windowHeight = window.innerHeight;
    }
    else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
    {
        //IE 6+ in 'standards compliant mode'
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    }

    windowSizeObject.width = windowWidth;
    windowSizeObject.height = windowHeight;

    return windowSizeObject;
};

/**
 * Returns the scroll position of the html element.
 *
 * @return {Object} an object containing the horizontal and vertical scroll
 *     offset, in pixels.
 */
DOMUtils.getScrollPosition = function()
{
    var position =
    {
    };

    if ( typeof window.pageYOffset != 'undefined')
    {
        position.scrollOffsetX = window.pageXOffset;
        position.scrollOffsetY = window.pageYOffset;
    }
    else if ( typeof document.documentElement.scrollTop != 'undefined' && document.documentElement.scrollTop > 0)
    {
        position.scrollOffsetX = document.documentElement.scrollLeft;
        position.scrollOffsetY = document.documentElement.scrollTop;
    }
    else if ( typeof document.body.scrollTop != 'undefined')
    {
        position.scrollOffsetX = document.body.scrollLeft;
        position.scrollOffsetY = document.body.scrollTop;
    }

    return position;
};

/**
 * Helper function to get the x and y position of an element.
 *
 * @param {Element} el - A DOM element.
 * @return {object} An object containing the global position of the element.
 */
DOMUtils.localToGlobal = function(el)
{
    var lx = 0;
    var ly = 0;

    while (el != null)
    {
        lx += el.offsetLeft;
        ly += el.offsetTop;
        el = el.offsetParent;
    }
    var returnObject =
    {
        x: lx, y: ly
    };
    return returnObject;

};

DOMUtils.getAbsoluteOffset = function(el)
{

    var offsetLeft = 0;
    var offsetTop = 0;

    do
    {
        if (!isNaN(el.offsetLeft))
        {
            offsetLeft += el.offsetLeft;
        }
    }
    while( el = el.offsetParent );

    do
    {
        if (!isNaN(el.offsetTop))
        {
            offsetTop += el.offsetTop;
        }
    }
    while( el = el.offsetParent );
    var returnObject =
    {
        x: offsetLeft, y: offsetTop
    };
    return returnObject;
};
/**
 * Returns the document height.
 *
 * @return {Object} And object containign the
 *    document width and height, in pixels.
 */
DOMUtils.getDocumentSize = function()
{
    var position =
    {
    };
    var currentDocument = document;

    var documentHeight = Math.max(Math.max(currentDocument.body.scrollHeight, currentDocument.documentElement.scrollHeight), Math.max(currentDocument.body.offsetHeight, currentDocument.documentElement.offsetHeight), Math.max(currentDocument.body.clientHeight, currentDocument.documentElement.clientHeight));

    var documentWidth = Math.max(Math.max(currentDocument.body.scrollWidth, currentDocument.documentElement.scrollWidth), Math.max(currentDocument.body.offsetWidth, currentDocument.documentElement.offsetWidth), Math.max(currentDocument.body.clientWidth, currentDocument.documentElement.clientWidth));

    position.width = documentWidth;
    position.height = documentHeight;

    return position;
};

DOMUtils.getMousePosition = function(e)
{
    var d = document.documentElement;
    var b = document.body;

    if (!e)
    {
        e = window.event;
    }

    if (window.pageXOffset != undefined)
    {
        return [e.clientX + window.pageXOffset, e.clientY + window.pageYOffset];
    }
    else
    {
        return [e.clientX + d.scrollLeft + b.scrollLeft, e.clientY + d.scrollTop + b.scrollTop];
    }
};

DOMUtils.getElementsByAttribute = function(attrib, value, context_node, tag)
{
    var nodes = [];
    if (context_node == null)
        context_node = this;
    if (tag == null)
        tag = '*';
    var elems = context_node.getElementsByTagName(tag);

    for (var i = 0; i < elems.length; i += 1)
    {
        if (value)
        {
            if (elems[i].hasAttribute(attrib) && elems[i].getAttribute(attrib) == value)
                nodes.push(elems[i]);
        }
        else
        {
            if (elems[i].hasAttribute(attrib))
                nodes.push(elems[i]);
        }
    }
    return nodes;
};