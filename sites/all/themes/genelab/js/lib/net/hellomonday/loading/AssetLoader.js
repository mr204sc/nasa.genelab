/**
 * @author Torben
 *
 * @fileoverview Provides Asset loading
 *  Usage Examples:
 *      var ARRAY_OF_IMAGES = ["IMAGE1.EXTENSION", "IMAGE2.EXTENSION"];
 *      AssetLoader.init(FOLDER_TO_LOAD_IMAGES_FROM);
 *      var GROUP_TO_LOAD = new AssetGroup(ARRAY_OF_IMAGES, {onComplete: CALLBACK_WHEN_ALL_IMAGES_ARE_LOADED, onUpdate: CALLBACK_WHEN_AN_IMAGE_IS_LOADED_RETURNS_PERCENTAGE});
 *      AssetLoader.loadGroup();
 *      AssetLoader.getAsset("IMAGE1.EXTENSION");
 *
 *  Usage Examples:
 *      var loadAssetsArray = ["intro_bg.jpg", "gfx/intro_berries_left.png", "intro_berries_right.png", "intro_coffee.png", "intro_halo.png", "intro_line.png", "intro_man.png", "intro_naora.png"];
 *      AssetLoader.init("assets/images/");
 *      var myAssetsGroup = new AssetGroup(loadAssetsArray, {onComplete: Preloader.done, onUpdate: Preloader.onUpdatePercent, stripPath: true, customPath: "test/testing/path/"});
 *      AssetLoader.loadGroup(myAssetsGroup);
 *      AssetLoader.getAsset("gfx/intro_berries_left.png");
 *
 *
 * @dependencies lib/net/hellomonday/hmframework/FixIE (for making it work in IE8)
 *
 * @browsers: All (however only tested in: FF3.6+, Chrome 11+, Safari 5+)
 *
 * 
 * 
 * @author Torben
 * 
 * @browsers: All
 * 
 */
(function AssetLoader()
{
    var _instance =
    {
    };

    /**
     * @param {String} assetPath, specifies the root path of the assets.
     */
    _instance.init = function(assetPath)
    {
        if (!_instance.initialized)
        {
            _instance.assets = [];
            _instance.que = [];
            _instance.busy = false;
            _instance.assetPath = assetPath;
            _instance._percentageLoaded = 0;
            _instance.initialized = true;
        }

    };
    /**
     * @param group, AssetGroup with an array containing assets to load.
     */
    _instance.loadGroup = function(group)
    {
        if (!_instance.initialized)
        {
            throw new Error("You need to initialize Assetloader before loading a group");
            return;
        }

        if (!_instance.busy)
        {
            _instance.busy = true;
            _instance.que.push(group);
            internalLoad();
        }
        else
        {
            _instance.que.push(group);
        }
    };
    
    function internalLoad()
    {
        _instance._percentageLoaded = 0;

        var data = _instance.que[0].data;
        var onComplete = _instance.que[0].onComplete;
        var customPath = _instance.que[0].customPath;
        var onUpdate = _instance.que[0].onUpdate;

        var dataURIFile = _instance.que[0].dataURIFile;
        var dataURIFilePercentageOfTotal = _instance.que[0].dataURIFilePercentageOfTotal;
        if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 9)
        {
            // IE8 has a max kb of 32.000 bytes - 32 kb - and we therefore load the assets by themselves
            // IE9 doesnt support "progress" on XMLHttpRequest
            dataURIFile = null;
            dataURIFilePercentageOfTotal = 0;
        }

        var i = 0;
        var l = data.length;
        var assetsLoaded = 0;
        var assetsToLoad = [];
        var assetLoadCount = 0;
        var asset;
        var id;

        function readyToLoadIndividualAssets()
        {
            for (i; i < l; i++)
            {
                id = data[i];
                if (!_instance.assetExist(id))
                {
                    assetsToLoad.push(data[i]);
                    assetLoadCount++;
                }
                else
                {

                }

            }
            load();
        }

        if (dataURIFile != null)
        {
            var loadRequest = new XMLHttpRequest();
            function dataURILoaded(event)
            {
                loadRequest.removeEventListener(Event.LOAD, dataURILoaded, true);
                var getData = event.currentTarget.responseText;
                var makeDataToArray = getData.split("|");
                for (var i = 0; i < makeDataToArray.length - 1; i++)
                {
                    var currentItem = makeDataToArray[i];
                    if (i % 2 == 0)
                    {
                        //  trace("currentItem : " + currentItem);
                        var asset = new Image();
                        asset.src = makeDataToArray[i + 1];
                        var id = currentItem.toString(); //stripExtension(currentItem.toString());
                        // trace("set ID : " + id)
                        _instance.assets.push(
                        {
                            asset: asset, id: id, isURI: true
                        });

                    }

                }
                getData = null;
                loadRequest = null;
                readyToLoadIndividualAssets();
            }

            function dataURIProgress(event)
            {
                if (event.lengthComputable)
                {
                    _instance._percentageLoaded = (event.loaded / event.total) * dataURIFilePercentageOfTotal;
                }
                else
                {
                    _instance._percentageLoaded = dataURIFilePercentageOfTotal;
                }

                if (onUpdate != null)
                {
                    onUpdate(_instance._percentageLoaded);
                }
            }


            loadRequest.addEventListener(ProgressEvent.PROGRESS, dataURIProgress, true);
            loadRequest.addEventListener(Event.LOAD, dataURILoaded, true);
            loadRequest.open("GET", dataURIFile, false);
            if (loadRequest.overrideMimeType)
            {
                loadRequest.overrideMimeType("text/plain; charset=x-user-defined");
            }
            loadRequest.send(null);

        }
        else
        {

            readyToLoadIndividualAssets();
        }

        function load()
        {
            if (assetsToLoad.length > 0)
            {
                var i = 0;
                var l = assetsToLoad.length;

                for (i; i < l; i++)
                {
                    asset = new Image();
                    id = assetsToLoad[i];

                    asset.addEventListener("load", assetInternalLoaded, true);
                    asset.addEventListener("error", assetInternalLoadError, true);
                    asset.style.position = "absolute";

                    // Check if path contains forward slash.
                    var path = assetsToLoad[i];
                    var regx = new RegExp('\/');

                    if (regx.test(path))
                    {
                        // Strip the path from the id if specified (otherwise the id will be the full path)
                        if (_instance.que[0].stripPath)
                        {
                            id = stripPath(assetsToLoad[i]);
                           // id = id;
                        }
                    }

                    // Check if custom path is provided, else use default path
                    if (customPath != null)
                    {
                        url = customPath + assetsToLoad[i];
                    }
                    else
                    {

                        url = _instance.assetPath + assetsToLoad[i];
                    }

                    _instance.assets.push(
                    {
                        asset: asset, id: id, isURI: false
                    });

                    asset.src = url;
                }
            }
            else
            {
                assetLoaded();
            }
        }

        function assetInternalLoaded(event)
        {
            event.currentTarget.removeEventListener("load", assetLoaded, true);
            event.currentTarget.removeEventListener("error", assetLoaded, true);
            assetsLoaded++;
            assetLoaded();
        }

        function assetInternalLoadError(event)
        {
            // throw new Error("AssetLoader -> Error, can't load " + event.currentTarget.src);
            event.currentTarget.removeEventListener("load", assetLoaded, true);
            event.currentTarget.removeEventListener("error", assetLoaded, true);
            assetsLoaded++;
            assetLoaded();
        }

        function assetLoaded()
        {
            _instance._percentageLoaded = Math.ceil((assetsLoaded / assetLoadCount) * (100 - dataURIFilePercentageOfTotal) + dataURIFilePercentageOfTotal);

            if (onUpdate != null)
            {
                onUpdate(_instance._percentageLoaded);
            }

            if (assetsLoaded == assetLoadCount)
            {
                // Trigger callback on group
                if (onComplete)
                {
                    onComplete();
                }
                _instance.que.shift();

                if (_instance.que.length > 0)
                {
                    setTimeout(internalLoad, 100);
                }
                else
                {
                    _instance.busy = false;
                }
            }
        }

    }

    /**
     * @param id, search string. Looks for asset with matching id
     * @returns cloned image
     **/
    _instance.getAsset = function(id)
    {
        if (!_instance.initialized)
        {
            throw new Error("You need to initialize Assetloader before attempting to retrive an asset - id: " + id);
            return;
        };

        // Strip file extension (all id's are kept without file extension')
        //id = id;

        var found = false;
        var i = 0;
        var l = _instance.assets.length;
        var asset = null;
        var existingAsset;
        var assetWidth;
        var assetHeight;

        for (i; i < l; i++)
        {
            if (id === _instance.assets[i].id)
            {
                existingAsset = _instance.assets[i].asset;

                found = true;

                asset = existingAsset.cloneNode(true);
                asset.style.position = "absolute";

                if (id.indexOf("@2") > -1)
                {
                    // If has @2 in name - automatically resize them to half their size
                    assetWidth = existingAsset.width / 2;
                    assetHeight = existingAsset.height / 2;
                }
                else
                {
                    assetWidth = existingAsset.width;
                    assetHeight = existingAsset.height;
                }

                asset.width = assetWidth;
                asset.height = assetHeight;

                asset._orgW = assetWidth;
                asset._orgH = assetHeight;
                asset._ratio = assetHeight / assetWidth;

                return asset;
            }
        }

        if (found !== true)
        {
            throw new Error("AssetLoader -> asset with id: " + id + ", not found");
        }

        return asset;
    };
    
    // Helper methods
    _instance.assetExist = function(id)
    {
        var i = 0;
        var l = _instance.assets.length;

        for (i; i < l; i++)
        {
            if (id === _instance.assets[i].id)
            {
                return true;
                break;
            }
        }

        return false;
    };
    
    // function stripExtension(str)
    // {
        // var rVal = "";
        // if (str)
        // {
            // var stripped = str.split(".");
            // rVal = stripped[0];
        // }
        // return rVal;
    // };

    function stripPath(str)
    {
        var rVal;

        if (str)
        {
            var regX = new RegExp('^.*[\/]', "g");
            rVal = str.replace(regX, "");

        }

        return rVal;
    };

    window.AssetLoader = _instance;
})();

/**
 * Data object to be used in conjunction with the AssetLoader
 *
 * @param {Array} data, Array containing assets to load (if a full relative path is specified, this path will be used instead of the default/custom)
 * @param {Object} params, Available params: {Function} onComplete, AssetLoader will run the callback when its done loading the group
 * 										     {String} customPath, optional, provide a custom load path (otherwise it will use the default path provided when AssetLoader is initialized)
 * 											 {Function} onUpdate, AssetLoader will run this method every time there is an update in the load progress and return a percentage value
 * 											 {Boolean} stripPath, indicating if the path should be stripped from the id
 */
function AssetGroup(data, params)
{
    var instance = this;

    instance.data = data;

    if (params.onComplete)
    {
        instance.onComplete = params.onComplete;
    }
    if (params.customPath !== null)
    {
        instance.customPath = params.customPath;
    }
    if (params.onUpdate)
    {
        instance.onUpdate = params.onUpdate;
    }
    if (params.dataURIFile)
    {
        instance.dataURIFile = params.dataURIFile;
    }
    if (params.dataURIFilePercentageOfTotal)
    {
        instance.dataURIFilePercentageOfTotal = params.dataURIFilePercentageOfTotal;
    }
    else
    {
        instance.dataURIFilePercentageOfTotal = 0;
    }
    if (params.stripPath)
    {
        instance.stripPath = params.stripPath;
    }
    else
    {
        instance.stripPath = false;
    }

    return instance;
};

