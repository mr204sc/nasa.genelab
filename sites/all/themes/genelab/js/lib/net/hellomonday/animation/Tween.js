/**
 * Tween.js
 * Used for all types of animations
 *
 * Possible values:
 * Tween.to(HTMLElement, time in seconds,
 * 	{
 * 	valueName: value,
 * 	valueName: value,
 * 	ease: easingTypes (see PennerEasings.js,
 * 	onComplete: function,
 * 	onCompleteParams:[arrayOfParameters],
 * 	onStart: function,
 * 	onStartParams:[arrayOfParameters])
 * 	onUpdate: function,
 * 	onUpdateParams:[arrayOfParameters])
 * 	bezier:[{
 * 		valueName: value,
 * 		valueName: value
 *  }]
 * 	delay: number of seconds the animation should be delayed with
 * 	})
 *
 * 	Example:
 * 	Tween.to(myDiv, 2, {left: 200, top: 150, ease: Quad.easeInOut});
 *
 *
 *
 */
var Tween =
{
};

// Items that we will add a "px" after and will also set the properties on target.style instead of just the target
Tween.usePXArray = new Array("top", "left", "bottom", "right", "width", "height", "marginLeft", "marginTop", "marginRight", "marginBottom", "paddingTop", "paddingRight", "paddingBottom", "paddingLeft");
Tween.frameRate = 60;

// If ease is not set use the default easing below - easeOutQuad
Tween.defaultEasing = function(t, b, c, d)
{
    return -c * (t /= d) * (t - 2) + b;
};

Tween.looping = false;
// Not used
Tween.itemsCurrentlyAnimating = [];
Tween.defaultOptions =
{
    time: 1,
    ease: Tween.defaultEasing,
    delay: 0,
    bezier: undefined,
    prefix:
    {
    }, // Not used
    suffix:
    {
    }, // not used
    onStart: undefined,
    onStartParams: undefined,
    onUpdate: undefined,
    onUpdateParams: undefined,
    onComplete: undefined,
    onCompleteParams: undefined,
    roundProps: undefined, // roundProps:["x","y"] - not yet implemented
    overwrite: false // Not yet implemented!
};

/**
 * Animates an object
 * @param {target} the target for the animation
 * @param {number} TweenTime - the time for the tween (example 0.1)
 * @param {object} Object - with the values that needs to be animated
 */
Tween.to = function(target, tweentime, options)
{
    if (target === null)
    {
        // If an onComplete functon is added call this function
        if ( typeof options.onComplete == 'function')
        {
            if (options.onCompleteParams)
            {
                options.onComplete.apply(options, options.onCompleteParams);
            }
            else
            {
                options.onComplete();
            }
        }
        return;
    }

    var storeItem =
    {
    };
    storeItem.target = target;
    storeItem.targetProperties =
    {
    };

    // Loop through the object and set default values if they are not set
    for (var key in Tween.defaultOptions)
    {
        if ( typeof options[key] != 'undefined')
        {
            storeItem[key] = options[key];
            delete options[key];
        }
        else
        {
            storeItem[key] = Tween.defaultOptions[key];
        }
    }

    // Define the easing type
    if (storeItem.ease !== undefined)
    {
        storeItem.easing = storeItem.ease;
    }
    else
    {
        storeItem.easing = Tween.defaultEasing;
    }

    function startAnim()
    {
        // Store the start animation values in the object
        var startValue;
        var endValue;
        for (var key in options)
        {
            var addPX = Tween.checkForPX(key);
            if (addPX == true)
            {
                startValue = parseInt(target.style[key]);
                endValue = options[key];
            }
            else
            {
                startValue = target[key];
                endValue = options[key];
            }
            storeItem.targetProperties[key] =
            {
                b: startValue,
                c: (endValue - startValue),
                d: endValue
            };
        }

        storeItem.target.___TweenIsAnimating = true;

        storeItem.startTime = (new Date() - 0);
        storeItem.endTime = tweentime * 1000 + storeItem.startTime;

        // If an onStart funciton is added call this function
        if ( typeof storeItem.onStart == 'function')
        {
            if (storeItem.onStartParams)
            {
                storeItem.onStart.apply(storeItem, storeItem.onStartParams);
            }
            else
            {
                storeItem.onStart();
            }
        }

        // Stop the items currents tweens
        Tween.stopTweening(storeItem.target);
        Tween.itemsCurrentlyAnimating.push(storeItem);
    }

    if (target.___storeTimeout != null)
    {
        clearTimeout(target.___storeTimeout);
        target.___storeTimeout = null;
    }

    target.___storeTimeout = setTimeout(startAnim, storeItem.delay * 1000);
};

Tween.usePXArrayLength = Tween.usePXArray.length;
Tween.checkForPX = function(_checkFor)
{
    var addPX = false;
    for (var i = 0; i < Tween.usePXArrayLength; i++)
    {
        var getValue = Tween.usePXArray[i];
        if (getValue == _checkFor)
        {
            return true;
        }
    }
    return addPX;
};
/*
 * Handles the animation
 */
Tween.eventLoop = function()
{
    var now = (new Date() - 0);
    var _length = Tween.itemsCurrentlyAnimating.length;
    // Loop through all the different animatable objects and see where they should move next
    for (var i = 0; i < Tween.itemsCurrentlyAnimating.length; i++)
    {
        var storeItem = Tween.itemsCurrentlyAnimating[i];
        if (storeItem != undefined)
        {
            var t = now - storeItem.startTime;
            var d = storeItem.endTime - storeItem.startTime;

            if (t >= d)
            {
                for (var property in storeItem.targetProperties)
                {
                    var tP = storeItem.targetProperties[property];
                    storeItem.target.___TweenIsAnimating = false;
                    try
                    {
                        var val = (tP.b + tP.c);
                        var addPX = Tween.checkForPX(property);
                        if (addPX == true)
                        {
                            storeItem.target.style[property] = val + "px";
                        }
                        else
                        {
                            storeItem.target[property] = val;
                        }
                    }
                    catch(e)
                    {
                    }
                }
                Tween.itemsCurrentlyAnimating.splice(i, 1);

                if ( typeof storeItem.onUpdate == 'function')
                {
                    if (storeItem.onUpdateParams)
                    {
                        storeItem.onUpdate.apply(storeItem, storeItem.onUpdateParams);
                    }
                    else
                    {
                        storeItem.onUpdate();
                    }
                }

                if ( typeof storeItem.onComplete == 'function')
                {
                    if (storeItem.onCompleteParams)
                    {
                        storeItem.onComplete.apply(storeItem, storeItem.onCompleteParams);
                    }
                    else
                    {
                        storeItem.onComplete();
                    }
                }
            }
            else
            {
                for (var property in storeItem.targetProperties)
                {
                    var tP = storeItem.targetProperties[property];
                    var val = storeItem.easing(t, tP.b, tP.c, d);
                    if (storeItem.bezier != undefined)
                    {
                        // Only takes the first bezier in the array __(multiple beziers not yet supported)
                        var getBezier = storeItem.bezier[0];
                        if (getBezier[property] != undefined)
                        {
                            var calcTime = Math.min(t / d, 1);
                            val = Tween.bezier(calcTime, val, getBezier[property], tP.d);
                            //maybe tp.d should be tp.c? not sure
                        }
                    }
                    try
                    {
                        var addPX = Tween.checkForPX(property);
                        if (addPX == true)
                        {
                            storeItem.target.style[property] = val + "px";
                        }
                        else
                        {
                            storeItem.target[property] = val;
                        }

                    }
                    catch(e)
                    {
                        trace("[Tween: Error] : Property not found")
                    }
                }

                if ( typeof storeItem.onUpdate == 'function')
                {
                    if (storeItem.onUpdateParams)
                    {
                        storeItem.onUpdate.apply(storeItem, storeItem.onUpdateParams);
                    }
                    else
                    {
                        storeItem.onUpdate();
                    }
                }
            }

        }

        //  if (Tween.itemsCurrentlyAnimating.length > 0) {
        //	setTimeout(Tween.eventLoop, 1000 / Tween.frameRate); //Tween.frameRate
        /*  }
         else {
         Tween.looping = false;
         }*/
    }

    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
    if (!window.requestAnimationFrame)
    {
        setTimeout(Tween.eventLoop, 1000 / Tween.frameRate);
    }
    else
    {
        setTimeout(function()
        {
            requestAnimationFrame(Tween.eventLoop);
            // Drawing code goes here

        }, 1000 / Tween.frameRate);
    }
};

Tween._css3AnimationSupport = false;

Tween.init = function()
{
    Tween._css3AnimationSupport = BrowserDetect.CSS3_TRANSITION_SUPPORT;
    if (Tween._css3AnimationSupport == true)
    {
        if (PennerEasings != undefined || PennerEasings != null)
        {
            PennerEasings.initCSSTransitions();
        }
    }
    Tween.eventLoop();
}

Tween.setNewLoop = function()
{
    //Tween.eventLoop();
}

Tween.init();

Tween.isTweening = function(target)
{
    var isItemTweening = false;
    var _length = Tween.itemsCurrentlyAnimating.length;
    for (var i = 0; i < _length; i++)
    {
        var getItem = Tween.itemsCurrentlyAnimating[i];
        if (getItem.target == target.target)
        {
            //Tween.itemsCurrentlyAnimating.splice(i, 1);
            // Thought we can also just loop through the targetProperties of the item to only abort the duplicate transform items
            isItemTweening = true;
        }
    }

    return isItemTweening;

}

Tween.stopTweening = function(target)
{
    var _length = Tween.itemsCurrentlyAnimating.length;
    for (var i = 0; i < Tween.itemsCurrentlyAnimating.length; i++)
    {
        var getItem = Tween.itemsCurrentlyAnimating[i];
        if (getItem.target == target)
        {
            Tween.itemsCurrentlyAnimating.splice(i, 1);
            return;
            // Thought we can also just loop through the targetProperties of the item to only abort the duplicate transform items
        }
    }
};

/*
* Bezier animation
* http://nocreativity.com/blog/bezier-animation-fun-revisited
*
*/
//
//

// about bezier curves http://en.wikipedia.org/wiki/B%C3%A9zier_curve
// bezier2: t = time, p0 = start value, p1 = bezier point, p2 = end value
// ? idea: bezier3 is used if you need to start a bezier after bezier2. see : Cubic Bézier curves:
/*
 * Four points P0, P1, P2 and P3 in the plane or in three-dimensional space define a cubic Bézier curve.
 * The curve starts at P0 going toward P1 and arrives at P3 coming from the direction of P2.
 * Usually, it will not pass through P1 or P2; these points are only there to provide directional information.
 * The distance between P0 and P1 determines "how long" the curve moves into direction P2 before turning towards P3.
 *
 * Meaning: that t = time, p0 = start value, p1 = bezier point, p2 = previous bezier curve value??, p3 = end value
 */

Tween.bezier = function(t, p0, p1, p2)
{
    return (1 - t) * (1 - t) * p0 + 2 * t * (1 - t) * p1 + t * t * p2;
}

Tween.bezier3 = function(t, p0, p1, p2, p3)
{
    return Math.pow(1 - t, 3) * p0 + 3 * t * Math.pow(1 - t, 2) * p1 + 3 * t * t * (1 - t) * p2 + t * t * t * p3;
}

Tween.getAllStyleProperties = function(element)
{
    var css;
    if (document.defaultView && document.defaultView.getComputedStyle)
    {
        css = document.defaultView.getComputedStyle(element, null);
    }
    else
    {
        css = element.currentStyle
    }
    for (var key in css)
    {
        if (!key.match(/^\d+$/))
        {
            try
            {
                element.style[key] = css[key];
            }
            catch(e)
            {
            };
        }
    }
}

