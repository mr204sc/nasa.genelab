/*
 * FixIE
 * Adds more standardised functions to IE7 and IE8 behave like a "modern browser"
 *
 * @dependencies js/lib/com/greensock/TweenLite.min.js
 * @dependencies js/lib/net/hellomonday/system/BrowserDetect.js
 * @author Anders
 * @browsers: IE8+, FF3.6+, Chrome 11+, Safari 5+)
 *
 * Specifically provides the following function to behave like "modern" browsers:
 *
 *  addEventListener
 *  removeEventListener
 *  dispatchEvent
 *
 *  // style.opacity // disabled
 *  style.pointerEvents
 *
 *  window.innerHeight
 *  window.innerWidth
 *
 *  document.getElementsByClassName
 *  document.head
 *
 *  Array.indexOf
 *  Array.lastIndexOf
 *  Array.forEach
 *  Array.map
 *  Array.filter
 *  Array.every
 *  Array.some
 *
 *  String.trim();
 *
 *  Function.bind
 *
 */

var FixIE =
{
};

// Enable getter and setters in IE7 and 8
FixIE.extend = function(target, value, useFunction)
{
    if (BrowserDetect.BROWSER_NAME == "Explorer")
    {
        if (BrowserDetect.BROWSER_VERSION >= 8)
        {
            Object.defineProperty(target, value,
            {
                get: function()
                {
                    return value;
                },
                set: function(setValue)
                {
                    value = setValue;
                    useFunction(setValue);
                }
            });
        }
        else
        {
            // IE7 (might work in IE6 as well - this have not been testet though
            target._setter.push(value);
        }
    }
    else
    {
        // Mozilla, Webkit and other browsers
        target.__defineGetter__(value, function()
        {
            return value;
        });

        target.__defineSetter__(value, function(val)
        {
            value = val;
            useFunction(val);
        });
    }
};

FixIE.init = function()
{
    /**
     * Adds ability to use style.opacity
     */
    function _styleOpacity(element)
    {
        function setOpacity(value)
        {
            if ( typeof TweenLite != null)
            {
                TweenLite.to(element, 0,
                {
                    css:
                    {
                        opacity: value
                    }
                });
            }
        }


        element.style.opacity = 1;
        FixIE.extend(element.style, "opacity", setOpacity);
    }

    /**
     * Adds ability to use style.pointerEvents (beta)
     */
    function _stylePointerEvents(element)
    {
        function setPointerEvents(value)
        {
            if (value == "none")
            {
                FixIE.pointerEventsNone(element);
            }
            else
            {
                FixIE.clearPointerEventsNone(element);
            }
        }


     //   element.style.pointerEvents = "auto";
     //   FixIE.extend(element.style, "pointerEvents", setPointerEvents);
    }

    /*
     * Provide the addEventListener functionality
     */
    function _addEventListener(_element)
    {

        // remove the default event listeners
        if (_element.attachEvent != null)
        {
            _element._attachEvent = _element.attachEvent;
            //    _element.attachEvent = null;
        }

        // Add the addEventListener functionality to the item (also returns the old IE8 event object values)
        // http://www.quirksmode.org/js/events_properties.html
        _element.addEventListener = function(_event, _function, _capture)
        {
            function _eventCallbackFunction(useEvent)
            {
                var _event = window.event || useEvent;
                var returnObject =
                {
                };
                returnObject.bubbles = _event.cancelBubble;
                returnObject.cancelBubble = _event.cancelBubble;
                returnObject.cancelable = _event.cancelBubble;
                returnObject.currentTarget = _element;
                 //http://www.quirksmode.org/js/events_mouse.html#relatedtarget
                if (_event == "mouseout")
                {
                     returnObject.relatedTarget = _event.toElement;
                }
                else if (_event == "mouseover")
                {
                     returnObject.relatedTarget = _event.fromElement;
                }
                // IE normaly doesnt expose this
                returnObject.defaultPrevented = false;
                returnObject.eventPhase = 2;
                returnObject.originalTarget = _event.srcElement;
                returnObject.target = _event.srcElement;
                returnObject.timeStamp = _event.timeStamp;
                returnObject.type = _event.type;
                returnObject.preventDefault = _event.returnValue;
                returnObject.pageX = _event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                returnObject.pageY = _event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                returnObject.clientX = _event.clientX;
                returnObject.clientY = _event.clientY;
                returnObject.offsetX = _event.offsetX;
                returnObject.offsetY = _event.offsetY;
                returnObject.altKey = _event.altKey;
                returnObject.ctrlKey = _event.ctrlKey;
                returnObject.metaKey = false;
                returnObject.shiftKey = _event.shiftKey;
                returnObject.keyCode = _event.keyCode;
                returnObject.button = _event.button;
                //     returnObject.cancelBubble = _event.cancelBubble;
                returnObject.returnValue = _event.returnValue;
                returnObject.which = _event.which;
                returnObject.srcElement = _element;

                var useButton = 0;
                if (_event.which == 1)
                {
                    // middle button
                    useButton = 0;
                }
                else if (_event.which == 4)
                {
                    // middle button
                    useButton = 1;
                }
                else if (_event.which == 2)
                {
                    // right button
                    useButton = 1;
                }

                // functions
                returnObject.stopPropagation = function _stopPropagation()
                {
                    _event.cancelBubble = true;
                };
                returnObject.preventDefault = function _preventDefault()
                {
                    _event.returnValue = false;
                };

                _function(returnObject);
            }


            if (_element.___storedFunctionsReference == null)
            {
                _element.___storedFunctionsReference = new Array();
            }
             if (_element.___storedFunctions == null)
            {
                _element.___storedFunctions = new Array();
            }

            _element.___storedFunctionsReference.push(_function);
            _element.___storedFunctions.push(_eventCallbackFunction);

            _element._attachEvent("on" + _event, _eventCallbackFunction);
        };
    }

    /*
     * Provide the removeEventListener functionality
     */
    function _removeEventListener(_element)
    {

        if (_element.detachEvent != null)
        {
            _element._detachEvent = _element.detachEvent;
            //    _element.detachEvent = null;
        }
        _element.removeEventListener = function(_event, _function)
        {
            if (_function != undefined && _function != "undefined" && _event != null && _element != undefined)
            {
                var _length = _element.___storedFunctionsReference.length;
                for (var i = 0; i < _length; i++)
                {
                    var currFunc = _element.___storedFunctionsReference[i];
                    if (currFunc == _function)
                    {
                        _element._detachEvent("on" + _event, _element.___storedFunctions[i]);
                    }
                }

            }
        };
    }

    /*
     * Provide the dispatchEvent functionality
     */
    function _dispatchEvent(_element)
    {
        _element.dispatchEvent = function(_event)
        {
            _element.fireEvent("on" + _event.type, _event);
        };
    }

    /**
     * Overall function to normalize all functionlity
     */
    function addFunctionsToElement(element)
    {
        if ( typeof element != "undefined" && element != null)
        {
            //trace(element)
            var nodeNameIs = element.nodeName;
            if (element.____hasBeenIEFixed != true && nodeNameIs != "SCRIPT" && nodeNameIs != "HEAD" && nodeNameIs != "META" && nodeNameIs != "LINK" && nodeNameIs != "TITLE")
            {
                // Add style.opacity
                //_styleOpacity(element);
                // Add style.pointerEvents
                //_stylePointerEvents(element);
                // Add addEventListener
                _addEventListener(element);
                // Add removeEventListener
                _removeEventListener(element);
                // Add dispatchEvent
                _dispatchEvent(element);
                // Mark the element as being fixed
                element.____hasBeenIEFixed = true;
            }
        }
        else
        {
            if ( typeof Trace != "undefined")
            {
                Trace("FixIE: ** Error **: Element does not exist.");
            }
        }
    }

    /*
     * Fix for "window.innerHeight" and "window.innerWidth"
     * And adds the ability to write document.head and document.body
     */
    function setWindowFunctions()
    {
        _addEventListener(document);
        _removeEventListener(document);
        _dispatchEvent(document);

        _addEventListener(window);
        _removeEventListener(window);
        _dispatchEvent(window);

        // _addEventListener(XMLHttpRequest);
        // _removeEventListener(XMLHttpRequest);
        // _dispatchEvent(XMLHttpRequest);

        function setWidthAndHeight()
        {
            window.innerWidth = document.documentElement.clientWidth;
            window.innerHeight = document.documentElement.clientHeight;
        }


        window._attachEvent("onresize", setWidthAndHeight);
        setWidthAndHeight();

        document.head = document.getElementsByTagName("head")[0];
        var getHTML = document.getElementsByTagName("html")[0];
    }

    /*
     * Overwrites "document." functionality
     */
    function documentOverwrites()
    {
        document.createEvent = function(eventType)
        {
            // trace("creating event")
            // IE -  8-
            //http://help.dottoro.com/ljhlvomw.php
            // var mousedownEvent = document.createEventObject(window.event);
            // mousedownEvent.button = 1;  // left button is down
            // event.srcElement.fireEvent("onmousedown", mousedownEvent);
            //
            // // Other
            // var evObj = document.createEvent('MouseEvents');
            // evObj.initEvent("mousedown", true, false);
            // target.parentItemWas.dispatchEvent(evObj);

            // Create
            var returnObject = document.createEventObject(window.event);
            returnObject.initEvent = function(eventType, cancelable, preventDefault)
            {
                // , window, 0, event.screenX, event.screenY, event.clientX, event.clientY, event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, 0, null
                this.type = eventType;
                this.cancelBubble = cancelable;
                this.returnValue = preventDefault;
                // this.clientX = event.sceenX;
                // this.clientY;
                // this.offsetX;
                // this.offsetY;
                // this.altKey;
                // this.ctrlKey;
                // this.shiftKey;
                // this.keyCode;
            };
            return returnObject;
        };

        // Override document.createElement
        document._createElement = document.createElement;
        document.createElement = function(tag)
        {
            var _elem = document._createElement(tag);
            addFunctionsToElement(_elem);
            return _elem;
        };

        // Override document.getElementById
        document._getElementById = document.getElementById;
        document.getElementById = function(tag)
        {
            var _elem = document._getElementById(tag);
            addFunctionsToElement(_elem);
            return _elem;
        };

        // Override document.getElementsByTagName
        document._getElementsByTagName = document.getElementsByTagName;
        document.getElementsByTagName = function(tag)
        {
            var _arr = document._getElementsByTagName(tag);
            for (var _elem = 0; _elem < _arr.length; _elem++)
            {
                addFunctionsToElement(_arr[_elem]);
            }
            return _arr;
        };

        // Adds the "getElementsByClassName" function to IE (its not available in IE per default)
        document._getElementsByClassName = document.getElementsByClassName;
        document.getElementsByClassName = _MS_HTML5_getElementsByClassName;

        function _MS_HTML5_getElementsByClassName(classList)
        {
            var tokens = classList.split(" ");
            // Pre-fill the list with the results of the first token search
            var staticNodeList = this.querySelectorAll("." + tokens[0]);
            //Start the iterator at 1 since first match already colllected
            for (var i = 1; i < tokens.length; i++)
            {
                // Search for each token independently
                var tempList = this.querySelectorAll("." + tokens[i]);
                // Collects the "keepers" between loop iterations
                var resultList = [];
                for (var finalIter = 0; finalIter < staticNodeList.length; finalIter++)
                {
                    var found = false;
                    for (var tempIter = 0; tempIter < tempList.length; tempIter++)
                    {
                        if (staticNodeList[finalIter] == tempList[tempIter])
                        {
                            found = true;
                            break;
                            // termination if found
                        }
                    }
                    if (found)
                    {
                        // This element was in both lists, it should be perpetuated
                        // into the next round of token checking...
                        addFunctionsToElement(staticNodeList[finalIter]);
                        resultList.push(staticNodeList[finalIter]);
                    }
                }
                // Copy the AND results for the next token
                staticNodeList = resultList;
            }
            return staticNodeList;
        }

    }

     Image = function() {
     var createNewImage = document.createElement("img");
    // // createNewImage.addEventListener(Event.LOAD, FixIE.imageCheckForPNGFix);
    return createNewImage;
     }

    // MSDN about XMLHttpRequest: http://msdn.microsoft.com/en-us/library/ms535874(v=vs.85).aspx
    /*
     * Makes XMLHttpRequest work with eventListeners
     */
   /* var _XMLHttpRequest = XMLHttpRequest;
    XMLHttpRequest = function()
    {

        var createXMLHttpRequest = new _XMLHttpRequest();
        createXMLHttpRequest.___eventListeners = [];

        createXMLHttpRequest.onreadystatechange = function()
        {
            trace("createXMLHttpRequest.readyState : " + createXMLHttpRequest.readyState)
            var _length = createXMLHttpRequest.___eventListeners.length;
            var returnObject =
            {
                currentTarget: createXMLHttpRequest,
                target: createXMLHttpRequest
            }
            if (createXMLHttpRequest.readyState == 4 && createXMLHttpRequest.status == 200)
            {
                // Check for "load" eventlisteners
                for (var i = 0; i < _length; i++)
                {
                    var currObject = createXMLHttpRequest.___eventListeners[i];
                    trace("currObject.eventType: " + currObject.eventType)
                    if (currObject.eventType == "load")
                    {

                        currObject.callback(returnObject);
                        trace("doing callback")
                    }
                }
            }
            else
            if (createXMLHttpRequest.readyState > 4)
            {
                // Check for "error"
                for (var i = 0; i < _length; i++)
                {
                    var currObject = createXMLHttpRequest.___eventListeners[i];
                    if (currObject.eventType == "error")
                    {
                        currObject.callback(createXMLHttpRequest);
                    }
                }
            }
        }
        createXMLHttpRequest.addEventListener = function(eventType, callback)
        {
            createXMLHttpRequest.___eventListeners.push(
            {
                eventType: eventType,
                callback: callback
            });
        }
        createXMLHttpRequest.removeEventListener = function(eventType, callback)
        {
            // var _length = createXMLHttpRequest.___eventListeners.length;
            // for (var i = 0; i < _length; i++)
            // {
            // var currObject = createXMLHttpRequest.___eventListeners[i];
            // if (currObject.eventType == "load")
            // {
            // currObject.callback(createXMLHttpRequest);
            // }
            // }
        };

        return createXMLHttpRequest;
    };
    */
    documentOverwrites();
    setWindowFunctions();

    // Add ECMA262-5 method binding if not supported natively
    //
    if (!('bind' in Function.prototype))
    {
        Function.prototype.bind = function(owner)
        {
            var that = this;
            if (arguments.length <= 1)
            {
                return function()
                {
                    return that.apply(owner, arguments);
                };
            }
            else
            {
                var args = Array.prototype.slice.call(arguments, 1);
                return function()
                {
                    return that.apply(owner, arguments.length === 0 ? args : args.concat(Array.prototype.slice.call(arguments)));
                };
            }
        };
    }

    // Add ECMA262-5 string trim if not supported natively
    //
    if (!('trim' in String.prototype))
    {
        String.prototype.trim = function()
        {
            return this.replace(/^\s+/, '').replace(/\s+$/, '');
        };
    }

    // Add ECMA262-5 Array methods if not supported natively
    //
    if (!('indexOf' in Array.prototype))
    {
        Array.prototype.indexOf = function(find, i /*opt*/)
        {
            if (i === undefined)
                i = 0;
            if (i < 0)
                i += this.length;
            if (i < 0)
                i = 0;
            for (var n = this.length; i < n; i++)
                if ( i in this && this[i] === find)
                    return i;
            return -1;
        };
    }
    if (!('lastIndexOf' in Array.prototype))
    {
        Array.prototype.lastIndexOf = function(find, i /*opt*/)
        {
            if (i === undefined)
                i = this.length - 1;
            if (i < 0)
                i += this.length;
            if (i > this.length - 1)
                i = this.length - 1;
            for (i++; i-- > 0; )/* i++ because from-argument is sadly inclusive */
                if ( i in this && this[i] === find)
                    return i;
            return -1;
        };
    }
    if (!('forEach' in Array.prototype))
    {
        Array.prototype.forEach = function(action, that /*opt*/)
        {
            for (var i = 0, n = this.length; i < n; i++)
                if ( i in this)
                    action.call(that, this[i], i, this);
        };
    }
    if (!('map' in Array.prototype))
    {
        Array.prototype.map = function(mapper, that /*opt*/)
        {
            var other = new Array(this.length);
            for (var i = 0, n = this.length; i < n; i++)
                if ( i in this)
                    other[i] = mapper.call(that, this[i], i, this);
            return other;
        };
    }
    if (!('filter' in Array.prototype))
    {
        Array.prototype.filter = function(filter, that /*opt*/)
        {
            var other = [], v;
            for (var i = 0, n = this.length; i < n; i++)
                if ( i in this && filter.call(that, v = this[i], i, this))
                    other.push(v);
            return other;
        };
    }
    if (!('every' in Array.prototype))
    {
        Array.prototype.every = function(tester, that /*opt*/)
        {
            for (var i = 0, n = this.length; i < n; i++)
                if ( i in this && !tester.call(that, this[i], i, this))
                    return false;
            return true;
        };
    }
    if (!('some' in Array.prototype))
    {
        Array.prototype.some = function(tester, that /*opt*/)
        {
            for (var i = 0, n = this.length; i < n; i++)
                if ( i in this && tester.call(that, this[i], i, this))
                    return true;
            return false;
        };
    }
};
/*
 * if the SRC is being set and it is a .PNG do a PNG fix on it
 */
FixIE.imageCheckForPNGFix = function(event)
{
    //trace("ie FIX");
    var isPNG = false;
    //trace("event.target : " + event.currentTarget);
    if (event.currentTarget !== null)
    {
        event.currentTarget.removeEventListener(Event.LOAD, FixIE.imageCheckForPNGFix);
        var getSRC = event.currentTarget.src;
        if (getSRC.substr(getSRC.length - 4).toLowerCase() === ".png")
        {
            isPNG = true;
        }
        if (isPNG === true)
        {
            //trace("is png");
            var setFilter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='crop',src='" + getSRC + "')";
            event.currentTarget.style.background = "transparent";
            event.currentTarget.___isAlphaPNG = true;
            event.currentTarget.___alphaPNGFilter = " " + setFilter;
            event.currentTarget.style.MsFilter = setFilter;
            /* IE8 */
            event.currentTarget.style.filter = setFilter;
            /* IE6 & 7 */
            event.currentTarget.style.zoom = 1;
        }
        else
        {
            //event.target.style.background = "transparent";
            //event.target.style.MsFilter = "progid:DXImageTransform.Microsoft.gradient(startColorstr=#00FFFFFF,endColorstr=#00FFFFFF)"; /* IE8 */
            //event.target.style.filter = "progid:DXImageTransform.Microsoft.gradient(startColorstr=#00FFFFFF,endColorstr=#00FFFFFF)";   /* IE6 & 7 */
            //event.target.style["zoom"] = 1;
        }
    }
};

FixIE.passThrough = function(event)
{
    // For IE see: http://stackoverflow.com/questions/2490825/how-to-trigger-event-in-javascript
    // See also: http://help.dottoro.com/ljrinokx.php
    // GOOD: http://stackoverflow.com/questions/911586/javascript-simulate-mouse-over-in-code

    var target = event.target;
    var storeParent = target.parentNode;
    //storeParent.removeChild(target);
    var storeDisplay = target.style.display;
    target.style.display = "none";
    var getElement = document.elementFromPoint(event.pageX, event.pageY);
    //trace("getElement.id : " + getElement.id);

    var allowDispatch = true;
    var dispatchEventType = event.type;
    if (target.parentItemWas == getElement && getElement != target)
    {
        // Register a mouse over when we moved the mouse over the element once
        // dispatchEventType = "mousemove";
        // var evObj = document.createEvent('MouseEvents');
        // evObj.initEvent(dispatchEventType, true, false);
        // getElement.dispatchEvent(evObj);
        //trace("Dispatching : " + dispatchEventType);

        dispatchEventType = "mouseover";
        var evObj = document.createEvent('MouseEvents');
        evObj.initEvent(dispatchEventType, true, false);
        getElement.dispatchEvent(evObj);
        //getElement.style.backgroundColor = "#ff0000";
    }
    else
    {
        // If it is a completely new element - the do a mouse out on the "old" object - and a mouse out on the old
        if (target.parentItemWas != null)
        {
            dispatchEventType = "mouseout";
            var evObj2 = document.createEvent('MouseEvents');
            evObj2.initEvent(dispatchEventType, true, false);
            target.parentItemWas.dispatchEvent(evObj2);
            //getElement.style.backgroundColor = "#fff00";
        }
    }

    target.style.display = storeDisplay;
    target.parentItemWas = getElement;
    target.parentItemWasWas = target.parentItemWas;
};

FixIE.passThroughClick = function(event)
{
    var target = event.target;
    var storeParent = target.parentNode;
    var storeDisplay = target.style.display;
    target.style.display = "none";
    var getElement = document.elementFromPoint(event.pageX, event.pageY);
    if (target.parentItemWas != null)
    {
        var evObj = document.createEvent('MouseEvents');
        evObj.initEvent(event.type, true, false);
        target.parentItemWas.dispatchEvent(evObj);
        //trace("Dispatching : " + event.type);
    }

    target.style.display = storeDisplay;
    target.parentItemWas = getElement;
};

// Adds PointerEvents = "none" to the HTML element
FixIE.pointerEventsNone = function(target)
{
    target.addEventListener(MouseEvent.MOUSE_ENTER, FixIE.passThrough);
    target.addEventListener(MouseEvent.MOUSE_LEAVE, FixIE.passThrough);
    target.addEventListener(MouseEvent.DOUBLE_CLICK, FixIE.passThrough);

    target.addEventListener(MouseEvent.MOUSE_MOVE, FixIE.passThrough);
    target.addEventListener(MouseEvent.CLICK, FixIE.passThroughClick);
    target.addEventListener(MouseEvent.MOUSE_DOWN, FixIE.passThroughClick);
};

FixIE.clearPointerEventsNone = function(target)
{
    target.removeEventListener(MouseEvent.MOUSE_ENTER, FixIE.passThrough);
    target.removeEventListener(MouseEvent.MOUSE_LEAVE, FixIE.passThrough);
    target.removeEventListener(MouseEvent.DOUBLE_CLICK, FixIE.passThrough);

    target.removeEventListener(MouseEvent.MOUSE_MOVE, FixIE.passThrough);
    target.removeEventListener(MouseEvent.CLICK, FixIE.passThroughClick);
    target.removeEventListener(MouseEvent.MOUSE_DOWN, FixIE.passThroughClick);
};

// All the below are meant for advanced scaling and rotations (however TweenLite seems to fix this - so this may be deleted?)
FixIE.degreesToRadians = function(num) {
    return (num) * Math.PI / 180;
}

FixIE.fixBoundaryBug = function(elem, matrix, calcX, calcY) {
    // create corners for the original element
  
    var matrices = {
        tl: matrix.x(Matrix.create([[0], [0], [1]])),
        bl: matrix.x(Matrix.create([[0], [calcY], [1]])),
        tr: matrix.x(Matrix.create([[calcX], [0], [1]])),
        br: matrix.x(Matrix.create([[calcX], [calcY], [1]]))
    };
    
    var useTL = {
            x: parseFloat(parseFloat(matrices.tl.e(1, 1)).toFixed(8)),
            y: parseFloat(parseFloat(matrices.tl.e(2, 1)).toFixed(8))
        };
     var useBL = {
           x: parseFloat(parseFloat(matrices.bl.e(1, 1)).toFixed(8)),
            y: parseFloat(parseFloat(matrices.bl.e(2, 1)).toFixed(8))
        };
      var useTR = {
            x: parseFloat(parseFloat(matrices.tr.e(1, 1)).toFixed(8)),
            y: parseFloat(parseFloat(matrices.tr.e(2, 1)).toFixed(8))
        };
      var useBR = {
           x: parseFloat(parseFloat(matrices.br.e(1, 1)).toFixed(8)),
            y: parseFloat(parseFloat(matrices.br.e(2, 1)).toFixed(8))
        };
            
    var corners = {
        tl: useTL,
        bl: useBL,
        tr: useTR,
        br: useBR
    };
    
    // Initialize the sides
    var sides = {
        top: 0,
        left: 0
    };
    
    // Find the extreme corners
    for (var pos in corners) {
        // Transform the coords
        var corner = corners[pos];
        
        if (corner.y < sides.top) {
            sides.top = corner.y;
        }
        if (corner.x < sides.left) {
            sides.left = corner.x;
        }
    }
 
   var registrationPointPercentX = 0.5;
   var registrationPointPercentY = 0.5;
    if (elem._transformOriginX != null) {
        registrationPointPercentX = elem._transformOriginX / 100;
        registrationPointPercentY = elem._transformOriginY / 100;
    }
   
   elem.___compensateY = elem.___storeTempY + sides.top; 
   elem.___compensateX = elem.___storeTempX + sides.left;
   

}

FixIE.transformOrigin = function(elem, matrix, width, height) {
    // The destination origin
    var registrationPointPercentX = 0.5;
    var registrationPointPercentY = 0.5;
    if (elem._transformOriginX != null || elem._transformOriginY != null) {
        registrationPointPercentX = elem._transformOriginX / 100;
        registrationPointPercentY = elem._transformOriginY / 100;
    }
    
    toOrigin = {
        x: width * registrationPointPercentX,
        y: height * registrationPointPercentY
    };
    
    // The original origin
    fromOrigin = {
        x: 0,
        y: 0
    };
    
    // Multiply our rotation matrix against an x, y coord matrix
    var toCenter = matrix.x(Matrix.create([
        [toOrigin.x],
        [toOrigin.y],
        [1]
    ]));
    var fromCenter = matrix.x(Matrix.create([
        [fromOrigin.x],
        [fromOrigin.y],
        [1]
    ]));
    
    // Position the element
    // The double parse float simply keeps the decimals sane
   // elem.style.position = 'relative';
   
   // elem.style.top
       elem.___storeTempY = parseFloat(parseFloat((fromCenter.e(2, 1) - fromOrigin.y) - (toCenter.e(2, 1) - toOrigin.y)).toFixed(8));
       elem.___storeTempX = parseFloat(parseFloat((fromCenter.e(1, 1) - fromOrigin.x) - (toCenter.e(1, 1) - toOrigin.x)).toFixed(8));
    
}

FixIE.createIEMatrixString = function(M, mainClip, matrix)
{
    var M11 = M.e(1, 1) * mainClip._scaleX;
    var M12 = M.e(1, 2) * mainClip._scaleY;
    var M21 = M.e(2, 1) * mainClip._scaleX;
    var M22 = M.e(2, 2) * mainClip._scaleY;
    matrix = Matrix.create([[M11, M12, 0], [M21, M22, 0], [0, 0, 1]]);
    return 'M11=' + M11 + ', M12=' + M12 + ', M21=' + M21 + ', M22=' + M22;
}




function Matrix()
{
}

Matrix.prototype =
{

    // Returns element (i,j) of the matrix
    e: function(i, j)
    {
        if (i < 1 || i > this.elements.length || j < 1 || j > this.elements[0].length)
        {
            return null;
        }
        return this.elements[i-1][j - 1];
    },

    // Returns the result of multiplying the matrix from the right by the argument.
    // If the argument is a scalar then just multiply all the elements. If the argument is
    // a vector, a vector is returned, which saves you having to remember calling
    // col(1) on the result.
    multiply: function(matrix)
    {
        /* if (!matrix.elements) {
         return this.map(function(x) { return x * matrix; });
         }*/
        var returnVector = matrix.modulus ? true : false;
        var M = matrix.elements || matrix;
        if (typeof (M[0][0]) == "undefined")
        {
            M = Matrix.create(M).elements;
        }
        // if (!this.canMultiplyFromLeft(M)) { return null; }
        var ni = this.elements.length, ki = ni, i, nj, kj = M[0].length, j;
        var cols = this.elements[0].length, elements = [], sum, nc, c;
        do
        {
            i = ki - ni;
            elements[i] = [];
            nj = kj;
            do
            {
                j = kj - nj;
                sum = 0;
                nc = cols;
                do
                {
                    c = cols - nc;
                    sum += this.elements[i][c] * M[c][j];
                }
                while (--nc);
                elements[i][j] = sum;
            }
            while (--nj);
        }
        while (--ni);
        var M = Matrix.create(elements);
        return returnVector ? M.col(1) : M;
    },

    x: function(matrix)
    {
        return this.multiply(matrix);
    },

    // Set the matrix's elements from an array. If the argument passed
    // is a vector, the resulting matrix will be a single column.
    setElements: function(els)
    {
        var i, elements = els.elements || els;
        if ( typeof (elements[0][0]) != 'undefined')
        {
            var ni = elements.length, ki = ni, nj, kj, j;
            this.elements = [];
            do
            {
                i = ki - ni;
                nj = elements[i].length;
                kj = nj;
                this.elements[i] = [];
                do
                {
                    j = kj - nj;
                    this.elements[i][j] = elements[i][j];
                }
                while (--nj);
            }
            while(--ni);
            return this;
        }
        var n = elements.length, k = n;
        this.elements = [];
        do
        {
            i = k - n;
            this.elements.push([elements[i]]);
        }
        while (--n);
        return this;
    }
}; 


// Constructor function
Matrix.create = function(elements)
{
    var M = new Matrix();
    return M.setElements(elements);
}; 



// Rotation matrix about some axis. If no axis is
// supplied, assume we're after a 2D transform

Matrix.Rotation = function(theta, a)
{
    if (!a)
    {
        return Matrix.create([[Math.cos(theta), -Math.sin(theta)], [Math.sin(theta), Math.cos(theta)]]);
    }
    var axis = a.dup();
    if (axis.elements.length != 3)
    {
        return null;
    }
    var mod = axis.modulus();
    var x = axis.elements[0] / mod, y = axis.elements[1] / mod, z = axis.elements[2] / mod;
    var s = Math.sin(theta), c = Math.cos(theta), t = 1 - c;
    // Formula derived here: http://www.gamedev.net/reference/articles/article1199.asp
    // That proof rotates the co-ordinate system so theta
    // becomes -theta and sin becomes -sin here.
    return Matrix.create([[t * x * x + c, t * x * y - s * z, t * x * z + s * y], [t * x * y + s * z, t * y * y + c, t * y * z - s * x], [t * x * z - s * y, t * y * z + s * x, t * z * z + c]]);
}; 

if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 8)
{
    FixIE.init();
}
