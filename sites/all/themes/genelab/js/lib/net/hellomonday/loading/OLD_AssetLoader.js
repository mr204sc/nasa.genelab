/**
 * @fileoverview Provides Asset loading
 *  
 *  Usage Examples:
 *      var ARRAY_OF_IMAGES = ["IMAGE1.EXTENSION", "IMAGE2.EXTENSION"];
 *      AssetLoader.init(FOLDER_TO_LOAD_IMAGES_FROM);
 *      var GROUP_TO_LOAD = new AssetGroup(ARRAY_OF_IMAGES, {onComplete: CALLBACK_WHEN_ALL_IMAGES_ARE_LOADED, onUpdate: CALLBACK_WHEN_AN_IMAGE_IS_LOADED_RETURNS_PERCENTAGE});
 *      AssetLoader.loadGroup();
 *      AssetLoader.getAsset("IMAGE1.EXTENSION");
 *
 *  Usage Examples:
 *      var loadAssetsArray = ["intro_bg.jpg", "gfx/intro_berries_left.png", "intro_berries_right.png", "intro_coffee.png", "intro_halo.png", "intro_line.png", "intro_man.png", "intro_naora.png"];
 *      AssetLoader.init("assets/images/");
 *      var myAssetsGroup = new AssetGroup(loadAssetsArray, {onComplete: Preloader.done, onUpdate: Preloader.onUpdatePercent, stripPath: true, customPath: "test/testing/path/"});
 *      AssetLoader.loadGroup(myAssetsGroup);
 *      AssetLoader.getAsset("gfx/intro_berries_left.png");
 *
 *
 *
 * @dependencies lib/net/hellomonday/hmframework/FixIE (for making it work in IE8)
 * 
 * @author Torben
 * 
 * @browsers: All
 * 
 */
(function()
{
	var _instance = {};
	var _stopLoad = false;
	
	/**
	* @param {String} assetPath, specifies the root path of the assets.
	*/
	_instance.init = function(assetPath)
	{
		if(!_instance.initialized)
		{	
			_instance.assets = [];
			_instance.que = [];
			_instance.busy = false;
			_instance.assetPath = assetPath;
			_instance._percentageLoaded = 0;
			_instance.initialized = true;
		}
	}
	
	/**
	 * @param group, AssetGroup with an array containing assets to load.
	 */
	_instance.loadGroup = function(group)
	{
		if(!_instance.initialized)
		{
			throw new Error("You need to initialize Assetloader before loading a group");
			return;
		}
		
		if(!_instance.busy)
		{
			_instance.busy = true;
			_instance.que.push(group);
			internalLoad();
		}
		else
		{
			_instance.que.push(group);
		}
	}
	
	/**
	 * @param group, AssetGroup with an array containing assets to remove (from memory if no other reference exists).
	 */
	_instance.removeGroup = function(group)
	{
		if(!_instance.initialized)
		{
			throw new Error("You need to initialize Assetloader before removing a group");
			return;
		}
		
		var i = 0;
		var l = _instance.assets.length;
		
		for(i; i < l; i++)
		{
			var j = 0;
			var k = group.data.length;
			var id; 
			
			for(j; j < k; j++)
			{
				id = group.data[j];
				
				if(group.stripPath)
				{
					id = (stripPath(id));
				}
				
				id = (stripExtension(id));
				
				// Remove from asset array if id's match
				if(_instance.assets[i].id == id)
				{
					_instance.assets.splice(i, 1);
				}
			}
		}
	}
	
	function internalLoad()
	{
		_instance._percentageLoaded = 0;	
		
		var data = _instance.que[0].data;
		var onComplete = _instance.que[0].onComplete;
		var customPath = _instance.que[0].customPath;
		var onUpdate = _instance.que[0].onUpdate;
		
		var i = 0;
		var l = data.length;
		var assetsLoaded = 0;
		var assetsToLoad = [];
		var assetLoadCount = 0;
		var asset;
		var id;
		
		for(i; i < l; i++)
		{
			id = stripExtension( data[i] );
			
			if( !_instance.assetExist(id) )
			{
				assetsToLoad.push(data[i]);
				assetLoadCount++;
			}
		}
		
		load();
		
		function load()
		{
			if(assetsToLoad.length > 0)
			{
				var i = 0;
				var l = assetsToLoad.length;
				
				for(i; i < l; i++)
				{
					asset = new Image();
					id = stripExtension( assetsToLoad[i] );
					
					asset.addEventListener("load", assetInternalLoaded, true);
                    asset.addEventListener("error", assetInternalLoadError, true);
                    asset.style.position = "absolute";
					
					// Check if path contains forward slash.
					var path = assetsToLoad[i];
					var regx = new RegExp('\/');
					
					if(regx.test(path))
					{
						// Strip the path from the id if specified (otherwise the id will be the full path)
						if(_instance.que[0].stripPath)
						{
							id = stripPath(assetsToLoad[i]);
							id = (stripExtension(id));
						}
					}
					
					// Check if custom path is provided, else use default path
					if(customPath != null)
					{
						url = customPath + assetsToLoad[i]; 	
					}
					else
					{
					
						url = _instance.assetPath + assetsToLoad[i]; 	
					}
					
					_instance.assets.push({asset: asset, id: id})
					
					asset.src = url;
				}
			}
			else
			{
				assetLoaded();
			}
		}
		
		function assetInternalLoaded(event)
        {
            event.currentTarget.removeEventListener("load", assetLoaded, true);
            event.currentTarget.removeEventListener("error", assetLoaded, true);
            assetsLoaded++;
            assetLoaded();
        }

        function assetInternalLoadError(event)
        {
        	throw new Error("AssetLoader -> Error, can't load " + event.currentTarget.src);
        	event.currentTarget.removeEventListener("load", assetLoaded, true);
            event.currentTarget.removeEventListener("error", assetLoaded, true);
            assetsLoaded++;
            assetLoaded();
        }
		
		function assetLoaded()
        {
        	_instance._percentageLoaded = Math.ceil((assetsLoaded / assetLoadCount) * 100);
            
            if(onUpdate != null)
            {
                onUpdate(_instance._percentageLoaded);
            }

            if(assetsLoaded == assetLoadCount)
            {
                // Trigger callback on group
                if(onComplete)
                {
                	onComplete();
                }
                _instance.que.shift();

                if(_instance.que.length > 0)
                {
                	// Check if loading has been canceled
                	if(!_stopLoad)
                	{
                    	setTimeout(internalLoad, 100);
                    }
                    else
                    {
                    	_stopLoad = false;
                    	_instance.busy = false;
                    	
                    	// Trigger callback on group
                    	if(onComplete)
		                {
		                	onComplete();
		                }
                    }
                }
                else
                {
                    _instance.busy = false;
                }
            }
        }
	}
	
	/**
	* @param id, search string. Looks for asset with matching id
	* @returns cloned image
	**/
	_instance.getAsset = function(id)
	{
		if(!_instance.initialized)
		{
			throw new Error("You need to initialize Assetloader before attempting to retrive an asset - id: " + id);
			return;
		}
		
		// Strip file extension (all id's are kept without file extension')
		id = stripExtension(id);
		
		var found = false;
		var i = 0;
		var l = _instance.assets.length;
		var asset = null;
		var existingAsset;
		
		for(i; i < l; i++)
		{
			if(id === _instance.assets[i].id)
			{
				existingAsset = _instance.assets[i].asset;
				
				found = true;
				asset = existingAsset.cloneNode(true);
	            asset.width = existingAsset.width;
	            asset.height = existingAsset.height;
	            asset.style.position = "absolute";
				break;
			}
		}
		
		if(found !== true)
		{
			//throw new Error("AssetLoader -> asset with id: " + id + ", not found");
		}
		
		return asset;
	}
	
	// Helper methods
	_instance.assetExist = function(id)
	{
		var i = 0;
		var l = _instance.assets.length;
		
		for(i; i < l; i++)
		{
			if(id === _instance.assets[i].id)
			{
				return true;
				break;
			}
		}
		
		return false;
	}
	
	stripExtension = function(str)
	{
		var rVal = "";
		if(str)
		{
			var stripped = str.split(".");
			rVal = stripped[0];
		}
		return rVal;
	};
	
	stripPath = function(str)
	{
		var rVal;
		
		if(str)
		{
			var regX = new RegExp('^.*[\/]', "g");
			rVal = str.replace(regX, "");;
		}
		
		return rVal;
	};
	
	window.AssetLoader = _instance;
})();


/**
 * Data object to be used in conjunction with the AssetLoader
 * 
 * @param {Array} data, Array containing assets to load (if a full relative path is specified, this path will be used instead of the default/custom)
 * @param {Object} params, Available params: {Function} onComplete, AssetLoader will run the callback when its done loading the group
 * 										     {String} customPath, optional, provide a custom load path (otherwise it will use the default path provided when AssetLoader is initialized)
 * 											 {Function} onUpdate, AssetLoader will run this method every time there is an update in the load progress and return a percentage value
 * 											 {Boolean} stripPath, indicating if the path should be stripped from the id
 */
function AssetGroup(data, params)
{
	var instance = {};
	
	instance.data = data;
	
	if(params.onComplete)
	{
		instance.onComplete = params.onComplete;
	}
	if(params.customPath !== null)
	{
		instance.customPath = params.customPath;
	}
	if(params.onUpdate)
	{
		instance.onUpdate = params.onUpdate;
	}
	
	if(params.stripPath)
	{
		instance.stripPath = params.stripPath;
	}
	else
	{
		instance.stripPath = false;
	}
	
	return instance;
}


