/**
 *  Wrapper for web audio api with fallback
 *  Does not support IE8 at the moment due to the setter/getter implementation (can easily be fixed)
 * 
 *  Engine initialization:
 *  
 *  HelloAudio.sm2Path = "js/lib/net/hellomonday/audio/fallback/soundManager2/"; <== Setup SoundManager2 path
 *  HelloAudio.init(audioEngineReady); <== run init with a ready callback 
 * 
 *  The engine supports different types of sounds
 *  - one shot (multiple instances of the same sound can be triggered at once, but they can't loop)
 *    HelloAudio.play("assets/sounds/test.mp3");
 * 
 *  - multi shot (only one instane of the sound can be played at once, and it is loopable)
 *    var loopingSound = HelloAudio.createAudioObject("assets/sounds/test.mp3", true);
 *    HelloAudio.play(loopingSound);
 *     
 *
 *  @author Torben (torben@hellomonday.com)
 * 
 * 
 *  TODO:
 *  - Implement Channels(will be an audio object) where you can route multiple sounds through (web audio only)
 *  
 *  02-09-2013:
 *  MediaElementAudioSourceNode should be used for playback of larger files.
 *  But is currently only supported in Chrome. 
 *  (This way the sound file will be streaming through an audio tag, hooked up to a web audio context)
 * 
 * 
 *  04-09-2013:
 *  Fixed compatibility issue with Safari. Remember to check the specs ..
 *  "createGain" is "createGainNode" in Safari
**/ 
(function(g){function f(a){n=a;c=a.path.length;d(n.path[0])}function d(c){var a=new XMLHttpRequest;a.open("GET",c,!0);a.responseType="arraybuffer";a.onerror=b;a.onload=e;a.send()}function b(){}function e(c){h.decodeAudioData(c.target.response,a,p)}function a(a){l.push({path:n.path[0],buffer:a});n.path.shift();a=n.path.length;n.updateCallback&&n.updateCallback(1-a/c);1>a?(n.completeCallback&&n.completeCallback(),0<m.length&&f(m.shift())):d(n.path[0])}function p(){}var k={},l,h,m,q=!1,n,c,r=!1;k.init=
function(c){r=!0;h=c;l=[];m=[]};k.loadAudio=function(c,a,b){if(r){var e=-1<Object.prototype.toString.call(c).indexOf("Array")?"Array":"String",d={};d.completeCallback=a;d.updateCallback=b;"Array"!=e?(d.path=[],d.path.push(c)):d.path=c;q?m.push(d):(q=!0,f(d))}};k.getAudioBuffer=function(c){var a=0,b=l.length,e=null;for(a;a<b;a++)if(c==l[a].path){e=l[a].buffer;break}return e};k.removeAudioBuffer=function(c){Object.prototype.toString.call(c).indexOf("Array")};g.AudioLoader=k})(window);
function AudioObject(g,f,d,b,e,a,p){var k={},l,h;k.addEffect=function(a){"dry"==a.outputType?l.addInsert(a):l.addSend(a)};k.removeEffect=function(a){"dry"==a.outputType?l.removeInsert(a):l.removeSend(a)};k.setSource=function(a){k.source=a;a.connect(h);l.update()};Object.defineProperty(k,"volume",{get:function(){return h.gain.value},set:function(a){h.gain.value=a}});k.playing=!1;k.paused=!1;h=b.createGain();l=new AudioSignalChain(h,e,a);k.path=g;k.source=f;k.buffer=d;k.params=p;k.startTime=0;k.startOffset=
0;k.paused=!1;k.pausePosition=0;k.setSource(f);return k}
function AudioSignalChain(g,f,d){var b={addInsert:function(e){b.dryEffects.push(e);b.update()},addSend:function(e){b.wetEffects.push(e);b.update()},removeInsert:function(e){var a=0,d=b.dryEffects.length;for(a;a<d;a++)if(b.dryEffects[a].id==e.id){b.dryEffects.splice(a,1);break}b.update()},removeSend:function(e){var a=0,d=b.wetEffects.length;for(a;a<d;a++)if(b.wetEffects[a].id==e.id){b.wetEffects.splice(a,1);break}b.update()},update:function(){g.disconnect();var e=1,a=b.dryEffects.length;if(0<a){g.connect(b.dryEffects[0].input);
for(e;e<a;e++)b.dryEffects[e-1].output.connect(b.dryEffects[e].input);b.dryEffects[e-1].output.connect(f)}else g.connect(f);e=1;a=b.wetEffects.length;if(0<a){g.connect(b.wetEffects[0].input);for(e;e<a;e++)b.wetEffects[e-1].output.connect(b.wetEffects[e].input);b.wetEffects[e].output.connect(d)}},dryEffects:[],wetEffects:[]};return b}
function AudioEffect_2dPanner(g){function f(d){b=d;e.gain.value=-0.5*b+0.5;a.gain.value=0.5*b+0.5}var d={outputType:"dry"},b=g||0,e,a,p;Object.defineProperty(d,"pan",{get:function(){return b},set:f});d.dispose=function(){HelloAudio.removeEffect(d.id);d=null};g=HelloAudio.mainBus.createChannelSplitter(2);p=HelloAudio.mainBus.createChannelMerger(2);e=HelloAudio.mainBus.createGain();a=HelloAudio.mainBus.createGain();g.connect(e,0);g.connect(a,1);e.connect(p,0,0);a.connect(p,0,1);d.id=HelloAudio.addEffect(d);
d.input=g;d.output=p;f(b);return d}
function SMAudioLoaderBrigde(){function g(a){_curGroup=a;_curGroupTotal=a.path.length;f(_curGroup.path[0])}function f(a){soundManager.createSound({id:a,url:a,autoLoad:!0,stream:!1,onload:d})}function d(){_curGroup.path.shift();var a=_curGroup.path.length;_curGroup.updateCallback&&_curGroup.updateCallback(1-a/_curGroupTotal);1>a?(_curGroup.completeCallback&&_curGroup.completeCallback(),0<e.length&&g(e.shift())):f(_curGroup.path[0])}var b={},e,a=!1;b.loadAudio=function(b,d,f){var h=-1<Object.prototype.toString.call(b).indexOf("Array")?
"Array":"String",m={};m.completeCallback=d;m.updateCallback=f;"Array"!=h?(m.path=[],m.path.push(b)):m.path=b;a?e.push(m):(a=!0,g(m))};e=[];return window.AudioLoader=b}
function SMAudioObjectBrigde(g,f){var d={},b=null,e=soundManager.getSoundById(g);d.addEffect=function(a){b=a;b.applySoundObject(e)};d.removeEffect=function(a){b.removeSoundObject();b=null};d.play=function(a){e.play(a);b&&b.update()};d.pause=function(){e.pause()};d.resume=function(){e.resume();b&&b.update()};d.stop=function(){e.stop()};Object.defineProperty(d,"volume",{get:function(){return 0.01*e.volume},set:function(a){1<a&&(a=1);e.setVolume(100*a)}});d.path=g;d.params=f;return d}
function SMAudioEffect_2dPannerBrigde(g){var f={},d=g||0,b=null;Object.defineProperty(f,"pan",{get:function(){return d},set:function(b){d=b;f.update()}});f.applySoundObject=function(e){b=e;f.update()};f.removeSoundObject=function(){b=null};f.update=function(){b&&b.setPan(100*d)};f.dispose=function(){f=null};return f}
(function(g){function f(){if(!a.sm2Path)throw Error("HelloAudio initSM2Fallback: sm2Path needs to be set!");soundManager.setup({url:a.sm2Path+"/swf",onready:d,ontimeout:b,flashVersion:9,preferFlash:!0});soundManager.beginDelayedInit()}function d(){AudioObject=SMAudioObjectBrigde;AudioEffect_2dPanner=SMAudioEffect_2dPannerBrigde;new SMAudioLoaderBrigde;n()}function b(){}function e(a,b){function e(){var a=this.readyState;if("loaded"==a||"complete"==a)this.onload=this.onreadystatechange=null,b&&b()}
function d(){this.onload=this.onreadystatechange=null;g.setTimeout(b,20)}var f=document.createElement("script");f.type="text/javascript";b&&(f.onreadystatechange=e,f.onload=d);f.src=a;document.getElementsByTagName("head")[0].appendChild(f)}var a={},p,k,l,h,m=[],q=0,n;a.sm2Path=null;a.init=function(c){if(!c)throw Error("HelloAudio init: ready callback function missing!");n=c;try{g.AudioContext=g.AudioContext||g.webkitAudioContext,h=new AudioContext,h.createGain=h.createGain||h.createGainNode,l=h.createGain(),
l.connect(h.destination),k=h.createGain(),k.connect(l),p=h.createGain(),p.connect(l),AudioLoader.init(h),a.fallback=!1,n()}catch(b){a.fallback=!0,e(a.sm2Path+"script/soundmanager2.js",f)}};a.createAudioObject=function(c,b,e,d,f){var g={},l=e;b&&!0!=e&&(g.loop=!0,g.loopStart=d,g.loopEnd=f,l=!1);a.fallback?c=new AudioObject(c,g):(b=AudioLoader.getAudioBuffer(c),e=h.createBufferSource(),c=new AudioObject(c,e,b,h,p,k,g),c.volume=1);c.multiShot=l||!1;return c};a.play=function(c,b,e){if(a.fallback){e=!0;
var d;-1<Object.prototype.toString.call(c).indexOf("String")?(c=soundManager.getSoundById(c),d=!1):(e=c.multiShot||!1,d=c.params.loop);var f={};b&&(b*=100);f.volume=b||100;f.loops=d?99999999:1;f.multiShot=e;c.playing=!0;c.play(f)}else{if("AudioObject"==(-1<Object.prototype.toString.call(c).indexOf("String")?"Path":"AudioObject")){if(!c.multiShot){if(c.paused){a.resume(c);return}if(c.playing)return}d=h.createBufferSource();d.buffer=c.buffer;c.setSource(d);c.playing=!0;trace(c.volume,"audioObject.volume");
c.volume=b||c.volume;f=c.params;c.startTime=h.currentTime;f.loop&&(d.loop=!0,d.loopStart=f.loopStart,d.loopEnd=f.loopEnd)}else d=h.createBufferSource(),c=AudioLoader.getAudioBuffer(c),d.buffer=c,d.connect(p);d.start||(d.start=d.noteOn);d.start(0,e||0)}};a.stop=function(c){c.playing=!1;c.paused=!1;if(a.fallback)c.stop();else{var b=c.source;c.startOffset=0;b.stop||(b.stop=b.noteOff);b.stop(0)}};a.pause=function(c){c.playing=!1;c.paused=!0;a.fallback?c.pause():(c.startOffset+=h.currentTime-c.startTime,
c=c.source,c.stop||(c.stop=c.noteOff),c.stop(0))};a.resume=function(c){if(a.fallback)c.resume();else if(c.paused){var b=c.startOffset%c.buffer.duration;c.paused=!1;a.play(c,b)}};a.addEffect=function(a){var b=q;m.push({effect:a,id:b});q++;return b};a.removeEffect=function(a){var b=0,d=m.length;for(b;b<d;b++)if(m[b].id==a){m.splice(b,1);break}};Object.defineProperty(a,"mainBus",{get:function(){return h}});Object.defineProperty(a,"masterVolume",{get:function(){return l.gain.value},set:function(a){1<
a?a=1:0>a&&(a=0);l.gain.value=a}});Object.defineProperty(a,"dryVolume",{get:function(){return l.gain.value},set:function(a){1<a?a=1:0>a&&(a=0);l.gain.value=a}});Object.defineProperty(a,"wetVolume",{get:function(){return l.gain.value},set:function(a){1<a?a=1:0>a&&(a=0);l.gain.value=a}});g.HelloAudio=a})(window); 

