/****************************************
 These functions allow you to easily set a handler for a virtual mouseleave/enter event, using my MouseBoundaryCrossing class.
 /****************************************/

//Note that a mouseout/over event is always fired before a mouseleave/enter event
//Also note that mouseleave/enter events do not bubble; effectively, they don't bubble in this implementation either.
//usage:  elem.onmouseout = onMouseLeave(leaveHandler, outHandler);
//usage:  elem.onmouseover = onMouseEnter(enterHandler, overHandler);
function onMouseLeave(handleLeave, handleOut)
{
    if (!handleLeave)
        return handleOut;
    return function(evt)
    {
        evt = evt || window.event;
        if (handleOut)
            handleOut.call(this, evt);
        try
        {
            var mbc = new MouseBoundaryCrossing(evt, this);
            if (mbc.leftLandmark)
                handleLeave.call(this, evt);
        }
        catch(e)
        {
        }
    }
};

function onMouseEnter(handleEnter, handleOver)
{
    if (!handleEnter)
        return handleOver;
    return function(evt)
    {
        evt = evt || window.event;
        if (handleOver)
            handleOver.call(this, evt);
        try
        {
            var mbc = new MouseBoundaryCrossing(evt, this);
            if (mbc.enteredLandmark)
                handleEnter.call(this, evt);
        }
        catch(e)
        {
        }
    }
};