function AudioEffect_Panner()
{
	var _instance					= {};
	_instance.outputType            = "dry";
	
	var _position                   = {x: 0, y: 0, z: 0};
	
	var _panner;

	function init()
	{
	    _panner = HelloAudio.mainBus.createPanner();
	    
	    _instance.id = HelloAudio.addEffect(_instance);
	    
	    _instance.input = _panner;
        _instance.output = _panner;
	    
	    // _instance.node.pannerModel = "linear";
// 	    
	    // _instance.node.coneOuterGain = 1;
        // _instance.node.coneOuterAngle = 180;
        // _instance.node.coneInnerAngle = 0;
	}
	
	Object.defineProperty(_instance, "x", {get: getX, set: setX});
    function getX()
    {
        return _position.x;
    }
    
    function setX(value)
    {
        _position.x = value;
        _instance.input.setPosition(value, _position.y, _position.z);
    }
    
    Object.defineProperty(_instance, "y", {get: getY, set: setY});
    function getY()
    {
        return _position.y;
    }
    
    function setY(value)
    {
        _position.y = value;
        _instance.input.setPosition(_position.x, value, _position.z);
    }
    
    _instance.setAngle = function(angle)
    {
        _instance.input.setOrientation(Math.cos(angle), -Math.sin(angle), 1);
    }
	
	_instance.dispose = function()
	{
	    HelloAudio.removeEffect(_instance.id);
	    _instance = null;
	}
	
	init();

	return _instance;
}