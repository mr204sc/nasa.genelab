// TODO: Make it possible to insert this effect on multiple sounds
//       Maybe use and array where sound path/sound object is saved in pairs, so they can be easily found again upon removal

function SMAudioEffect_2dPannerBrigde(pan)
{
    var _instance                   = {};
    var _pan                        = pan || 0;
    
    var _SM2SoundObject             = null;
    
    function init()
    {
        //trace("SMAudioEffect_2dPannerBrigde init");
    }

    Object.defineProperty(_instance, "pan", {get: getPan, set: setPan});
    function getPan()
    {
        return _pan;
    }
    
    function setPan(value)
    {
       _pan = value;
       _instance.update();
    }
    
    _instance.applySoundObject = function(soundObject)
    {
        _SM2SoundObject = soundObject;
        _instance.update();
    }
    
    _instance.removeSoundObject = function()
    {
        _SM2SoundObject = null;
    }
    
    _instance.update = function()
    {
        if(_SM2SoundObject)
        {
            _SM2SoundObject.setPan(_pan * 100);
        }
    }
    
    _instance.dispose = function()
    {
        _instance = null;
    }
    
    init();

    return _instance;
}