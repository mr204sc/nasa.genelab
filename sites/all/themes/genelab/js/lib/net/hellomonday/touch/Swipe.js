/*
 * var swipeLeft = new Swipe(document, 130, "left", ControlTemplates.nextTemplate, 1);
        var swipeRight = new Swipe(document, 130, "right", ControlTemplates.prevTemplate, 1);
 */
function Swipe(target, swipeLength, direction, callback, fingerCount) {
	if (fingerCount == null) {
		fingerCount = 1;
	}
	var _fingerCount = fingerCount;
	var _dragStartX = 0;
	var _isDragging = false;
	var _callback = callback;
	function startDrag(event) {
		//trace("startdrag, " + event.pageX)
		if ((event.targetTouches == null || event.targetTouches.length == _fingerCount)) {
			if (event.targetTouches == null) {
				 var touch = event;
			}
			else {
				 var touch = event.targetTouches[0];
			}
			_dragStartX = touch.pageX;
			_isDragging = true;
			
			// Remember prevent default else "move drag" wont fire on Android
			
		}
		//event.preventDefault();
	}
	
	function moveDrag(event) {
		//trace("move")
		if ((event.targetTouches == null || event.targetTouches.length == _fingerCount)) {
			if (_isDragging == true) {
				if (event.targetTouches == null) {
					 var touch = event;
				}
				else {
					 var touch = event.targetTouches[0];
				}
				var difference = _dragStartX - touch.pageX;
				if (direction == "left" && difference > swipeLength) {
					//trace("SWIPED LEFT");
					_isDragging = false;
					_callback();
				}
				else if (direction == "right" && difference * - 1 > swipeLength) {
					//trace("SWIPED RIGHT");
					_isDragging = false;
					_callback();
				}
			}
		}
		event.preventDefault();
	}
	
	function stopDrag(event) {
		//trace("UP")
		_isDragging = false;
		event.preventDefault();
	}
	
	if (_fingerCount > 1) {
		
	}
	else {
		
	}
	
	
	this.deactivate = function() {
		trace("DEACTIVATING");
		target.removeEventListener(TouchEvent.TOUCH_MOVE, moveDrag, false);
		target.removeEventListener(TouchEvent.TOUCH_START, startDrag, false);
		target.removeEventListener(TouchEvent.TOUCH_END, stopDrag, false);
		
		target.removeEventListener(MouseEvent.MOUSE_MOVE, moveDrag, false);
		target.removeEventListener(MouseEvent.MOUSE_DOWN, startDrag, false);
		target.removeEventListener(MouseEvent.MOUSE_UP, stopDrag, false);
	}
	
	this.activate = function() {
		target.addEventListener(TouchEvent.TOUCH_MOVE, moveDrag, false);
		target.addEventListener(TouchEvent.TOUCH_START, startDrag, false);
		target.addEventListener(TouchEvent.TOUCH_END, stopDrag, false);
		
		target.addEventListener(MouseEvent.MOUSE_MOVE, moveDrag, false);
		target.addEventListener(MouseEvent.MOUSE_DOWN, startDrag, false);
		target.addEventListener(MouseEvent.MOUSE_UP, stopDrag, false);
	}
	
	this.activate();
	
	/*
	 * 
	 * 
	if (BrowserDetect.MOBILE == true || BrowserDetect.TABLET == true) {
		target.addEventListener(TouchEvent.TOUCH_MOVE, moveDrag, false);
		target.addEventListener(TouchEvent.TOUCH_START, startDrag, false);
		target.addEventListener(TouchEvent.TOUCH_END, stopDrag, false);
	}
	else if (_fingerCount == 1) {
		target.addEventListener(MouseEvent.MOUSE_MOVE, moveDrag, false);
		target.addEventListener(MouseEvent.MOUSE_DOWN, startDrag, false);
		target.addEventListener(MouseEvent.MOUSE_UP, stopDrag, false);
	}
	 */
}
