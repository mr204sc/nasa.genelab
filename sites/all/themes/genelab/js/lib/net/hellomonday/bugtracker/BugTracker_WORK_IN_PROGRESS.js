/**
 * @fileoverview The bug tracker automatically reports JS errors.
 *    Usage Examples:
 *       The bug tracker automatically starts and report bugs
 *          To get stacktraces added to the Bug Tracking use:
 *             BugTracker.track(STRING);
 *               Example:
 *                 BugTracker.track("testFunctionName()");
 *
 * The bug tracker can run on its own (fully automatical).
 * However using BugTracker.track will help give more advanced analytics.
 * The BugTracker will also automatically report the lastest 5 trace commands that were made (using trace.js) or last 5 bugtracker.track items
 *
 * The Bugtracker will also push the errors into the Google Analytics as events
 *
 * Hold down SRC (desktop) - 3 finger "swipe down" (tablet / mobile) to take screenshots - need to set Projekt Key to enable this
 *
 * @dependencies  lib/net/hellomonday/system/BrowserDetect.js - optional but provides better code hinting and lib/net/hellomonday/trace/trace.js
 * @author Anders
 * @browsers: All (however only tested in: IE7+, FF3.6+, Chrome 11+, Safari 5+)
 *
 *
 */

function BugTracker()
{
}

/**
 * Bug Tracker settings
 */
// 5b07eff0 Is used for overall testing
BugTracker.bugKey = "5b07eff0";
// Set in a MAX of errors that each visit can send through (in order to avoid possible error spamming)
BugTracker.maxNumberOfErrorsPerSession = 10;
// Current version of the Application / website
BugTracker.appVersion = 0.1;
// Enter the name of the Application / website
BugTracker.appName = "default";
// Disable
BugTracker.isEnabled = true;
// Project Key - it null - no screenshots is possible
BugTracker.projectKey = null;
// mainColor: The color thats used in the screengrab tool
BugTracker.mainColor = "#a5e1bb";
// Post any error to Google Analytics
BugTracker.postErrorToAnalytics = false;

// Private
BugTracker._currentNumberOfErrorsSent = 0;
BugTracker._bugTrackerURL = "https://www.bugsense.com/api/errors";
BugTracker._traceTrackArray = [];
// Snap functionality
BugTracker._screenshotToolLoaded = false;
BugTracker._keyRDown = false;
BugTracker._keyCDown = false;
BugTracker._keySDown = false;

/**
 * Start the bugtrackers.
 */
BugTracker.init = function()
{
    var _previousError = "-";

    var _errorMessage = "None";
    var _errorURL = "None";
    var _errorLineNumber = 0;

    var _postToIframe = null;

    BugTracker.returnErrorDetails = returnErrorDetails;

    /**
     * If Google Analytics is implemented we store the error as an event
     */
    function postToAnalytics()
    {
        if (_gaq == true)
        {
            // _gaq.push(['_trackEvent', category, action, label, value]);
            var getBrowser = BrowserDetect.BROWSER_NAME + " " + BrowserDetect.BROWSER_VERSION;
            var getErrorDetails = "Error: " + _errorMessage + ", File: " + _errorURL + ", Line: " + _errorLineNumber;
            _gaq.push(['_trackEvent', "HM Bug Tracker", "JS Error", "Browser: " + getBrowser, getErrorDetails]);
        }
    }

    /**
     * Gets the error data
     */
    function returnErrorDetails(makeReadable)
    {
        // Normalize the error messages in IE8:
        if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 8)
        {
            var getIsUndefined = _errorMessage.indexOf("is undefined");
            if (getIsUndefined > 0)
            {
                // rename the error to Unncaught ReferenceError: ITEMNAME is not defined
                var getReference = _errorMessage.substr(1, getIsUndefined - 3);
                _errorMessage = "Uncaught ReferenceError: " + getReference + " is not defined";
            }
        }

        var _backTrackBugTracks = "";
        // Loop through the last 5 entries
        var _maxLengthTrackArray = BugTracker._traceTrackArray.length;
        if (_maxLengthTrackArray > 3)
        {
            _maxLengthTrackArray = 3;
        }
        if (BugTracker._traceTrackArray.length > 0)
        {

            for (var q = BugTracker._traceTrackArray.length - _maxLengthTrackArray; q < BugTracker._traceTrackArray.length; q++)
            {
                // Store all trace information
                var getraceStackArrayString = BugTracker._traceTrackArray[q];
                _backTrackBugTracks += getraceStackArrayString;
                if (q < BugTracker._traceTrackArray.length - 1)
                {
                    _backTrackBugTracks += ", ";
                }
            }
        }

        var _backtrackTraces = "";
        if ( typeof trace != "undefined")
        {
            // Loop through the last 5 entries
            var _maxLengthBackTrace = trace._traceArray.length;
            if (_maxLengthBackTrace > 5)
            {
                _maxLengthBackTrace = 5;
            }
            if (trace._traceArray.length > 0)
            {
                for (var k = trace._traceArray.length - _maxLengthBackTrace; k < trace._traceArray.length; k++)
                {
                    // Store all trace information
                    var getStringBackTrace = trace._traceArray[k];
                    if (getStringBackTrace.length > 20)
                    {
                        getStringBackTrace = getStringBackTrace.substr(0, 20);
                    }
                    _backtrackTraces += getStringBackTrace;
                    if (k < trace._traceArray.length - 1)
                    {
                        _backtrackTraces += ", ";
                    }
                }
            }
        }

        // Get user data
        var browserCodeName = navigator.appCodeName;
        var browserName = navigator.appName;
        var browserVersion = navigator.appVersion;
        var browserDeviceType = "Not set";
        // Desktop, Tablet or Smartphone

        if ( typeof BrowserDetect != undefined)
        {
            browserName = BrowserDetect.BROWSER_NAME;
            browserVersion = BrowserDetect.BROWSER_VERSION;
            if (BrowserDetect.TABLET == true)
            {
                browserDeviceType = "Tablet";
            }
            else if (BrowserDetect.MOBILE == true)
            {
                browserDeviceType = "Smartphone";
            }
            else
            {
                browserDeviceType = "Desktop";
            }
        }

        var cookiesisEnabled = navigator.cookieisEnabled;
        var platform = navigator.platform;
        var userAgentHeader = navigator.userAgent;
        // var osVer = navigator.oscpu || "unknown";
        //
        var browserWinWidth = 0;
        var browserWinHeight = 0;
        var getOrientation = window.orientation || "Not set";
        var pageURL = document.location.href;

        // debug data (backtracks)
        var traces = _backtrackTraces;
        var bugtracks = _backTrackBugTracks;
        // max char around 113

        // Get browser window size (cross browser compatible)
        function browserWindowSize()
        {
            if ( typeof (window.innerWidth) == "number")
            {
                //Non-IE
                browserWinWidth = window.innerWidth;
                browserWinHeight = window.innerHeight;
            }
            else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
            {
                //IE 6+ in 'standards compliant mode'
                browserWinWidth = document.documentElement.clientWidth;
                browserWinHeight = document.documentElement.clientHeight;
            }
            else if (document.body && (document.body.clientWidth || document.body.clientHeight))
            {
                //IE 4 compatible
                browserWinWidth = document.body.clientWidth;
                browserWinHeight = document.body.clientHeight;
            }
        }

        browserWindowSize();

        // Gather error details
        var errorDetails_string = '{"client":{"name":"bugsense-js", "version":"1.1"}, ';
        // Request data
        //errorDetails_string += '"request":{"":""}, ';
        errorDetails_string += '"request":{}, ';
        //"remote_ip":"0.0.0.0"
        // Exception data
        errorDetails_string += '"exception":{"message":"' + _errorMessage + '", "where":"Line number ' + _errorLineNumber + ' in ' + _errorURL + '", "klass":"' + _errorURL + '", "backtrace":"Line number ' + _errorLineNumber + ' - File: ' + _errorURL + ' - BugTracker.track: ' + bugtracks + '", "breadcrumbs":"' + BugTracker._traceTrackArray + '"}, ';
        // Basic data
        errorDetails_string += '"application_environment":{"phone":"' + platform + ' ' + browserName + ' ' + browserVersion + '", "appver":"' + BugTracker.appVersion + '", "appname":"' + BugTracker.appName + '", "osver":"' + browserName + ' ' + browserVersion + '", "user_agent":"' + userAgentHeader + '", "device:type":"' + browserDeviceType + '", "window.innerWidth":"' + browserWinWidth + '", "window.innerHeight":"' + browserWinHeight + '", "screen.width":"' + screen.width + '", "screen.height":"' + screen.height + '", "window.orientation":"' + getOrientation + '", "trace":"' + traces + '", "BugTracker.track":"' + bugtracks + '", "url":"' + pageURL + '"}}';
        if (makeReadable == true)
        {
            //  errorDetails_string = errorDetails_string.replace("{", "<br>");
            // errorDetails_string = errorDetails_string.replace("}", "<br>");
            /*    errorDetails_string = errorDetails_string.replace(/{/gi, ""); // %0A = new line
             errorDetails_string = errorDetails_string.replace(/}/gi, "");
             errorDetails_string = errorDetails_string.replace(/:/gi, ": ");
             errorDetails_string = errorDetails_string.replace(/,/gi, "");
             errorDetails_string = errorDetails_string.replace(/"/gi, "");*/

            errorDetails_string = "Last error reported (if any): " + _errorMessage + ", Line number " + _errorLineNumber + " in " + _errorURL + "%0A";
            errorDetails_string += "User Agent: " + userAgentHeader + "%0A";
            errorDetails_string += "Device: " + platform + " " + browserName + " " + browserVersion + "%0A";
            errorDetails_string += "Device Type: " + browserDeviceType + "%0A";
            errorDetails_string += "window.innerWidth: " + window.innerWidth + ", window.innerHeight: " + window.innerHeight + ", screen.width: " + screen.width + ", screen.height: " + screen.height + ", window.orientation: " + getOrientation + "%0A";
            errorDetails_string += "BugTracker.track: " + bugtracks + ", Trace (last ones):" + BugTracker._traceTrackArray + "%0A";

            //BugTracker.track: ' + bugtracks + '", "breadcrumbs":"' + BugTracker._traceTrackArray

        }
        return errorDetails_string;
        /*
         Official bugsense SDK
         var data = {
         // information about the bugsense client
         client: {
         // Obligatory
         *    'name'    : 'bugsense-js',
         // Optional
         *   'version' : '1.1'
         },
         // Optional
         // details & custom data about the exception including url, request, response,…
         request: {
         'custom_data' : custom_data
         },
         // basics about the exception
         exception: {
         // Obligatory
         *  'message'     : message,
         *  'where'       : [ url, line ].join( ':' ),
         *  'klass'       : message.split( ':' )[ 0 ],
         *  'backtrace'   : ( typeof(stack) === 'undefined' ) ? message : stack,
         *  'breadcrumbs': this.config.breadcrumbs
         },
         // basic data ( required )
         application_environment: {
         // Obligatory
         *    'phone'              : window.navigator.platform,
         *    'appver'             : ( this.config.appversion || 'unknown' ),
         *    'appname'            : ( this.config.appname || 'unknown' ),
         *    'osver'              : ( typeof window.device !== 'undefined' ) ? window.device.version : s.substr(s.indexOf('; ')+2,s.length).replace(')',';').split(';')[0] || 'unknown' ,
         // Optional
         'connection_type'    : connection_type,
         *    'user_agent'         : window.navigator.userAgent,
         'cordova'            : ( typeof window.device !== 'undefined' ) ? window.device.cordova : 'unknown',
         'device_name'        : ( typeof window.device !== 'undefined' ) ? window.device.name : 'unknown',
         'log_data'           : this.extraData
         }
         };
         */

    }

    function sendData(dataToSent)
    {
        var sendData_escaped = escape(dataToSent);

        function postToBugTracker(path, params, method)
        {
            // If the iframe doesnt exist - create it
            if (_postToIframe == null)
            {
                _postToIframe = document.createElement("iframe");
                _postToIframe.name = "postToIframe";
                _postToIframe.id = "postToIframe";
                _postToIframe.style.display = "none";
                _postToIframe.style.width = _postToIframe.style.height = "1px";
                document.body.appendChild(_postToIframe);
            }

            method = method || "post";
            // Set method to post by default, if not specified.

            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);
            form.setAttribute("target", "postToIframe");

            for (var key in params)
            {
                if (params.hasOwnProperty(key))
                {
                    var hiddenField2 = document.createElement("input");
                    hiddenField2.setAttribute("type", "hidden");
                    hiddenField2.setAttribute("name", key);
                    hiddenField2.setAttribute("value", params[key]);

                    form.appendChild(hiddenField2);
                }
            }
            form.target = "postToIframe";
            document.body.appendChild(form);
            form.submit();
        }

        // Send the data over to Bugsense
        // if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 8)
        // {
            // Post to the bug tracker
            var addData =
            {
                data: sendData_escaped, key: BugTracker.bugKey
            };

            var d = new Date();
            var n = d.getTime();
            postToBugTracker("http://www.hellomonday.net/_tools/bugtracker/1_0/BugTracker.php?" + n, addData, "POST");
        // }
        // else
        // {
            // var xmlhttp;
            // if (window.XMLHttpRequest)
            // {
                // // code for IE7+, Firefox, Chrome, Opera, Safari
                // xmlhttp = new XMLHttpRequest();
            // }
            // else
            // {
                // // code for IE6, IE5
                // xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            // }
// 
            // xmlhttp.open('POST', BugTracker._bugTrackerURL);
            // xmlhttp.setRequestHeader('X-BugSense-Api-Key', BugTracker.bugKey);
            // xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            // // xmlhttp.onreadystatechange = function()
            // // {
            // // trace(this.responseText)
            // // }
            // xmlhttp.send(sendData_escaped);
        // }
    }

    /**
     * Automatically detect any JS Errors
     */
    window.onerror = function(msg, url, linenumber)
    {
        if (BugTracker.isEnabled === true)
        {
            // Check to see if it is the same error as we had before - if it is dont send it
            if (msg != _errorMessage && _errorURL != url && _errorLineNumber != linenumber)
            {
                // Store the error
                _errorMessage = msg;
                _errorURL = url;
                _errorLineNumber = linenumber;
                if (BugTracker._currentNumberOfErrorsSent < BugTracker.maxNumberOfErrorsPerSession)
                {
                    // Push the error to the bugtracker
                    var getErrorDetails = returnErrorDetails();
                    sendData(getErrorDetails);
                    // Post the error to analytics
                    if (BugTracker.postErrorToAnalytics == true)
                    {
                        postToAnalytics();
                    }
                }
                BugTracker._currentNumberOfErrorsSent++;
            }
        }
        //return true // will stop the error from being displayed
    };
};

/**
 * Track a bugtracker event
 * @param {string} trackString The string that you would like to store in the tracker
 */
BugTracker.track = function(trackString)
{
    BugTracker._traceTrackArray.push(trackString);
};

/*
 * Screengrab tool
 */
BugTracker.initScreenGrab = function()
{
    var _screenshotToolOpen = false;

    var context;
    var canvas;

    var mouseIsDown = false;

    var parentX = 0;
    var parentY = 0;

    var drawType = "draw";
    // draw, box or text
    var drawBox = false;
    var startX = 0;
    var startY = 0;

    var color_green = "#38483e";
    var color_red = "#5a2730";
    var color_orange = "#ac5440";
    
    var moveAllItemsBy = 0;

    // Dialogue
    var _dialogueOpen = false;
    var _summaryBox;
    var _descriptionBox;
    var _usernameBox;
    var _passwordBox;
    var _submitBtn;
    var _cancelBtn;
    var _dialogue;
    var _overlay;

    var firstClick = new Date().getTime();
    var was3Down = false;

    var isRunning = false;

    var onDown = "mousedown";
    var onMove = "mousemove";
    var onUp = "mouseup";
    if (BrowserDetect.TABLET == true || BrowserDetect.MOBILE == true)
    {
        onDown = "touchstart";
        onMove = "touchmove";
        onUp = "touchend";
    }

    /*
     * Listen for Key Down
     */
    function snapDownListener(event)
    {
        if (_screenshotToolOpen == false)
        {
            if (BugTracker.projectKey != null && BugTracker.isEnabled == true)
            {
                // s = 83
                // r = 82
                // c = 67
                var getKeyCode = event.keyCode;
                console.log(getKeyCode)
                if (getKeyCode == 82)
                {
                    BugTracker._keyRDown = true;
                }
                else if (getKeyCode == 83)
                {
                    BugTracker._keySDown = true;
                }
                else if (getKeyCode == 67)
                {
                    BugTracker._keyCDown = true;
                }
                if (BugTracker._keySDown == true && BugTracker._keyRDown == true && BugTracker._keyCDown == true)
                {
                    BugTracker._keySDown = false;
                    BugTracker._keyCDown = false;
                    BugTracker._keyRDown = false;
                    // If first run - load usersnap and then open user snap
                    // if second run - then show usernap
                    if (BugTracker._screenshotToolLoaded == false)
                    {
                        loadScreenshot();
                    }
                    else
                    {
                        grabScreenshot();
                    }

                }
            }
        }
    };

    /*
     * Listen for Key Up
     */
    function snapUpListener(event)
    {

        if (BugTracker.projectKey != null && BugTracker.isEnabled == true)
        {
            var getKeyCode = event.keyCode;
            if (getKeyCode == 82)
            {
                BugTracker._keyRDown = false;
            }
            else if (getKeyCode == 83)
            {
                BugTracker._keySDown = false;
            }
            else if (getKeyCode == 67)
            {
                BugTracker._keyCDown = false;
            }
        }
    }

    /*
     * Load HTML2Canvas tool
     */
    function loadScreenshot()
    {
        _screenshotToolOpen = true;

        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        //s.src = 'js/lib/net/hellomonday/bugtracker/html2canvas.js';
        s.src = 'http://www.hellomonday.net/_tools/bugtracker/1_0/html2canvas.js';
        s.onreadystatechange = function(responseText)
        {
            if (s.readyState == "complete" || s.readyState == "loaded")
            {
                completedLoad();
            }
        };
        s.onload = completedLoad;
        var x = document.getElementsByTagName('head')[0];
        x.appendChild(s);

    }

    function completedLoad()
    {

        setSnapLoaded();
        grabScreenshot();

    }

    function setSnapLoaded()
    {
        if (BugTracker._screenshotToolLoaded == false)
        {
            BugTracker._screenshotToolLoaded = true;
        }
    }

    function grabScreenshot()
    {
        _screenshotToolOpen = true;

        // flashcanvas: "../php/flashcanvas/flashcanvas.js",
        html2canvas([document.body],
        {
            proxy: false, logging: false, profile: true, useCORS: false, width: window.innerWidth, height: window.innerHeight, onrendered: canvasGrabbed
        });
    }
    
    function textSettings(element)
    {
        element.style.textAlign = "left";
        element.style.fontFamily = "arial";
        element.style.color = "#ffffff";
       
    }

    function createButton(left, text, alignRight)
    {
        var addBtn = document.createElement("div");
        addBtn.style.position = "absolute";
        // addBtn.style.top = 0 + "px";
        if (alignRight == true)
        {
            addBtn.style.right = left + "px";
            addBtn.style.borderLeft = "1px solid #26303e";
        }
        else
        {
            addBtn.style.left = left + "px";
            addBtn.style.borderRight = "1px solid #26303e";
        }

        //  addBtn.style.width = 100 + "px";
        addBtn.style.height = 80 + "px";

        addBtn.style.padding = "33px 30px 0px 30px";
        textSettings(addBtn);
        addBtn.innerHTML = text;
        return addBtn;
    }

    var screenGrabHolder;
    function canvasGrabbed(getCanvas)
    {
        canvas = getCanvas;
        screenGrabHolder = document.createElement("div");
        screenGrabHolder.style.position = "absolute";
        screenGrabHolder.style.zIndex = 9999;
        screenGrabHolder.style.top = 0 + "px";
        screenGrabHolder.style.left = 0 + "px";
        screenGrabHolder.style.backgroundColor = document.body.style.backgroundColor;
        screenGrabHolder.style.overflowY = "auto";
        screenGrabHolder.style.overflowX = "hidden";

        var canvasHolder = document.createElement("div");

        // var canvas = document.createElement("canvas");
        // canvas.width = window.innerWidth - 0;
        // canvas.height = window.innerHeight - 4;
        screenGrabHolder.appendChild(canvasHolder);
        canvas.style.backgroundColor = document.body.style.backgroundColor;
        canvasHolder.appendChild(canvas);
        canvasHolder.style.position = "absolute";
        canvasHolder.style.top = 80 + "px";

        screenGrabHolder.style.width = "100%";
        screenGrabHolder.style.height = "100%";

        // Add buttons
        var toolbarHolder = document.createElement("div");
        toolbarHolder.style.position = "fixed";
        toolbarHolder.style.top = 0 + "px";
        toolbarHolder.style.left = 0 + "px";
        toolbarHolder.style.backgroundColor = "#19202a";
        toolbarHolder.style.width = 100 + "%";
        toolbarHolder.style.height = 80 + "px";
        toolbarHolder.style.fontFamily = "arial";
        toolbarHolder.style.fontSize = "14px";
        toolbarHolder.style.color = "#ffffff";
        toolbarHolder.style.zIndex = 9999;
        toolbarHolder.style.fontWeight = 500;
        toolbarHolder.style.textAlign = "left";
        //     toolbarHolder.style.webkitTransform = "translate3d(0px,0px,10px)";
        toolbarHolder.style.backgroundColor = "#101010";
        //toolbarHolder.style.borderBottom = "1px solid #000000";
        screenGrabHolder.appendChild(toolbarHolder);

        // Logo
        var addLogo = new Image();
        addLogo.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAABQCAIAAADjg7zGAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5QzgxRTk5NDVDNEMxMUUyQjY4QkJBNTAxNzk2OTJDNSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo5QzgxRTk5NTVDNEMxMUUyQjY4QkJBNTAxNzk2OTJDNSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjlDODFFOTkyNUM0QzExRTJCNjhCQkE1MDE3OTY5MkM1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjlDODFFOTkzNUM0QzExRTJCNjhCQkE1MDE3OTY5MkM1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+ZX4kGQAAC0lJREFUeNrsnQk4Vvkex6+X0KZFZWuZ9r2QUJJonUppI5WmxbTMlKZui6ZbMtY2xq1MRlpUN0LIFpWihaGyVUqLEOKlQipeNN/3/XO84crM03a7v89znvf5n//5n//5nXO+57f89TxJ9FEd/Q+C+LTw6BEQJDuCZEcQJDuCZEcQJDuCZEcQJDuCZEcQJDuCZEeQ7AiCZEeQ7AiCZEeQ7AiCZEeQ7AiCZEeQ7AiSHUGQ7AiSHUGQ7AiSHUGQ7AiSHUGQ7AiSHUGyIwiSHUGyI4gPgNSnv+S9hKj6nfa798m1brVq+aL6h/qq6TU4j6aG6jF3lzrDAr09+vXpxc159ITPX7LN8ZfNMwwnife8LC3V1p8uEAi4Hust/5w7e5r4mLgbiWbmaz7sU9qyYfXCebPf+xD+R5GUV+z2iS+Zk5sXG3dDS1NdSlISu/vcjvgHnb1+Mykj80lSSurjzCx11cHoL3lZusPJ9ey5S6n3HjQ4T1lZ+cP0jHZt2ygpKrB58JvPL3yUnqE1XA3ty9fiklLu/CXbnr8oupl4q4uKkrx8O9YjLS0N27Ke5FR/plJSDtaWsrIybDcjK3u3i1vUldjMrOwP+5SKiktgPMxQVOjI3R15u7/P6TNh+F2ycK6yklAucEjFJS/ZISisTRs58+9M0c7OyT3h7d/IPAWFz/wCQlu3asVkyoiMuorNYuWSv2fbjYQUbH179+jTuwe/oLBjB3l0jh0z6mrsdTZgpNawtm3kcp/mKyl2wu7NxBQf/+CP8ZSSb6Via9myhergAZTbfXTeVlU12N9cVhY+bIzuiG5dO78z/u3bJs4MQWtrqkNDgwf2kxQ52sZB6ESERUNfbyTXafjtOPzCBzd4ioy0NL6Bcfq6w9WHtmjRvJHJFTp1GK2jhWH1Lenftxduk32TdWOTpCRSC5wIOQp3ebwhg/ob6OngFAkJCfJ2H5jFZsZrVi5t3lyW7cbE3dj4L/t8fkHTBbd105rJEw3wnljPi6Li/W5HjnmdbkS15eWCi9Ex0JmyogLeKzyxrIwMVItTwi9EwaR3nqOUFBLTRfPncEaWlZefOh28y/m3Ht27Bnh51N7LinWYxNTYiBlz5+79BeYWpaWv0IYf3bvbBsJin1NgSMSj9ExxzXn+/quG+hC08/ILNm21t7PaqKKsyI7CH6//2SYmMrBOPrNpm714Bnzh0pUf1m4hb/d+lpiZWK77kSfJ22K9c/7S1ddvJo/QHHZw/85mzZo15XRoxfN3Z6iHzy9c+sP6cYamO51/wwvestFi9YrFjZ977kI0a0Ao+NUbpQ03g+SvvuIdtm9aaW4mwZPYbOVoMMXkp03boVqzuTP3OdkiIm+z3R0ZdY2NXLd6WdcuKla2e86ERGB3QL/exjOnskN77LdCcxUVFa7uR60dnAf262P+3VzuEiO0hkFzBzyOM2fp4boLGrLYYHU7NQ09Otoa8OXIhu/df8jGX7kW5+jkCuFut3NCQoKeP+IT8CWQtxNy5tRhcZdTJ1jIybVmiRqel29ACBo2O1xQrvbt3RNKYmli48wzNmK1rZXdnisx8Wh4eHrBexlOHr/C3MwvMDQ75+l/O/dyTBycFkInAhmS+qmiCBsWEVln2DC1wdOmTEDDzeMEMwlzyrdvBxeLgKiuOsjbL+hu2kMDUbBGdbLW0hoN3A6iLASEoI9dtaGDRo3UZOa5uB5CI+TshfDAE9xVenTrUlRUfOp00IqlC4SB/vwlu1170ejUof3A/n3QQHV16Jh3UNj5yBBvaelmRSUl4eeFycAp/+BVKxbh3OVrLF+/fkO5nZDcp3lPcnK5DbviR7WHq7GwVVlRgZwJ2zfdqnM7/dEjmzI/c1TCr1+kOUbU1T9YbtT4JK9evYbPQAPvtXfP7nq62qIIG11n2Hj90TWRrvYSl0UngnGio+Xl5eIVFQujT7JzmT9mrpT1R4tsA6i0xJNICR6vorKydv6rwvnhfSeOH8NmQ32DBiqhgOBwNCaO1YP0RTNrdZBv7xsY+oVo7ovwdsstLLlKVujeWreKjw7hdruoKLPGogXG2MRPbNtWrinzd1ZRwm/hs+cIXlxnoSjoAFaQNkL4+SgmXJut6+H2UOciwnIZFUNZqVPNJ5TPdebnF9Q5yuV8XLuyqlJsEoWaCv0518kt3HCrOXWySTPTWUgYkOrtcjnArTTB582ZMQUDTGYZurp7Gs80hCi9fAKppGgqghqtbLd34twAt27XlBnKyspYIVynAmCNiorKxk+/dCW2srIS6TyCYIMRVujJBNVGilev3NqeQFDRFDs5S5pJ1b6URsph220bUI4c8vS+npCUm5ufm1er+PTHmUj7EBlMZk07E3oOfhSePlOkYEWFjk/z+BRk30NG5hPuLSJhYpsw6Xa2Y8t77yXtQTp7f/AKXCeS+pqVwvvvWbYtKo6NT+DCYv0IC7gsXkVs1YPziPfSHjbFzscZWXVsAyztaxC/gNAZpuZuh47bbN1wMewU8kjxowePejGROTta8Xi8/5wKYCatt1iOeIIGynM8kF49vkEW+H8kO650kOC9Y4OMTLWT4Enw2OIZC8EmMw3ZYpVwDWKBCaqEPD6fLZVxa2Z1PFnLFsLxXn5n2C4+fW4A+wsY6rtL0TEN2CYyiXsfXD3L1bBcsGNONDA4goVO0zlGtaWMqF1ZVeUfdFY4snm135IWK8BbipwZszwy6irrNJ0zna2tjNQaxiV83PIe0s3qle3EZNZo364tfnV1tMRvISHpFjY0hgzqn5Obd+my8DY7yrfv1rUz8ry9u20Mxuh4HdlvNHXiQtPZn+Htf/r/6u7YQRck0Xgc1d7o/qMXxcXHvfzxDmZMm4QsuGf36r/XJSTfFggEvv4hO2x+hkwRHRKTb3fprIwEH17Q0spx7Srzgf36MDk+TM9AAocGN0Np6avbd9Psd+1FmbnEzKSqqgq5NoZNMBg9dPAA5NfLLDbFXU8Ut23Z4vm6OprwMdATYmtSyp1HjzNRV0aH+8IA2x0ud+7d/+lHcyWFTjAD4zEnTLp1517yrVQYCXFEXYnFnEMG90dGD++IuhtFia3VRiSpLI9Eyo8xqBXWrvqeVaAYhhkiLkQrKyuYzp6OHlwUqaHq0IEXo659O0Ff5JUf4DZRq2roTj64f5fuSE1EUqd97ngmUCeSPB//EGsHZ/F7QZDd72SLhtNedzhFljc7/rLZNyB0ttFky20O+53t9h44rKWhht+vP7crKXmJDcnHOzmcoLysTJId4sINIzAkIo9fsHThXCh1nP4ofsGzk76B/3Y9JC/fDoNj42/Wn198BlR/O5xc428kzTc2MtDTmTZ5fH5BoY9/sPuRk1wE53jz5g1Ov1bzpzCmXXi4Yyf9EJgQYSEdZmTag0e1Y169Dgo7/+hx1pKFJloaqjraGi+KSs5FXj583BslCGoajL9zNw0bNydy1qd5+U/FEjJc2sbRhc8vnG00BdfCGBRbLZrLigfBV6JSdM1GK+gMH4+Tw7Y3ZeWZWdnO+w56nvRjha360EFJt1KLi0vgPuHO28i1ZgtP7LpIiBFb0YCa8/mFCBrMWX793o74ePged4Orvp2aNnPe9717dQ/2OXL6TNhmK8cvzU7693ZfD/CLrASRkhImgquXL0L4djt04gs0VYre1ldDebkACR9ql/DzUaj0x44Z5eHpVSdj+UL4DP/ejvh43EhM0dZUnzxpLIrWw8d9ft3n3vR/oUO5HfGVQ7kdQbIjSHYEQbIjSHYEQbIjSHYEQbIjSHYEQbIjSHYEyY4gSHYEyY4gSHYEyY4gSHYEyY4gSHYEyY4g2REEyY4g2REEyY4g2REEyY4g2REEyY74nPwpwADT6OIRcxHwmAAAAABJRU5ErkJggg==";
        if (BrowserDetect.MOBILE == false)
        {
            toolbarHolder.appendChild(addLogo);
        }
        
        if (BrowserDetect.MOBILE == true)
        {
            moveAllItemsBy = -210;
        }

        var drawBtn = createButton(210 + moveAllItemsBy, "Draw");
        drawBtn.style.top = 0 + "px";
        toolbarHolder.appendChild(drawBtn);

        var boxBtn = createButton(300 + moveAllItemsBy, "Box");
        boxBtn.style.top = 0 + "px";
        toolbarHolder.appendChild(boxBtn);

        var textBtn = createButton(380 + moveAllItemsBy, "Text");
        textBtn.style.top = 0 + "px";
        toolbarHolder.appendChild(textBtn);

        var submitBtn = createButton(15, "Submit", true);
        submitBtn.style.top = 0 + "px";
        toolbarHolder.appendChild(submitBtn);

        var cancelBtn = createButton(120, "Cancel", true);
        cancelBtn.style.top = 0 + "px";
        toolbarHolder.appendChild(cancelBtn);

        // Get image functionality
        if (BrowserDetect.BROWSER_NAME == "Explorer")
        {
            G_vmlCanvasManager.initElement(canvas);
        }
        context = canvas.getContext("2d");

        // Add drawing funcitonality
        // context.globalCompositeOperation = "source-over";

        context.strokeStyle = BugTracker.mainColor;
        context.lineJoin = "round";
        context.lineWidth = 5;

        context.shadowColor = "rgba(0, 0, 0, 0.8)";
        context.shadowBlur = 7;
        context.shadowOffsetX = 0;
        context.shadowOffsetY = 3;
        context.lineJoin = "miter";

        context.fillStyle = context.strokeStyle;
        context.font = "bold 26px sans-serif";

        document.body.appendChild(screenGrabHolder);
        isRunning = true;

        //displayDialogue();
    }

    function kill()
    {
        mouseWasJustDown = false;
        document.body.removeChild(screenGrabHolder);
        screenGrabHolder = null;
        isRunning = false;
        drawType = "draw";
        _screenshotToolOpen = false;
    }

    // Open the reporting dialogue
    function displayDialogue()
    {
        _dialogueOpen = true;

        var overlay = document.createElement("div");
        overlay.style.position = "fixed";
        overlay.style.width = "100%";
        overlay.style.height = "100%";
        overlay.style.backgroundColor = "#ffffff";
        overlay.style.opacity = 0.6;
        overlay.style.zIndex = 9999;
        _overlay = overlay;

        // Add buttons
        var dialogue = document.createElement("div");
        dialogue.style.position = "fixed";
        dialogue.style.backgroundColor = "#19202a";
        dialogue.style.width = 320 + "px";
        dialogue.style.height = 450 + "px";

        dialogue.style.left = 50 + "%";
        dialogue.style.top = 50 + "%";

        dialogue.style.marginLeft = -(320 / 2) + "px";
        dialogue.style.marginTop = -(450 / 2) + "px";

        dialogue.style.fontFamily = "arial";
        dialogue.style.fontSize = "14px";
        dialogue.style.color = "#ffffff";
        dialogue.style.zIndex = 10000;
        dialogue.style.fontWeight = 500;
        //     toolbarHolder.style.webkitTransform = "translate3d(0px,0px,10px)";
        dialogue.style.backgroundColor = "#101010";
        // dialogue.style.border = "1px solid #26303e";

        _dialogue = dialogue;

        // Add header
        var headerText = document.createElement("div");
        headerText.innerHTML = "Submit — Screenshot";
        headerText.style.top = "20px";
        headerText.style.left = "20px";
        headerText.style.color = "#ffffff";
        headerText.style.width = "100%";
        headerText.style.height = "80px";
        headerText.style.fontWeight = "bold";
        headerText.style.backgroundColor = "#26303e";
        headerText.style.padding = "35px 0px 20px 20px";
        headerText.style.fontFamily = "arial";
        headerText.style.marginBottom = 15 + "px";
        dialogue.appendChild(headerText);

        function getInputField(text, height)
        {
            var inputObject = document.createElement("div");
            inputObject.style.padding = "5px 0px 10px 20px";
            inputObject.style.color = "#ffffff";
            inputObject.style.fontFamily = "arial";
            inputObject.style.fontSize = 12 + "px";

            // headline
            var headline = document.createElement("div");
            headline.style.color = "#ffffff";
            headline.style.fontFamily = "arial";
            headline.style.marginBottom = 5 + "px";
            headline.innerHTML = text;

            // input box
            var inputBox = document.createElement("input");
            if (height != null)
            {
                inputBox = document.createElement("textarea");
            }
            inputBox.style.width = 275 + "px";
            inputBox.style.border = 0;
            inputBox.style.height = 25 + "px";
            inputBox.style.color = "#19202a";
            inputBox.style.fontFamily = "arial";
            inputBox.style.paddingTop = 2 + "px";
            inputBox.style.paddingLeft = 8 + "px";
            inputBox.style.fontFamily = "arial";

            inputBox.style.webkitUserSelect = inputBox.style.mozUserSelect = inputBox.style.msUserSelect = inputBox.style.khtmlUserSelect = inputBox.style.userSelect = "auto";

            inputBox.type = "text";
            if (text == "Password:")
            {
                inputBox.type = "password";
            }

            //    inputBox.contenteditable = true;

            //inputBox.value = "Input";
            if (height != null)
            {
                inputBox.style.height = height + "px";
            }
            inputBox.style.backgroundColor = "#ffffff";
            inputBox.style.webkitBorderRadius = inputBox.style.borderRadius = inputBox.style.mozBorderRadius = inputBox.style.msBorderRadius = 12 + "px";

            inputObject.appendChild(headline);
            inputObject.appendChild(inputBox);
            dialogue.appendChild(inputObject);

            inputObject._inputBox = inputBox;

            return inputObject;
        }

        // Summary
        var summary = getInputField("Summary:");
        _summaryBox = summary._inputBox;

        // Description
        var description = getInputField("Description:", 70);
        _descriptionBox = description._inputBox;

        // User name
        var username = getInputField("Username:");
        var getStoreUsername = localStorage.BugTracker_username;
        if (getStoreUsername != "undefined" && getStoreUsername != null)
        {
            username._inputBox.value = getStoreUsername;
        }
        _usernameBox = username._inputBox;
        // Password
        var password = getInputField("Password:");
        var getStoredPassword = localStorage.BugTracker_password;
        if (getStoredPassword != "undefined" && getStoredPassword != null)
        {
            password._inputBox.value = getStoredPassword;
        }
        _passwordBox = password._inputBox;

        var buttons = document.createElement("div");
        dialogue.appendChild(buttons);

        // Add header
        var endArea = document.createElement("div");
        endArea.style.width = "100%";
        endArea.style.height = "80px";
        endArea.style.fontWeight = "bold";
        endArea.style.backgroundColor = "#26303e";
        endArea.style.marginTop = 15 + "px";
        dialogue.appendChild(endArea);

        var cancelBtn = createButton(0, "Cancel");
        cancelBtn._name = "cancel";
        endArea.appendChild(cancelBtn);
        _cancelBtn = cancelBtn;

        var submitBtn = createButton(0, "Submit", true);
        submitBtn._name = "submit";
        endArea.appendChild(submitBtn);
        _submitBtn = submitBtn;

        dialogue.appendChild(endArea);

        screenGrabHolder.appendChild(overlay);
        screenGrabHolder.appendChild(dialogue);

    }

    // Close the report dialogue
    function closeDialogue()
    {
        screenGrabHolder.removeChild(_dialogue);
        screenGrabHolder.removeChild(_overlay);
        _dialogueOpen = false;
    }

    function sendImage()
    {

        localStorage.BugTracker_username = _usernameBox.value;
        localStorage.BugTracker_password = _passwordBox.value;

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', "http://www.hellomonday.net/_tools/bugtracker/1_0/communicate.php");
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xmlhttp.onreadystatechange = function()
        // {
        // trace(this.responseText)
        // }

        var summary = _summaryBox.value;
        var description = _descriptionBox.value;
        description += "%0A%0A" + "System info:%0A" + BugTracker.returnErrorDetails(true);

        var username = _usernameBox.value;
        var password = _passwordBox.value;

        // %0A = new line
        var key = BugTracker.projectKey;
        xmlhttp.send("key=" + key + "&summary=" + summary + "&description=" + description + "&username=" + username + "&password=" + password + "&imageData=" + canvas.toDataURL());
        kill();
    }

    function checkIfStillDown()
    {
        var timeDiff = firstClick - new Date().getTime();
        // alert(timeDiff)
        if (timeDiff < -2000)
        {
            openingGrab = true;
            if (BugTracker._screenshotToolLoaded == false)
            {
                loadScreenshot();
            }
            else
            {
                //  loadSnap();
                grabScreenshot();
            }
            clearInterval(checkInterval);
            was3Down = false;
        }
    }

    var mouseWasJustDown = false;
    var checkInterval;

    // Function to add to the whole screen in order to make sure nothing else is called if the screengrab tool is started
    function preventAll(event)
    {
        var openingGrab = false;
        var touchCount = 1;
        if (event.touches != null)
        {
            touchCount = event.touches.length;
        }
        if (isRunning == false)
        {
            if (event.type == "touchstart" && event.touches != null && event.touches.length == 3 && was3Down == false)
            {
                firstClick = new Date().getTime();
                was3Down = true;
                checkInterval = setInterval(checkIfStillDown, 100);
            }
            else if (event.touches != null && event.type == "touchend" && was3Down == true)
            {
                clearInterval(checkInterval);
                was3Down = false;
                //      was3Down = false;

            }
            else
            {
                //    was3Down = false;
            }

            if (was3Down == true || openingGrab == true)
            {
                event.cancelBubble = true;
                event.stopPropagation();
                event.preventDefault();
                openingGrab = false;
                //was3Down = false;
            }
        }

        if (isRunning == true && (touchCount == 1 || touchCount == 0))
        {
           
            if (event.type == "mousedown" || event.type == "touchstart")
            {
                startX = event.pageX - canvas.offsetLeft;
                startY = event.pageY - canvas.offsetTop + screenGrabHolder.scrollTop;
                parentX = event.pageX - canvas.offsetLeft;
                parentY = event.pageY - canvas.offsetTop + screenGrabHolder.scrollTop;

                if (_dialogueOpen == true)
                {
                    var buttons = [_summaryBox, _descriptionBox, _usernameBox, _passwordBox, _cancelBtn, _submitBtn];
                    // _usernameBox,
                    for (var i = 0; i < buttons.length; i++)
                    {

                        var currButton = buttons[i];
                        var getX = currButton.offsetLeft + _dialogue.offsetLeft;
                        var getY = currButton.offsetTop + _dialogue.offsetTop;
                        var getHeight = parseInt(currButton.offsetHeight);
                        var getWidth = parseInt(currButton.offsetWidth);

                        if (event.pageX > getX && event.pageX < getWidth + getX && event.pageY > getY && event.pageY < getHeight + getY)
                        {

                            if (currButton.type != null)
                            {
                                currButton.setSelectionRange(0, 0);
                                currButton.focus();
                            }
                            else
                            {
                                if (currButton._name == "submit")
                                {
                                    sendImage();
                                }
                                else if (currButton._name == "cancel")
                                {
                                    closeDialogue();
                                }
                            }
                        }
                    }

                    return;
                }

                if (event.pageY <= 80)
                {
                    mouseWasJustDown = true;
                    // check where it is on the X axis
                    //  console.log("Clicked at: " + event.pageX);
                    var getX = event.pageX;
                    if (getX > 210 + moveAllItemsBy && getX < 300 + moveAllItemsBy)
                    {
                        // Draw
                        drawType = "draw";
                    }
                    else if (getX >= 300 + moveAllItemsBy && getX < 380 + moveAllItemsBy)
                    {
                        // Box
                        drawType = "box";
                    }
                    else if (getX >= 380 + moveAllItemsBy && getX < 470 + moveAllItemsBy)
                    {
                        // Insert Text
                        drawType = "text";
                    }
                    else if (getX > window.innerWidth - 240 && getX < window.innerWidth - 120)
                    {
                        // Cancel
                        kill();
                    }
                    else if (getX >= window.innerWidth - 120 && getX < window.innerWidth)
                    {
                        // Submit
                        displayDialogue();
                    }
                }
                else
                {
                    mouseIsDown = true;
                }
            }
            else if (event.type == "mouseup" || event.type == "touchend")
            {
                if (mouseWasJustDown == false)
                {
                    if (drawType == "box" && drawBox == true)
                    {
                        context.strokeRect(startX, startY - 80, parentX - startX, parentY - startY)
                    }
                    else if (drawType == "text")
                    {
                        // startX = event.pageX - canvas.offsetLeft;
                        // startY = event.pageY - canvas.offsetTop - 80;

                        var name = prompt("Insert Text: ");
                     
                        if (name != null && name != "")
                        {
                            context.fillText(name, startX, startY - 80);
                        }

                    }
                    drawBox = false;
                    mouseIsDown = false;
                }
                else
                {
                    mouseWasJustDown = false;
                }
            }
            else if (event.type == "mousemove" || event.type == "touchmove")
            {
                if (mouseIsDown == true)
                {
                    //console.log("_setMouseMove")
                    var currX = event.pageX - canvas.offsetLeft;
                    var currY = event.pageY - canvas.offsetTop + screenGrabHolder.scrollTop;
                    if (drawType == "draw")
                    {
                        context.beginPath();
                        context.moveTo(parentX, parentY - 80);
                        context.lineTo(currX, currY - 80);
                        context.closePath();
                        context.stroke();

                    }
                    else if (drawType == "box")
                    {

                        drawBox = true;
                    }

                }
                if (canvas != null)
                {
                    parentX = event.pageX - canvas.offsetLeft;
                    parentY = event.pageY - canvas.offsetTop + screenGrabHolder.scrollTop;
                }
            }

            event.cancelBubble = true;
            event.stopPropagation();
            event.preventDefault();
        }

    }

       if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 8)
       {
       }
       else {
    document.addEventListener(onMove, preventAll, false);
    document.addEventListener(onDown, preventAll, false);
    document.addEventListener(onUp, preventAll, false);

    window.addEventListener(onMove, preventAll, true);
    window.addEventListener(onDown, preventAll, true);
    window.addEventListener(onUp, preventAll, true);
}
    if (document.addEventListener)
    {
        if (BrowserDetect.TABLET == true || BrowserDetect.MOBILE == true)
        {
            // document.addEventListener("touchstart", snapTouchDownListener, false);
            // document.addEventListener("keyup", snapUpListener, false);
        }
        else
        {
            document.addEventListener("keydown", snapDownListener, false);
            document.addEventListener("keyup", snapUpListener, false);
        }
    }
    else if (document.attachEvent)
    {
        // IE
        // document.attachEvent("onkeydown", snapDownListener);
        // document.attachEvent("onkeyup", snapUpListener);
    }

    function autoStart()
    {
        loadScreenshot();

    }

    setTimeout(autoStart, 0)

}
BugTracker.initScreenGrab();
BugTracker.init();