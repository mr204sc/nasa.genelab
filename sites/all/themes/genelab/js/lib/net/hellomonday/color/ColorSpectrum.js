function ColorSpectrum(spectrumWidth)
{
    trace(ColorSpectrum);

    this._spectrumWidth = spectrumWidth;
    this._sepctrumArray = null;

    trace(this._spectrumWidth, "this._spectrumWidth");

    this.getColors = function()
    {
        if (!this._spectrumArray)
        {
            this._spectrumArray = [];

            var i = 0;
            var l = this._spectrumWidth;
            var pct = 0;
            var nRadians = 0;
            var nR = null;
            var nG = null;
            var nB = null;
            var nColor = null;
            var hex = "";

            for ( i = 0; i < l; i += 1)
            {
                pct = i / l;

                nRadians = (-360 * pct) * (Math.PI / 180);

                nR = Math.cos(nRadians) * 127 + 128 << 16;
                nG = Math.cos(nRadians + 2 * Math.PI / 3) * 127 + 128 << 8;
                nB = Math.cos(nRadians + 4 * Math.PI / 3) * 127 + 128;

                nColor = nR | nG | nB;

                hex = nColor.toString(16);
                if (hex.length < 6)
                {
                    hex = "0" + hex;
                }

                this._spectrumArray.push(hex);
            }
        }

        return this._spectrumArray;
    };
}

