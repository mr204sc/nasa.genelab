function SMAudioObjectBrigde(path, params)
{
    var _instance                   = {};
    var _signalChain;
    
    var _pan                        = null;
    
    var _soundObject                = soundManager.getSoundById(path); 
    
    function init()
    {
        _instance.path = path;
        _instance.params = params;
    }
    
    /**
     * @param {Object} effect - Only accepts "SMAudioEffect_2dPannerBrigde"
     */
    _instance.addEffect = function(effect)
    {
        _pan = effect;
        _pan.applySoundObject(_soundObject);
    }
    
    /**
     * @param {Object} effect - Only accepts "SMAudioEffect_2dPannerBrigde"
     */
    _instance.removeEffect = function(effect)
    {
        _pan.removeSoundObject();
        _pan = null;
    }
    
    _instance.play = function(params)
    {
        _soundObject.play(params);
        
        if(_pan)
        {
            _pan.update();
        }
    }
    
     _instance.pause = function()
    {
        _soundObject.pause();
    }
    
     _instance.resume = function()
    {
        _soundObject.resume();
        
        if(_pan)
        {
            _pan.update();
        }
    }
    
     _instance.stop = function()
    {
        _soundObject.stop();
    }
    
    Object.defineProperty(_instance, "volume", {get: getVolume, set: setVolume});
    function getVolume()
    {
        return _soundObject.volume * .01;
    }
    
    function setVolume(value)
    {
        if(value > 1)
        {
            value = 1;
        }
        
        _soundObject.setVolume(value * 100);
    }
    
    init();

    return _instance;
}