function AudioSignalChain(input, dryOutput, wetOutput)
{
    var _instance                   = {};
    
    function init()
    {
        _instance.dryEffects = [];
        _instance.wetEffects = [];
    }
    
    _instance.addInsert = function(effect)
    {
        _instance.dryEffects.push(effect);
        _instance.update();
    }
    
    _instance.addSend = function(effect)
    {
        _instance.wetEffects.push(effect);
        _instance.update();
    }
    
    _instance.removeInsert = function(effect)
    {
        var i = 0;
		var l = _instance.dryEffects.length;
		
		for(i; i < l; i++)
		{
		    if(_instance.dryEffects[i].id == effect.id)
		    {
		        _instance.dryEffects.splice(i, 1);
		        break;
		    }		 
		}
        
        _instance.update();
    }
    
    _instance.removeSend = function(node)
    {
        var i = 0;
        var l = _instance.wetEffects.length;
        
        for(i; i < l; i++)
        {
            if(_instance.wetEffects[i].id == node.id)
            {
                _instance.wetEffects.splice(i, 1);
                break;
            }        
        }
        
        _instance.update();
    }
    
    _instance.update = function()
    {
       input.disconnect();
        
       updateDry();
       updateWet();
    }
    
    function updateDry()
    {
        var i = 1;
        var l = _instance.dryEffects.length;
        
        if(l > 0)
        {
            input.connect(_instance.dryEffects[0].input);
            
            for(i; i < l; i++)
            {
                _instance.dryEffects[i-1].output.connect(_instance.dryEffects[i].input);      
            }
            
            _instance.dryEffects[i-1].output.connect(dryOutput);    
        }
        else
        {
            // If there is no dry notes in the chain, connect input directly to dry output
            input.connect(dryOutput);    
        }
    }
    
    function updateWet()
    {
        var i = 1;
        var l = _instance.wetEffects.length;
        
        if(l > 0)
        {
            input.connect(_instance.wetEffects[0].input);
            
            for(i; i < l; i++)
            {
                _instance.wetEffects[i-1].output.connect(_instance.wetEffects[i].input);      
            }
            
            _instance.wetEffects[i].output.connect(wetOutput);    
        }        
    }
    
    init();

    return _instance;
}