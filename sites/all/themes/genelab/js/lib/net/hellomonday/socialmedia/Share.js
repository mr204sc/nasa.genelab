
var Share = {};

Share.twitter = function(text) {
    window.open("https://twitter.com/intent/tweet?source=webclient&text=" + text, "_blank", "width=575, height=400");
};

Share.facebook = function(url) {
    window.open("https://www.facebook.com/sharer/sharer.php?u=" + url, "_blank",  "width=640, height=470");
};
