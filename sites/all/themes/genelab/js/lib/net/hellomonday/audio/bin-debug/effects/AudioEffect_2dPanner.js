/**
 * 
 * @author Torben (torben@hellomonday.com)
 * 
 * As there is no simple pan implementation in the web audio api this is a way to get around it.
 * Though this will create overhead cause 4 nodes are needed to make this work. 
 * 
 */
function AudioEffect_2dPanner(pan)
{
    var _instance                   = {};
    _instance.outputType            = "dry";
    
    var _pan                        = pan || 0;
    
    var _leftPan;
    var _rightPan
    
    var _splitterChannel;
    var _mergerChannel;
    
    function init()
    {
        _splitterChannel = HelloAudio.mainBus.createChannelSplitter(2);
        _mergerChannel = HelloAudio.mainBus.createChannelMerger(2);
        
        _leftPan = HelloAudio.mainBus.createGain();
        _rightPan = HelloAudio.mainBus.createGain();
        
        // Split the input into two channels (left/right) 
        _splitterChannel.connect(_leftPan, 0);
        _splitterChannel.connect(_rightPan, 1);
        
        // Merge them back after pan is applied
        _leftPan.connect(_mergerChannel, 0, 0);
        _rightPan.connect(_mergerChannel, 0, 1);
        
        _instance.id = HelloAudio.addEffect(_instance);
        _instance.input = _splitterChannel;
        _instance.output = _mergerChannel;
        
        setPan(_pan);
    }
    

    Object.defineProperty(_instance, "pan", {get: getPan, set: setPan});
    function getPan()
    {
        return _pan;
    }
    
    function setPan(value)
    {
        _pan = value;
        
        _leftPan.gain.value = (_pan * -.5) + .5;
        _rightPan.gain.value = (_pan * .5 ) + .5;
    }
    
    _instance.dispose = function()
    {
        HelloAudio.removeEffect(_instance.id);
        _instance = null;
    }
    
    init();

    return _instance;
}