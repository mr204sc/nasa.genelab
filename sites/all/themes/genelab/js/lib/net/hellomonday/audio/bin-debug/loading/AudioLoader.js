/**
 * @param {Object} window
 */
(function AudioLoader(window)
{
	var _instance					= {};
	
	var _buffers;
	var _mainBus;
	var _que;
	var _busy                       = false;
	
	var _curGroup;
	var _curGroupTotal;
	
	var _initialized                = false;

	_instance.init = function(mainBus)
	{
	   _initialized = true;
	   _mainBus = mainBus;
	   _buffers = [];
	   _que = [];
	}
	
	/**
     * @param {*} data - Array or String containing the path(s) to load 
	 */
	_instance.loadAudio = function(data, completeCallback, updateCallback)
	{
	    if(!_initialized)
	    {
	        return;
	    }
	    
        var type = Object.prototype.toString.call(data).indexOf("Array") > -1 ? "Array" : "String";
   
        var audioGroup = {};
        audioGroup.completeCallback = completeCallback;
        audioGroup.updateCallback = updateCallback;
        
        if(type != "Array")
        {
            audioGroup.path = []; 
            audioGroup.path.push(data);
        }
        else
        {
            audioGroup.path = data;
        }
        
	    if(!_busy)
        {
            loadAudioGroup(audioGroup);
        }
        else
        {
            _que.push(audioGroup);
        }
	}
	
	function loadAudioGroup(group)
	{
	    _busy = true;
	    //trace("loadAudioGroup", "AudioLoader");
	    
	    _curGroup = group;
	    _curGroupTotal = group.path.length;
	    
	    loadNextBuffer(_curGroup.path[0]);         
	}
	
	function loadNextBuffer(path)
	{
	    // Check if buffer exists
	    if(_instance.getAudioBuffer(path) != null)
	    {
	        nextPath();
	        return;
	    }
	    
	    // Load buffer asynchronously
        var request = new XMLHttpRequest();
        request.open("GET", path, true);
        request.responseType = "arraybuffer";
        request.onerror = bufferRequestError;
        request.onload = bufferLoaded;
        request.send();
	}
	
	function bufferRequestError()
	{
	    //trace("XHR error", "AudioLoader");
	}
	
	/**
	 * Audio file loaded (into a buffer)
	 */
	function bufferLoaded(e)
	{
	    // Start Decoding the audio file (into memory)
	    _mainBus.decodeAudioData(e.target.response, bufferDecodeSucess, bufferDecodeError);
	}
	
	function bufferDecodeSucess(buffer)
	{
	    if(!buffer)
        {
            //trace("bufferDecodeSucess - error decoding file data", "AudioLoader");
        }
        
        //trace("bufferDecodeSucess", "AudioLoader");
        
        // Add the buffer to the buffer list
        _buffers.push({path: _curGroup.path[0], buffer: buffer});
        
        nextPath();
	}
	
	function nextPath()
	{
	     // Remove the loaded audio path from the group
        _curGroup.path.shift();
        
        var remainingPaths = _curGroup.path.length
        
        if(_curGroup.updateCallback)
        {
            var perc = 1 - (remainingPaths / _curGroupTotal);
            _curGroup.updateCallback(perc)
        }
        
        // Run complete callback if the group is finished loading
        if(remainingPaths < 1)
        {
            _busy = false;
            
            if(_curGroup.completeCallback)
            {
                _curGroup.completeCallback();
            }
            
            // Check if if there is more groups to load in the que
            if(_que.length > 0)
            {
                 loadAudioGroup(_que.shift());
            }
        }
        // Otherwise load the next buffer
        else
        {
            loadNextBuffer(_curGroup.path[0]);
        }
	}
	
	
	function bufferDecodeError()
	{
	    //trace("bufferDecodeError", "AudioLoader");   
	}
	
	_instance.getAudioBuffer = function(path)
	{
	    var i = 0;
        var l = _buffers.length;
        var buffer = null;
        
        for(i; i < l; i++)
        {
            if(path == _buffers[i].path)
            {
                buffer = _buffers[i].buffer;
                break;
            }
        }
        
        return buffer
	}
	
    /**
     * @param {*} data - Array or String containing the path(s) to remove 
     */
	_instance.removeAudioBuffer = function(data)
    {
        var type = Object.prototype.toString.call(data).indexOf("Array") > -1 ? "Array" : "String";
        var curPath;
        var i = 0;
        var l = _buffers.length;
        var buffer = null;
        
        if(type != "Array")
        {
            curPath = data;
            
            for(i; i < l; i++)
            {
                if(curPath == _buffers[i].path)
                {
                    buffer = _buffers[i].buffer;
                    _buffers.splice(i, 1);
                    buffer = null;
                    break;
                }
            }
        }
        else
        {
            var j = 0;
			var k = data.length;
			
			for(j; j < k; j++)
			{
			    curPath = data[j];
			    i = 0;
            
                for(i; i < l; i++)
                {
                    if(curPath == _buffers[i].path)
                    {
                        buffer = _buffers[i].buffer;
                        _buffers.splice(i, 1);
                        buffer = null;
                        break;
                    }
                }
			}
            
        }
    }
	
	window.AudioLoader = _instance;
})(window);
