(function Minify()
{
    var _instance =
    {
    };

    var menuItems = [
    {
        menuItemName: "Minify JS", onClickFunction: MinifyJS
    },
    {
        menuItemName: "Data-URI Creator", onClickFunction: CreateDataURI
    }];

   
    var WHITESPACE_ONLY = "WHITESPACE_ONLY";
    var SIMPLE_OPTIMIZATIONS = "SIMPLE_OPTIMIZATIONS";
    var ADVANCED_OPTIMIZATIONS = "ADVANCED_OPTIMIZATIONS";
    _instance.compilationLevel = SIMPLE_OPTIMIZATIONS;
    _instance.jsOutputFilename = "minified.js";
    _instance.dataURIOutputFilename = "datauri.txt";

    var _textOutput = null;

    /*
     * Setup key listeners / key check
     */
    function init()
    {
        // Check for key down
        var _currentKeys = [];
        var _keyIsDown = false;

        function detectKeyUp(event)
        {
            _currentKeys = [];
            _keyIsDown = false;
        };

        function detectKeyDown(event)
        {
            _keyIsDown = true;
            _currentKeys.push(event.keyCode);
            var matchCount = 0;
            for (var i = 0; i < _currentKeys.length; i++)
            {
                var currItem = _currentKeys[i];
                // 17 = CTRL, M = 77
                if (currItem == 17 || currItem == 77)
                {
                    matchCount++;
                }
            }
            if (_currentKeys.length == 2 && matchCount == 2)
            {
                createModalBox();
            }
        };

        // Key detection
        window.addEventListener("keyup", detectKeyUp, false);
        window.addEventListener("keydown", detectKeyDown, false);
    }

    function createModalBox()
    {
        // Modal Box
        var modalBox = document.createElement("div");
        modalBox.style.position = "absolute";
        modalBox.style.zIndex = 9999999999;
        modalBox.style.top = 0 + "px";
        modalBox.style.left = 0 + "px";
        modalBox.style.width = "100%";
        modalBox.style.height = "100%";

        // Background
        var modalBG = document.createElement("div");
        modalBG.style.position = "absolute";
        modalBG.style.top = 0 + "px";
        modalBG.style.left = 0 + "px";
        modalBG.style.width = "100%";
        modalBG.style.height = "100%";
        modalBG.style.opacity = 0.7;
        modalBG.style.backgroundColor = "#000000";
        modalBox.appendChild(modalBG);

        // modal area inside
        var modalArea = document.createElement("div");
        modalArea.style.position = "absolute";
        modalArea.style.top = 0 + "px";
        modalArea.style.left = window.innerWidth / 2 - 450 / 2 + "px";
        modalArea.style.width = "450px";
        modalArea.style.height = "300px";
        modalArea.style.backgroundColor = "#333333";
        modalBox.appendChild(modalArea);

        var divider = document.createElement("div");
        divider.style.position = "absolute";
        divider.style.top = 0 + "px";
        divider.style.left = 150 + "px";
        divider.style.width = "1px";
        divider.style.height = "300px";
        divider.style.backgroundColor = "#555555";
        modalArea.appendChild(divider);

        // Menu Items
        for (var i = 0; i < menuItems.length; i++)
        {
            var getItem = menuItems[i];
            var menuItem = document.createElement("div");
            menuItem.style.position = "absolute";
            menuItem.style.color = "#ffffff";
            menuItem.style.fontSize = 12 + "px";
            menuItem.style.fontFamily = "arial, verdana";
            menuItem.innerHTML = getItem.menuItemName;
            menuItem.style.pointerEvents = "all";
            menuItem.style.top = 20 * i + 20 + "px";
            menuItem.style.left = 20 + "px";
            menuItem.addEventListener("click", getItem.onClickFunction);
            modalArea.appendChild(menuItem);
        }

        var infoTextArea = document.createElement("div");
        infoTextArea.style.position = "absolute";
        infoTextArea.style.top = 20 + "px";
        infoTextArea.style.left = 180 + "px";
        infoTextArea.style.width = "250px";
        infoTextArea.style.height = "250px";
        infoTextArea.innerHTML = "Choose an action on the left hand side.";
        infoTextArea.style.color = "#ffffff";
        infoTextArea.style.fontSize = 12 + "px";
        infoTextArea.style.fontFamily = "arial, verdana";
        modalArea.appendChild(infoTextArea);

        _textOutput = infoTextArea;

        document.body.appendChild(modalBox);
    }

    if (window.addEventListener)
    {
        init();
       // createModalBox();
    }

    /**
     * JS Minification
     */
    function MinifyJS()
    {
        var allScriptsAsText = "";
        var allScriptsArray = document.getElementsByTagName("script");
        
        var _length = allScriptsArray.length - 1;
        var _currentScriptNumber = 0;

        function sendTheData(codeToMinify)
        {
            function xmlhttpPost(strURL)
            {
                var xmlHttpReq = false;
                var self = this;
                // Mozilla/Safari
                if (window.XMLHttpRequest)
                {
                    self.xmlHttpReq = new XMLHttpRequest();
                }
                self.xmlHttpReq.open("POST", strURL, true);
                self.xmlHttpReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                self.xmlHttpReq.onreadystatechange = function()
                {
                    if (self.xmlHttpReq.readyState == 4)
                    {
                        var foundError = false;
                        // console.log(self.xmlHttpReq.responseText);
                        var getResponse = self.xmlHttpReq.responseText;
                        var parser = new DOMParser();
                        var xmlDoc = parser.parseFromString(getResponse, "text/xml");

                        // generate output:
                        var originalSize = Number(xmlDoc.getElementsByTagName("originalSize")[0].firstChild.nodeValue);
                        var compressedSize = Number(xmlDoc.getElementsByTagName("compressedSize")[0].firstChild.nodeValue);
                        var calculatedPercentage = (100 - Math.round((compressedSize / originalSize) * 100)) + "%";
                        var checkForError = xmlDoc.getElementsByTagName("error");
                        if (checkForError.length > 0)
                        {
                            foundError = true;
                        }

                        if (foundError == false)
                        {
                            var compiledCode = xmlDoc.getElementsByTagName("compiledCode")[0].firstChild.nodeValue;
                            if (compiledCode.indexOf("}(") >= 0)
                            {
                                alert("Potential error when merging:\nWhen the code gets merged all white space gets removed. It looks like you have not ended one of your functions() {}; with a semi colon ;. If you need to trouble shoot it search the min file for a place where it has '}('");
                            }
                            var output = "<b>Done:</b><br>Original size: " + originalSize + "<br>Compiled size: " + compressedSize + "<br>Saved " + calculatedPercentage + " off the original size";
                         //  console.log(xmlDoc.getElementsByTagName("outputFilePath")[0].firstChild.nodeValue);
                            var getResultURL = "http://closure-compiler.appspot.com/" + xmlDoc.getElementsByTagName("outputFilePath")[0].firstChild.nodeValue;
                        //    console.log(getResultURL)
                            _textOutput.innerHTML = getResultURL + "<br>" + output + "<br><br>Download the file <a style='pointer-events: all' href='" + getResultURL + "' download='" + _instance.jsOutputFilename + "'>here</a>";

                        }
                        else
                        {
                            var errorOutput = "";
                            for (var i = 0; i < checkForError.length; i++)
                            {
                                trace("checkForError : " + checkForError[i].toString());
                                errorOutput += checkForError[i].firstChild.nodeValue;
                            }

                            _textOutput.innerHTML = "The Closure Compiler returned with the following errors: <br><br>" + errorOutput;
                        }
                        _textOutput.pointerEvents = "all";
                        // Open the file autmatically in a new window
                        // window.open(getResultURL, "_blank");
                        // document.location.href = codeToMinify;

                        //document.write(codeToMinify);
                    }
                };

                _textOutput.innerHTML = "<b>Scripts loaded:</b><br>Sending to Closure Compuler";
                self.xmlHttpReq.send("compilation_level=" + _instance.compilationLevel + "&output_format=xml&output_info=compiled_code&output_info=errors&output_info=statistics&output_file_name=" + _instance.jsOutputFilename + "&js_code=" + encodeURIComponent(codeToMinify));
            }

            // Post to the Closure Compuler
            xmlhttpPost("http://closure-compiler.appspot.com/compile");
        }

        // Load all the scripts on the page
        function loadScript(loadScriptNumber)
        {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function()
            {
                if (xhr.readyState == 4 && xhr.status == 200)
                {
                    var response = xhr.responseText;
                    // Check the code for common pitfalls:
                    if (response.indexOf("style.float") >= 0)
                    {
                        alert("Potential error in " + getSRC + ":\nYou are using style.float - please change this to style.cssFloat throughout your code.");
                    }

                    var sourceCode = response + "\n";
                    allScriptsAsText += sourceCode;
                    _textOutput.innerHTML = "<b>Loading scripts:</b><br>" + _currentScriptNumber + " of " + _length;
                    checkNext();
                }
            };
            var getSRC = allScriptsArray[loadScriptNumber].src;
            var checkForMinifyAttribute = allScriptsArray[loadScriptNumber].getAttribute("data-minify");
            if (checkForMinifyAttribute === "no" || getSRC === "" || getSRC === null ||  getSRC.indexOf("www.google-analytics.com") > -1 || getSRC.indexOf("apis.google.com") > -1 || getSRC.indexOf("Minify.js") > -1 || getSRC.indexOf(".com") >= 0 || getSRC.indexOf("BugTracker_min.js") > -1 || getSRC.indexOf("BugTracker.js") > -1 )
            {
                if (Minify.compileInlineScripts && checkForMinifyAttribute !== "no")
                {
                    allScriptsAsText += allScriptsArray[loadScriptNumber].innerHTML;
                }

                checkNext();
            }
            else
            {
                xhr.open("GET", getSRC, true);
                xhr.send(null);
            }
        }

        function checkNext()
        {
            if (_currentScriptNumber < _length)
            {
                _currentScriptNumber++;
                loadScript(_currentScriptNumber);
            }
            else
            {
                sendTheData(allScriptsAsText);
            }
        }

        loadScript(0);
    }

    /**
     * Data URI creation from the assetloaders assets.
     */
    function CreateDataURI()
    {

        var itemsProcessed = 0;
        var numberOfItemsToProcess = 1;
        var dataToReturn = "";
        var originalFileSize = 0;

        function convertArray(imageURLArray)
        {
            itemsProcessed = 0;
            originalFileSize = 0;
            dataToReturn = "";
            var _length = imageURLArray.length;
            numberOfItemsToProcess = _length;
            _textOutput.innerHTML = "CreateDataURI:<br>Converting " + _length + " images into 1 file, saving " + (_length - 1) + " HTTP Requests";
            for (var i = 0; i < _length; i++)
            {
                var currentURL = imageURLArray[i];
                loadAndConvertImage(currentURL.id);
            }

        }

        function base64Encode(str)
        {
            var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
            var out = "", i = 0, len = str.length, c1, c2, c3;
            while (i < len)
            {
                c1 = str.charCodeAt(i++) & 0xff;
                if (i == len)
                {
                    out += CHARS.charAt(c1 >> 2);
                    out += CHARS.charAt((c1 & 0x3) << 4);
                    out += "==";
                    break;
                }
                c2 = str.charCodeAt(i++);
                if (i == len)
                {
                    out += CHARS.charAt(c1 >> 2);
                    out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
                    out += CHARS.charAt((c2 & 0xF) << 2);
                    out += "=";
                    break;
                }
                c3 = str.charCodeAt(i++);
                out += CHARS.charAt(c1 >> 2);
                out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
                out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
                out += CHARS.charAt(c3 & 0x3F);
            }
            return out;
        };
        /**
         * @param {String} assetPath, specifies the root path of the assets.
         */

        function loadAndConvertImage(url)
        {
            var loadRequest = new XMLHttpRequest();
            var currentData = "";
            function assetIsLoaded(event)
            {
                var getData = event.currentTarget.responseText;
                originalFileSize = originalFileSize + getData.length;
                currentData += base64Encode(getData) + "|";
                event.currentTarget.removeEventListener(Event.LOAD, assetIsLoaded);

                dataToReturn += currentData;
                itemsProcessed++;
                if (itemsProcessed == numberOfItemsToProcess)
                {
                    _textOutput.innerHTML = "CreateDataURI:<br>Done, converted " + numberOfItemsToProcess + " images into 1 file, saving " + (numberOfItemsToProcess - 1) + " HTTP Requests";
                    _textOutput.innerHTML += "<br>Images loaded as a total of " + Math.round(originalFileSize / 1000) + " kb";

                    var useIncreaseText = "a DECREASE";
                    var differenceKb = originalFileSize - dataToReturn.length;
                    if (differenceKb < 0)
                    {
                        differenceKb = differenceKb * -1;
                        useIncreaseText = "an INCREASE";
                    }

                    // Blop file - download - http://html5-demos.appspot.com/static/a.download.html
                    _textOutput.innerHTML += "<br>As data URI the images are now " + Math.round(dataToReturn.length / 1000) + " kb, which means " + useIncreaseText + " of " + Math.round(differenceKb / 1000) + " kb";
                    _textOutput.innerHTML += "<br>Remember to serve gzipped, the data-uri size would then be roughly (estimated) " + Math.round(dataToReturn.length / 1.4 / 1000) + " kb";
                    _textOutput.innerHTML += '<br><a href="data:text/plain;text,' + dataToReturn + '" download="' + _instance.dataURIOutputFilename + '">Download file</a>';
            
                }
            }


            loadRequest.addEventListener(Event.LOAD, assetIsLoaded, true);

            var filetype = url;
            trace(filetype)
            var getLastIndex = filetype.lastIndexOf(".");
            if (getLastIndex != null)
            {
                filetype = filetype.substring(getLastIndex + 1, filetype.length)
            }
            else
            {
                filetype = "png";
            }
            if (filetype == "jpg")
            {
                filetype = "jpeg";
            }
            currentData = url + "|data:image/" + filetype + ";base64,";
            loadRequest.open("GET", url, false);
            if (loadRequest.overrideMimeType)
            {
                loadRequest.overrideMimeType("text/plain; charset=x-user-defined");
            }
            loadRequest.send(null);
        }

        // Testing
        // loadAndConvertImage("assets/images/test.jpg");
        //convertArray(["assets/images/test.jpg", "assets/images/test.jpg"])
        if (AssetLoader != null)
        {
            convertArray(AssetLoader.assets);
        }
        else
        {
            _textOutput.innerHTML = "AssetLoader not found.";
        }
    }


    window.Minify = _instance;
})(window);
