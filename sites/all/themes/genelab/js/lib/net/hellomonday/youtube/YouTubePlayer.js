/*
 * Add a YouTube Player using the iframe property
 * Need to embed this in the script - <script src="http://www.youtube.com/iframe_api"></script>
 *
 *
 * Usage Example:
 *      <script src="http://www.youtube.com/iframe_api"></script>
 *      var newPlayer = YouTubePlayer("u1zgFlCw8Aw", 600, 400, {autoplay: 1});
 *      document.body.appendChild(newPlayer);
 *
 *      YouTubePlayer(videoID, width, height, playerVariables);
 *
 *
 *
 * // List of Player Variables see: https://developers.google.com/youtube/player_parameters?playerVersion=HTML5
 * Example: {html5: 0, cc_load_policy: 0, autoplay: 1}
 *
 * autohide: 1                  // Hides the video controls
 * autoplay: 1                  // starts the video
 * cc_load_policy: 1            // Show closed caption per default
 * color: red,white             // color of the progress bar
 * controls: 1,2,3
 * disablekb: 1                 // diables keyboard controls
 * enablejsapi: 1               // Enables javascript API
 * end: number                  // time when the video should end
 * html5: 1                     // Force YouTube to use the HTML5 Player (not yet an official parameter)
 * iv_load_policy: 1,2,3        // show video annotations
 * loop: 1                      // Video should loop
 * modestbranding: 1            // Dont show YouTube logo on the Player (lower right)
 * playlist: videoID,videoID    // set X number of VideoIDs to have the videos play in succession
 * rel: 0                       // Should the video show related videos after it is done playing (default is 1)
 * showinfo: 0                  // Should the video show video info before playing (default is 1)
 * start: number                // From where should the video start from?
 * theme: dark, light           // Choose from either a dark og light theme
 *
 *
 * MIGHT NOT WORK IN IE7!
 *
 */

function YouTubePlayer(videoID, width, height, playerVars)
{
    var tempDiv = document.createElement("div");
    tempDiv.id = "player";
    document.body.appendChild(tempDiv);

    if (playerVars == null)
    {
        playerVars =
        {
        };
    }

    // Playervars
    // var forceHTML5Player = 0; // 1 = true;
    // var showSubtitles = 0; // 1 = true
    //
    // var playerVars = {};
    // var playerVars = {html5: 1, cc_load_policy: 1};

    var player = new YT.Player('player',
    {
        height: height,
        width: width,
        videoId: videoID,
        playerVars: playerVars,
        events:
        {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event)
    {
        // event.target.playVideo();
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
    function onPlayerStateChange(event)
    {
        trace("YouTubePlayer onPlayerStateChange : " + event.data);
        // if (event.data == YT.PlayerState.PLAYING && !done)
        // {
        // setTimeout(stopVideo, 6000);
        // done = true;
        // }
    }

    function stopVideo()
    {
        player.stopVideo();
    }

    function pauseVideo()
    {
        player.stopVideo();
    }

    function seekTo(seconds, allowSeekAhead)
    {
        player.seekTo(seconds, allowSeekAhead);
    }


    tempDiv.stopVideo = stopVideo();
    tempDiv.startVideo = startVideo();
    tempDiv.pauseVideo = pauseVideo();
    tempDiv.seekTo = seekTo(seconds, allowSeekAhead);

    document.body.removeChild(tempDiv);
    return tempDiv;

}