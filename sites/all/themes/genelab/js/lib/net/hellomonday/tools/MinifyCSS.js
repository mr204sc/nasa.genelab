function MinifyCSS()
{
}

// Public variables
MinifyCSS.outputFileName = "minified.css";
// Private variables
MinifyCSS._storeAllScripts = [];
MinifyCSS._dialogueBG = null;
MinifyCSS._dialogue = null;
// Reference to the dialogue that opens up
MinifyCSS._dialogueText = null;
MinifyCSS._textarea;
// Reference to the textfield within the dialogue box

/**
 * Check for key down
 */
MinifyCSS.init = function()
{
    // Store all the "script" tags
    MinifyCSS._storeAllScripts = document.getElementsByTagName("link");

    // Check for key down
    var _currentKeys = [];
    var _keyIsDown = false;

    detectKeyUp = function(event)
    {
        _currentKeys = [];
        _keyIsDown = false;
    };
    detectKeyDown = function(event)
    {
        _keyIsDown = true;
        _currentKeys.push(event.keyCode);
        //console.log(event.keyCode);

        var matchCount = 0;
        for (var i = 0; i < _currentKeys.length; i++)
        {
            var currItem = _currentKeys[i];
            if (currItem == 17 || currItem == 78)
            {
                matchCount++;
            }
        }

        if (_currentKeys.length == 2 && matchCount == 2)
        {
            MinifyCSS.minifyAllScripts();
        }
        // N = 78
        // CTRL = 17
    };

    // Key detection
    window.addEventListener("keyup", detectKeyUp, false);
    window.addEventListener("keydown", detectKeyDown, false);
};

MinifyCSS.minifyAllScripts = function()
{

    var allScriptsAsText = "";
    var allScriptsArray = MinifyCSS._storeAllScripts;
    var _length = allScriptsArray.length - 1;
    var _currentScriptNumber = 0;

    // Show the dialogue
    MinifyCSS._dialogueBG = document.createElement("div");
    MinifyCSS._dialogueBG.style.position = "fixed";
    MinifyCSS._dialogueBG.style.width = window.innerWidth + "px";
    MinifyCSS._dialogueBG.style.height = window.innerHeight + "px";
    MinifyCSS._dialogueBG.style.backgroundColor = "#000000";
    MinifyCSS._dialogueBG.style.opacity = 0.3;
    MinifyCSS._dialogueBG.style.top = 0 + "px";
    MinifyCSS._dialogueBG.style.left = 0 + "px";
    MinifyCSS._dialogueBG.style.zIndex = 999999999998;
    MinifyCSS._dialogueBG.style.cursor = "pointer";
    MinifyCSS._dialogueBG.addEventListener("mousedown", removeDialogue, false);

    MinifyCSS._dialogue = document.createElement("div");
    MinifyCSS._dialogue.style.position = "fixed";
    MinifyCSS._dialogue.style.width = "200px";
    MinifyCSS._dialogue.style.height = "150px";
    MinifyCSS._dialogue.style.backgroundColor = "#ffffff";
    MinifyCSS._dialogue.style.top = window.innerHeight / 2 - 200 / 2 + "px";
    MinifyCSS._dialogue.style.left = window.innerWidth / 2 - 150 / 2 + "px";
    MinifyCSS._dialogue.style.borderRadius = "3px";
    MinifyCSS._dialogue.style.zIndex = 999999999999;

    MinifyCSS._dialogueText = document.createElement("div");
    MinifyCSS._dialogueText.style.position = "absolute";
    MinifyCSS._dialogueText.style.top = "10px";
    MinifyCSS._dialogueText.style.left = "10px";
    MinifyCSS._dialogueText.style.width = 200 - 20 + "px";
    MinifyCSS._dialogueText.style.height = 150 - 20 + "px";
    MinifyCSS._dialogueText.style.color = "#000000";
    MinifyCSS._dialogueText.style.fontFamily = "arial";
    MinifyCSS._dialogueText.style.fontSize = "10px";

    var _dialogueClose = document.createElement("div");
    _dialogueClose.style.position = "absolute";
    _dialogueClose.style.top = 150 - 40 + "px";
    _dialogueClose.style.left = "10px";
    _dialogueClose.style.width = 200 - 40 + "px";
    _dialogueClose.style.fontFamily = "arial";
    _dialogueClose.style.textAlign = "center";
    _dialogueClose.style.fontSize = "10px";
    _dialogueClose.style.color = "#ff0000";
    _dialogueClose.style.cursor = "pointer";
    _dialogueClose.innerHTML = "Close";
    _dialogueClose.addEventListener("mousedown", removeDialogue, false);

    MinifyCSS._dialogue.appendChild(MinifyCSS._dialogueText);
    MinifyCSS._dialogue.appendChild(_dialogueClose);
    document.body.appendChild(MinifyCSS._dialogueBG);
    document.body.appendChild(MinifyCSS._dialogue);

    MinifyCSS._dialogueText.innerHTML = "<b>Loading scripts</b>";

    function removeDialogue()
    {
        // Close the dialogue
        MinifyCSS._dialogueBG.removeEventListener("mousedown", removeDialogue, false);
        _dialogueClose.removeEventListener("mousedown", removeDialogue, false);
        document.body.removeChild(MinifyCSS._dialogueBG);
        document.body.removeChild(MinifyCSS._dialogue);
        if (MinifyCSS._textarea)
        {
            document.body.removeChild(MinifyCSS._textarea);
            MinifyCSS._textarea = null;
        }
        MinifyCSS._dialogueBG = null;
        MinifyCSS._dialogue = null;
        MinifyCSS._dialogueText = null;
    }

    // Load all the scripts on the page
    function loadScript(loadScriptNumber)
    {
    
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState == 4 && xhr.status == 200)
            {
                var response = xhr.responseText;

                var sourceCode = response + "\n";
                allScriptsAsText += sourceCode;
                MinifyCSS._dialogueText.innerHTML = "<b>Loading files:</b><br>" + _currentScriptNumber + " of " + _length;
                checkNext();
            }
        };
        var getSRC = allScriptsArray[loadScriptNumber].href;
        var checkForMinifyAttribute = allScriptsArray[loadScriptNumber].getAttribute("data-minify");
        if (checkForMinifyAttribute == "no" || getSRC.indexOf(".css") == -1)
        {
            checkNext();
        }
        else
        {
            xhr.open("GET", getSRC, true);
            xhr.send(null);
        }
    }

    function checkNext()
    {
        if (_currentScriptNumber < _length)
        {
            _currentScriptNumber++;
            loadScript(_currentScriptNumber);
        }
        else
        {
            MinifyCSS.minifyCode(allScriptsAsText);
        }
    }

    loadScript(0);
};

MinifyCSS.minifyCode = function(codeToMinify)
{
    
    MinifyCSS._textarea = document.createElement("textarea");
    MinifyCSS._textarea.style.width = "800px";
    MinifyCSS._textarea.style.height = "500px";
    MinifyCSS._textarea.style.top = "50%";
    MinifyCSS._textarea.style.marginTop = "-250px";
    MinifyCSS._textarea.style.left = "50%";
    MinifyCSS._textarea.style.marginLeft = "-400px";
    MinifyCSS._textarea.style.position = "absolute";
    MinifyCSS._textarea.style.zIndex = 9999999999;
    MinifyCSS._textarea.style["-webkit-user-select"] = "text";
    document.body.appendChild(MinifyCSS._textarea);
    
    MinifyCSS._textarea.innerHTML = codeToMinify;
    
};

if (window.addEventListener)
{
    window.addEventListener("load", MinifyCSS.init, false);
}