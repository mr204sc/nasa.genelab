/**
 * @fileoverview Provides Floodlight Analytics through Javascript
 *   Example:
 *       FloodLight.track("src=2507573;type=ads-m612;cat=mobil544");
 *
 * @dependencies none
 * @author Anders
 * @browsers: All
 */

var FloodLight =
{
};

FloodLight._iframe = null;
/**
 * Tracks a specific URL
 * @param {string} trackName Pass in the URL for the activity - for example: src=2507573;type=ads-m612;cat=mobil544
 */
FloodLight.track = function(trackItems)
{
    var axel = Math.random() + "";
    var a = axel * 10000000000000;

    if (FloodLight._iframe === null)
    {
        FloodLight._iframe = document.createElement("iframe");
        FloodLight._iframe.setAttribute("width", 1);
        FloodLight._iframe.height = 1;
        FloodLight._iframe.frameBorder = 0;
        FloodLight._iframe.style.visibility = "hidden";
        FloodLight._iframe.style.zIndex = -100000;
        FloodLight._iframe.style.display = "none";
        FloodLight._iframe.style.position = "absolute";
        FloodLight._iframe.style.backgroundColor = "#ff0000";
        //  FloodLight._iframe.style.width = "1px";
        document.getElementsByTagName("div")[0].appendChild(FloodLight._iframe);
    }

    FloodLight._iframe.src = "https://fls.doubleclick.net/activityi;" + trackItems + ";ord=" + a + "?";

    //document.getElementsByTagName("body")[0].appendChild('<iframe src="https://fls.doubleclick.net/activityi;' + trackItems + ';ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
};
