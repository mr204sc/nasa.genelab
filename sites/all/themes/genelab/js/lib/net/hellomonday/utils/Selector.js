/**
 *
 * @author Torben (torben@hellomonday.com) 
 * 
 */
(function Selector()
{
	var _instance = this;
	
	_instance.disableSelection = function(target)
	{
	    if (typeof target.style.MozUserSelect!="undefined")
	    { 
	        target.style.MozUserSelect = "-moz-none";
		}
	    
	    target.onselectstart=function(){return false}
	    target.onmousedown=function(){return false}
	}
	
	_instance.getHtml = function(data, name)
	{
		var rVal = data.querySelector('div[data-name="' + name + '"]').innerHTML;
		return rVal;
	}
	
	_instance.getChildren = function(data, name)
	{
		var rVal = data.querySelector('div[data-name="' + name + '"]').children;
		return rVal;
	}
	
	_instance.getData = function(data, name)
	{
		var rVal = data.querySelector('div[data-name="' + name + '"]');
		return rVal;
	}
	
	_instance.copyCSS = function(target, source)
	{
		target.style.cssText = document.defaultView.getComputedStyle(source, "").cssText;
	}
	
	window.Selector = _instance;
})();
