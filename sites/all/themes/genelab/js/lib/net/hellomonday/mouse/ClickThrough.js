var ClickThrough = {};

ClickThrough._items = [];
ClickThrough._initiated = false;

ClickThrough.addItem = function(item)
{
    trace("ClickThrough.addItem();");

    if (!ClickThrough._initiated)
    {
        ClickThrough.init();
        ClickThrough._initiated = true;
    }

    ClickThrough._items.push(
    {
        item: item
    })
}

ClickThrough.removeItem = function(item)
{
    trace("ClickThrough.addItem();");

    var index = ClickThrough.getIndexIndexOfItem(item);

    if (index != -1)
    {
        ClickThrough._items.splice(index, 1);
    }

    if (ClickThrough._items.length == 0)
    {
        ClickThrough._initiated = false;
        $(document).unbind("mousemove", ClickThrough.onMouseMove);
        $(document).unbind("click", ClickThrough.onMouseClick);
    }
}

ClickThrough.getIndexIndexOfItem = function(item)
{
    var i;
    var l = ClickThrough._items.length;
    var rId = -1;
    var itemObj;

    for ( i = 0; i < l; i += 1)
    {
        itemObj = ClickThrough._items[i];

        if (itemObj.item == item)
        {
            rId = i;
            break;
        }
    }

    return rId;
}

ClickThrough.onMouseClick = function(e)
{
    if (BackgroundAnimation._oldImageId >= 4)
    {
        var mouseX = e.pageX;
        var mouseY = e.pageY;

        var i;
        var l = ClickThrough._items.length;
        var item;
        var pos;
        var itemWidth;
        var itemHeight;
        var itemObj;

        for ( i = 0; i < l; i += 1)
        {
            itemObj = ClickThrough._items[i];
            if (itemObj.item)
            {
                item = itemObj.item;

                pos = $(item).offset();

                itemWidth = $(item).width();
                itemHeight = $(item).height();

                // click
                if (mouseX > pos.left && mouseX < pos.left + itemWidth && mouseY > pos.top && mouseY < pos.top + itemHeight)
                {
                    // mouse over
                    trace("click");
                    $(item).click();
                }
            }
        }
    }
}

ClickThrough.onMouseMove = function(e)
{
    if (BackgroundAnimation._oldImageId >= 4)
    {
        var mouseX = e.pageX;
        var mouseY = e.pageY;

        var i;
        var l = ClickThrough._items.length;
        var item;
        var pos;
        var itemWidth;
        var itemHeight;
        var itemObj;

        for ( i = 0; i < l; i += 1)
        {
            itemObj = ClickThrough._items[i];
            if (itemObj.item)
            {
                item = itemObj.item;

                pos = $(item).offset();

                itemWidth = $(item).width();
                itemHeight = $(item).height();

                // mouse over and out
                if (mouseX > pos.left && mouseX < pos.left + itemWidth && mouseY > pos.top && mouseY < pos.top + itemHeight)
                {
                    if (!itemObj.isOver)
                    {
                        itemObj.isOver = true;
                        trace("mouse over");
                        Assets.LAYER_TOP.style.cursor = "pointer";
                        $(item).mouseover();
                    }
                }
                else
                {
                    if (itemObj.isOver)
                    {
                        trace("mouse out");
                        itemObj.isOver = false;
                        Assets.LAYER_TOP.style.cursor = "default";
                        $(item).mouseout();
                    }
                }
            }
        }
    }
};

ClickThrough.init = function()
{
    trace("ClickThrough.init();");

    $(document).bind("mousemove", ClickThrough.onMouseMove);
    $(document).bind("click", ClickThrough.onMouseClick);
};

