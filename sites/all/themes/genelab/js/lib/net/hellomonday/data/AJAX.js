/**
 * @fileoverview Loads a file through AJAX
 * @author Anders S Jessen
 * @browsers: IE5+, FF3.6+, Chrome 11+, Safari 5+
 */
(function AJAX(window) {
    var _instance = {};

    /**
    * Loads a file through AJAX
    * @param {url} the URL to load the document from
    * @param {callbackFunction} function to initialize when the data is loaded
    * @param {method} load file using GET or POST
    */
    _instance.load = function(url, callbackFunction, method, params) {
        var _xmlhttp;

        if (method === null) {
            method = "GET";
        }
        
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            _xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            _xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
    
        var onReadyStateChange = function() {
            if (_xmlhttp.readyState == 4 && _xmlhttp.status == 200) {
                callbackFunction(_xmlhttp.responseText);
            } else {
                // callbackFunction();
            }
        }

        _xmlhttp.onreadystatechange = onReadyStateChange;
        _xmlhttp.open(method, url, true);

        if (params) {
            _xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            _xmlhttp.send(params);
        } else {
            _xmlhttp.send();    
        }

    };
    
    window.AJAX = _instance;
    
})(window)




