// Calc PennerEasings to CSS transitions: http://matthewlein.com/ceaser/
// Create custom easing types: http://cubic-bezier.com

function PennerEasings()
{
};

function Quad()
{
};
function Cubic()
{
};
function Quint()
{
};
function Sine()
{
};
function Expo()
{
};
function Back()
{
};
function Quart()
{
};
function Circ()
{
};
function Bounce()
{
};
function Elastic()
{
};
function Linear()
{
};
function Swing()
{
};

PennerEasings.initCSSTransitions = function()
{
    /*var easeBegin = 'cubic-bezier(';
     var	easeEnd = ')';

     Bounce.easeNone = easeBegin + '0.0, 0.35, .5, 1.3' + easeEnd;
     Linear.easeNone = 'linear',
     Swing.easeNone = 'ease-in-out',

     // Penner equation approximations from Matthew Lein's Ceaser: http://matthewlein.com/ceaser/
     Quad.easeIn =     easeBegin + '0.550, 0.085, 0.680, 0.530' + easeEnd;
     Cubic.easeIn =    easeBegin + '0.550, 0.055, 0.675, 0.190' + easeEnd;
     Quart.easeIn =    easeBegin + '0.895, 0.030, 0.685, 0.220' + easeEnd;
     Quint.easeIn =    easeBegin + '0.755, 0.050, 0.855, 0.060' + easeEnd;
     Sine.easeIn =     easeBegin + '0.470, 0.000, 0.745, 0.715' + easeEnd;
     Expo.easeIn =     easeBegin + '0.950, 0.050, 0.795, 0.035' + easeEnd;
     Circ.easeIn =     easeBegin + '0.600, 0.040, 0.980, 0.335' + easeEnd;
     Back.easeIn =     easeBegin + '0.600, -0.280, 0.735, 0.045' + easeEnd;
     Quad.easeOut =    easeBegin + '0.250, 0.460, 0.450, 0.940' + easeEnd;
     Cubic.easeOut =   easeBegin + '0.215, 0.610, 0.355, 1.000' + easeEnd;
     Quart.easeOut =   easeBegin + '0.165, 0.840, 0.440, 1.000' + easeEnd;
     Quint.easeOut =   easeBegin + '0.230, 1.000, 0.320, 1.000' + easeEnd;
     Sine.easeOut =    easeBegin + '0.390, 0.575, 0.565, 1.000' + easeEnd;
     Expo.easeOut =    easeBegin + '0.190, 1.000, 0.220, 1.000' + easeEnd;
     Circ.easeOut =    easeBegin + '0.075, 0.820, 0.165, 1.000' + easeEnd;
     Back.easeOut =    easeBegin + '0.175, 0.885, 0.320, 1.275' + easeEnd;
     Quad.easeInOut =  easeBegin + '0.455, 0.030, 0.515, 0.955' + easeEnd;
     Cubic.easeInOut = easeBegin + '0.645, 0.045, 0.355, 1.000' + easeEnd;
     Quart.easeInOut = easeBegin + '0.770, 0.000, 0.175, 1.000' + easeEnd;
     Quint.easeInOut = easeBegin + '0.860, 0.000, 0.070, 1.000' + easeEnd;
     Sine.easeInOut =  easeBegin + '0.445, 0.050, 0.550, 0.950' + easeEnd;
     Expo.easeInOut =  easeBegin + '1.000, 0.000, 0.000, 1.000' + easeEnd;
     Circ.easeInOut =  easeBegin + '0.785, 0.135, 0.150, 0.860' + easeEnd;
     Back.easeInOut =  easeBegin + '0.680, -0.550, 0.265, 1.550' + easeEnd;
     */
}
// Linear
Linear.easeNone = function(t, b, c, d)
{
    return c * t / d + b;
}
// Quad
Quad.easeIn = function(t, b, c, d)
{
    return c * (t /= d) * t + b;
}
Quad.easeOut = function(t, b, c, d)
{
    return -c * (t /= d) * (t - 2) + b;
}
Quad.easeInOut = function(t, b, c, d)
{
    if ((t /= d / 2) < 1)
        return c / 2 * t * t + b;
    return -c / 2 * ((--t) * (t - 2) - 1) + b;
}
// Cubic
Cubic.easeIn = function(t, b, c, d)
{
    return c * (t /= d) * t * t + b;
}
Cubic.easeOut = function(t, b, c, d)
{
    return c * (( t = t / d - 1) * t * t + 1) + b;
}
Cubic.easeInOut = function(t, b, c, d)
{
    if ((t /= d / 2) < 1)
        return c / 2 * t * t * t + b;
    return c / 2 * ((t -= 2) * t * t + 2) + b;
}
Cubic.easeOutIn = function(t, b, c, d)
{
    if (t < d / 2)
        return Cubic.easeOut(t * 2, b, c / 2, d);
    return Cubic.easeIn((t * 2) - d, b + c / 2, c / 2, d);
}
// Quart
Quart.easeIn = function(t, b, c, d)
{
    return c * (t /= d) * t * t * t + b;
}
Quart.easeOut = function(t, b, c, d)
{
    return -c * (( t = t / d - 1) * t * t * t - 1) + b;
}
Quart.easeInOut = function(t, b, c, d)
{
    if ((t /= d / 2) < 1)
        return c / 2 * t * t * t * t + b;
    return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
}
Quart.easeOutIn = function(t, b, c, d)
{
    if (t < d / 2)
        return Quart.easeOut(t * 2, b, c / 2, d);
    return Quart.easeIn((t * 2) - d, b + c / 2, c / 2, d);
}
// Circ
Circ.easeIn = function(t, b, c, d)
{
    return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
}
Circ.easeOut = function(t, b, c, d)
{
    return c * Math.sqrt(1 - ( t = t / d - 1) * t) + b;
}
Circ.easeInOut = function(t, b, c, d)
{
    if ((t /= d / 2) < 1)
        return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
    return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
}
Circ.easeOutIn = function(t, b, c, d)
{
    if (t < d / 2)
        return Circ.easeOut(t * 2, b, c / 2, d);
    return Circ.easeIn((t * 2) - d, b + c / 2, c / 2, d);
}
// Bounce
Bounce.easeIn = function(t, b, c, d)
{
    return c - PennerEasing.easeOutBounce(d - t, 0, c, d) + b;
}
Bounce.easeOut = function(t, b, c, d)
{
    if ((t /= d) < (1 / 2.75))
    {
        return c * (7.5625 * t * t) + b;
    }
    else
    if (t < (2 / 2.75))
    {
        return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
    }
    else
    if (t < (2.5 / 2.75))
    {
        return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
    }
    else
    {
        return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
    }
}
Bounce.easeInOut = function(t, b, c, d)
{
    if (t < d / 2)
        return PennerEasing.easeInBounce(t * 2, 0, c, d) * .5 + b;
    else
        return PennerEasing.easeOutBounce(t * 2 - d, 0, c, d) * .5 + c * .5 + b;
}
Bounce.easeOutIn = function(t, b, c, d)
{
    if (t < d / 2)
        return PennerEasing.easeOutBounce(t * 2, b, c / 2, d);
    return PennerEasing.easeInBounce((t * 2) - d, b + c / 2, c / 2, d);
}
// Elastic
Elastic.easeIn = function(t, b, c, d, a, p)
{
    var s;
    if (t == 0)
        return b;
    if ((t /= d) == 1)
        return b + c;
    if (!p)
        p = d * .3;
    if (!a || a < Math.abs(c))
    {
        a = c;
        s = p / 4;
    }
    else
        s = p / (2 * Math.PI) * Math.asin(c / a);
    return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
}
Elastic.easeOut = function(t, b, c, d, a, p)
{
    var s;
    if (t == 0)
        return b;
    if ((t /= d) == 1)
        return b + c;
    if (!p)
        p = d * .3;
    if (!a || a < Math.abs(c))
    {
        a = c;
        s = p / 4;
    }
    else
        s = p / (2 * Math.PI) * Math.asin(c / a);
    return (a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b);
}
Elastic.easeInOut = function(t, b, c, d, a, p)
{
    var s;
    if (t == 0)
        return b;
    if ((t /= d / 2) == 2)
        return b + c;
    if (!p)
        p = d * (.3 * 1.5);
    if (!a || a < Math.abs(c))
    {
        a = c;
        s = p / 4;
    }
    else
        s = p / (2 * Math.PI) * Math.asin(c / a);
    if (t < 1)
        return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
}
Elastic.easeOutIn = function(t, b, c, d, a, p)
{
    if (t < d / 2)
        return PennerEasing.easeOutElastic(t * 2, b, c / 2, d, a, p);
    return PennerEasing.easeInElastic((t * 2) - d, b + c / 2, c / 2, d, a, p);
}

