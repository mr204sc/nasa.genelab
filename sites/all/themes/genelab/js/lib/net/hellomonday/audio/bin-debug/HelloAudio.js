/**
 *  Wrapper for web audio api with fallback
 *  Does not support IE8 at the moment due to the setter/getter implementation (can easily be fixed)
 * 
 *  Engine initialization:
 *  
 *  HelloAudio.sm2Path = "js/lib/net/hellomonday/audio/fallback/soundManager2/"; <== Setup SoundManager2 path
 *  HelloAudio.init(audioEngineReady); <== run init with a ready callback 
 * 
 *  The engine supports different types of sounds
 *  - one shot (multiple instances of the same sound can be triggered at once, but they can't loop)
 *    HelloAudio.play("assets/sounds/test.mp3");
 * 
 *  - multi shot (only one instane of the sound can be played at once, and it is loopable)
 *    var loopingSound = HelloAudio.createAudioObject("assets/sounds/test.mp3", true);
 *    HelloAudio.play(loopingSound);
 *     
 *
 *  @author Torben (torben@hellomonday.com)
 * 
 * 
 *  TODO:
 *  - Implement Channels(will be an audio object) where you can route multiple sounds through (web audio only)
 *  
 *  02-09-2013:
 *  MediaElementAudioSourceNode should be used for playback of larger files.
 *  But is currently only supported in Chrome. 
 *  (This way the sound file will be streaming through an audio tag, hooked up to a web audio context)
 * 
 * 
 *  04-09-2013:
 *  Fixed compatibility issue with Safari. Remember to check the specs ..
 *  "createGain" is "createGainNode" in Safari
 */
(function HelloAudio(window)
{
    var DEBUG_MODE                  = false;
    
	var _instance					= {};
    
    var _masterDry;
    var _masterWet;
    var _masterVolume;
    
    var _mainBus;
    
    var _loader;
    
    var _audioDict                  = {};
    var _audioObjects               = [];
    
    var _effects                    = [];
    var _curEffectId                = 0;
    
    var _readyCallback;
    
    _instance.sm2Path               = null;
    
	_instance.init = function(readyCallback)
	{
	    if(!readyCallback)
        {
            throw new Error("HelloAudio init: ready callback function missing!");
        }
	    
	    _readyCallback = readyCallback;
	    
        try
        {
            // Fix up for prefixing
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            createMasterChannels();
            
            _instance.fallback = false;
            
            _readyCallback();
        }
        catch(e) 
        {
            if(DEBUG_MODE)
            {
                trace('Web Audio API is not supported in this browser', "HelloAudio");
                trace("Falling back to SM2", "HelloAudio");
            }
            
            _instance.fallback = true;
            
            // Load SoundManager2
            loadScript(_instance.sm2Path + "script/soundmanager2-nodebug-jsmin.js", initSM2Fallback); 
        }
	}
	
	function createMasterChannels()
	{
	    // Create main bus
        _mainBus = new AudioContext();
        
        _mainBus.createGain = _mainBus.createGain || _mainBus.createGainNode; // Fix for Safari
        
         // Create master volume
        _masterVolume = _mainBus.createGain(); 
        _masterVolume.connect(_mainBus.destination);
            
        // Create dry/wet signal route
        _masterWet = _mainBus.createGain(); 
        _masterWet.connect(_masterVolume);
        _masterDry = _mainBus.createGain(); 
        _masterDry.connect(_masterVolume);
            
        // Init audio loader
        AudioLoader.init(_mainBus); 
	}
	
	function initSM2Fallback()
	{
	    if(!_instance.sm2Path)
	    {
	        throw new Error("HelloAudio initSM2Fallback: sm2Path needs to be set!");
	        return;
	    }
	    
        soundManager.setup({url: _instance.sm2Path + "/swf", onready: SM2Ready, ontimeout: SM2setupError, flashVersion: 9, preferFlash: true});
        soundManager.beginDelayedInit();
	}
	
	function SM2Ready()
	{
	    // Add fallback AudioObject brigde
	    AudioObject = SMAudioObjectBrigde;
	    
	    // Add fallback brigde versions of effects - currently only pan is supported
	    AudioEffect_2dPanner = SMAudioEffect_2dPannerBrigde;
	    
	    // Add fallback AudioLoader brigde 
	    new SMAudioLoaderBrigde();
	    _readyCallback();
	}
	
	function SM2setupError()
	{
	    //trace('HelloAudio: SoundManager2 setup error');
	}
	
	_instance.createAudioObject = function(path, loop, multishot, loopStart, loopEnd)
	{
	    var audioObject;
        var params = {};
        var multiShotEnabled = multishot;
	    
        if(loop && multishot != true) // multishot sounds should not be able to loop
        {
            params.loop = true;
            params.loopStart = loopStart;
            params.loopEnd = loopEnd;
            multiShotEnabled = false;
        }
        
        if(_instance.fallback)
        {
            audioObject = new AudioObject(path, params);
        }
        else
        {
            var buffer = AudioLoader.getAudioBuffer(path);
            var source = _mainBus.createBufferSource();
            
            audioObject = new AudioObject(path, source, buffer, _mainBus, _masterDry, _masterWet, params);
            audioObject.volume = 1;
        }
        
        // Allow the sound to have multiple instances
        audioObject.multiShot = multiShotEnabled || false;
        
        return audioObject
	}
	
	_instance.play = function(audioObject, volume, time)
	{
	    // Fallback
	    if(_instance.fallback)
        {
            var isSMObject = Object.prototype.toString.call(audioObject).indexOf("String") > -1 ? false : true;
            var multishot = true;
            var looping;
            
            if(!isSMObject)
            {
                audioObject = soundManager.getSoundById(audioObject);
                looping = false;
            }
            else
            {
                multishot = audioObject.multiShot || false;
                looping = audioObject.params.loop;
            }
            
            var params = {};
            
            if(volume)
            {
                volume *= 100; 
            }
            
            params.volume = volume || 100;
            params.loops = looping ? 99999999 : 1;
            params.multiShot = multishot;
            
            audioObject.playing = true;
            audioObject.play(params);
        }
        else
        {
            var source;
            var path = audioObject.path || audioObject;
            var isAudioObject = getType(audioObject) == "AudioObject" ? true : false;
            
            // Check if audioObject is already playing
            if(isAudioObject)
            {
                // Check if multiShot is allowed
                if(!audioObject.multiShot)
                {
                    if(audioObject.paused)
                    {
                        _instance.resume(audioObject);
                        return;
                    }
                    else if(audioObject.playing)
                    {
                        return;
                    }
                }
                
                source = _mainBus.createBufferSource();
                source.buffer = audioObject.buffer;
                
                audioObject.setSource(source);
                audioObject.playing = true;
                
                trace(audioObject.volume, "audioObject.volume");
                
                audioObject.volume = volume || audioObject.volume;
                
                var params = audioObject.params;
                //var bufferTimeLength = (buffer.length / 44.1) / 1000; // Length in seconds (samplerate needs to be 44.1)
                            
                audioObject.startTime = _mainBus.currentTime;
                
                // Looping
                if(params.loop)
                {
                    source.loop = true;
                    source.loopStart = params.loopStart;
                    source.loopEnd = params.loopEnd;
                }
            }// if its not an audioObject, then create new buffer and connect directly to master dry
            else
            {
                source = _mainBus.createBufferSource();
                
                var buffer = AudioLoader.getAudioBuffer(audioObject);
                source.buffer = buffer;
                
                // one-shot audio goes directly to master dry
                source.connect(_masterDry);
            }
            
            var startTime = time || 0;
            var initialVolume = volume || 1;
            
            if(!source.start)
            {
                source.start = source.noteOn;
            }
            
            if(DEBUG_MODE)
            {
                trace(path, "HelloAudio play");
            }
            
            source.start(0, startTime);
        }
	}
	
	_instance.stop = function(audioObject)
	{
	    audioObject.playing = false;
        audioObject.paused = false;
	    
	    if(_instance.fallback)
        {
            audioObject.stop();
        }
        else
        {
            var source = audioObject.source;
        
            audioObject.startOffset = 0;
            
            if(!source.stop)
            {
                source.stop = source.noteOff;
            }
            
            if(DEBUG_MODE)
            {
                trace(audioObject.path, "HelloAudio stop");
            }
            source.stop(0);
        }
	}
	
	_instance.pause = function(audioObject)
    {
        audioObject.playing = false;
        audioObject.paused = true;
        
        if(_instance.fallback)
        {
            audioObject.pause();
        }
        else
        {
            var source;
            
            audioObject.startOffset += (_mainBus.currentTime - audioObject.startTime);
            
            source = audioObject.source;
            
            if(!source.stop)
            {
                source.stop = source.noteOff;
            }
            
            if(DEBUG_MODE)
            {
                trace(audioObject.path, "HelloAudio pause");
            }
            source.stop(0);
        }
    }
	
	_instance.resume = function(audioObject)
    {
        if(_instance.fallback)
        {
            audioObject.resume();
        }
        else
        {
            if(audioObject.paused)
            {
                var source;
                var resumeAt = audioObject.startOffset % audioObject.buffer.duration;
                
                if(DEBUG_MODE)
                {
                    trace(audioObject.path, "HelloAudio resume");
                }
                
                audioObject.paused = false;
                _instance.play(audioObject, resumeAt)
            }
        }
    }
    
    _instance.addEffect = function(effect)
    {
        var id = _curEffectId;
        _effects.push({effect: effect, id: id});
        _curEffectId++;
        
        return id;
    }
    
    _instance.removeEffect = function(id)
    {
        var i = 0;
		var l = _effects.length;
		
		for(i; i < l; i++)
		{
		    if(_effects[i].id == id)
		    {
		        _effects.splice(i, 1);
		        break;
		    }		   
		}
    }
	
	// Utility methods
	function getType(audioObject)
	{
	    var type = Object.prototype.toString.call(audioObject).indexOf("String") > -1 ? "Path" : "AudioObject";
	    return type;
	}
	
	function loadScript(url, onLoadCallback) 
    {
        function loadScriptHandler()
        {
            var readyState = this.readyState;
            if(readyState == 'loaded' || readyState == 'complete')
            {
                this.onreadystatechange = null;
                this.onload = null;
                if(onLoadCallback)
                {
                    onLoadCallback();
                }
            }
        }
    
        function scriptOnload()
        {
            this.onreadystatechange = null;
            this.onload = null;
            window.setTimeout(onLoadCallback, 20);
        }
    
        var script = document.createElement('script');
        script.type = 'text/javascript';
        if(onLoadCallback)
        {
            script.onreadystatechange = loadScriptHandler;
            script.onload = scriptOnload;
        }
        script.src = url;
        document.getElementsByTagName('head')[0].appendChild(script);
    }
	
	// Setter / getters
	Object.defineProperty(_instance, "mainBus", {get: getMainBus});
    function getMainBus()
    {
        return _mainBus;
    }
    
    Object.defineProperty(_instance, "masterVolume", {get: getMasterVolume, set: setMasterVolume});
    function getMasterVolume()
    {
        return _masterVolume.gain.value;
    }
    
    function setMasterVolume(value)
    {
        if(value > 1)
        {
            value = 1;
        }
        else if(value < 0)
        {
            value = 0;
        }
        
        _masterVolume.gain.value = value;
    }
    
    Object.defineProperty(_instance, "dryVolume", {get: getDryVolume, set: setDryVolume});
    function getDryVolume()
    {
        return _masterVolume.gain.value;
    }
    
    function setDryVolume(value)
    {
        if(value > 1)
        {
            value = 1;
        }
        else if(value < 0)
        {
            value = 0;
        }
        
        _masterVolume.gain.value = value;
    }
    
    Object.defineProperty(_instance, "wetVolume", {get: getWetVolume, set: setWetVolume});
    function getWetVolume()
    {
        return _masterVolume.gain.value;
    }
    
    function setWetVolume(value)
    {
        if(value > 1)
        {
            value = 1;
        }
        else if(value < 0)
        {
            value = 0;
        }
        
        _masterVolume.gain.value = value;
    }
    
	window.HelloAudio = _instance;
})(window);
