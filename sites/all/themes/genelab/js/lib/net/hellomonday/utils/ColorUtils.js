/**
 * 
 * @author Torben
 * 
 */
(function()
{
	var _instance = {};
	
	/**
	 * @param {String} color - Hex color
	 * @param {Int} percent - Percentage to shade the color (use a negative value to darken the hex);
	 * 
	 * TODO: I might be able to improve the algorithm futher, but it seems fast enough (Torben) 
	 */
	_instance.shadeColor = function(color, percent)
	{   
    	var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, B = (num >> 8 & 0x00FF) + amt, G = (num & 0x0000FF) + amt;
    	return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
	}
	
	_instance.getRandomHex = function(addHex)
	{
	    if(addHex === null)
	    {
	        addHex = "#";
	    }
	    else if(addHex === true)
	    {
	        addHex = "#";
	    }
	    else
	    {
	        addHex = "";
	    }
	    
	    return addHex + Math.floor(Math.random()*16777215).toString(16);
	}
	
	window.ColorUtils = _instance;
})();
