/**
 * @param {Array} - Array containing the filename of the images used in the animation
 * @param {Array} - Array containing the motion data used to animate the images
 * @param {int} - The framerate of the animation
 * @param {String} - Path to the folder containing the images used in the animation
 * @param {Boolean} - Defines if the animation should autoplay or not
 * @param {Function} - Optional callback function. Will fire when the images are done loading
 * @param {Boolean} - Optional. Specifies if IE png fix should be applied (does not work with jquery objects at the moment)
 * 
 * Dependencies:
 * 
 * FixIE.js
 * SmartObject.js
 * 
 * TODO:
 * 
 * Implement events instead of Callbacks
 * Create FrameData object for better data handling and incapsulation
 * Provide better looping capabilities
 */
function FrameByFrameAnim(imageArray, animArray, frameRate, imageFolder, autoplay, callback, addPngFix) // Change the params to FrameData object
{
    var instance            = this;
    
    var element             = new SmartObject();
    
    var _autoPlay           = autoplay;
    
    var _imagesLoaded       = 0;
    var _numberOfImages     = imageArray.length;
    var _imagesArray        = new Array();
    var _imageArray         = imageArray;
    
    var _totalFrames        = animArray[0].length;
    
    var _frameRate          = 1000 / frameRate;
    var _isPlaying          = false;
    var _allImagesLoaded    = false;
    var _isStopped          = false;
    
    var _frameCount         = 1;
    
    var _globalTimeout;
    
    instance.load = function() 
    {
        var _length = _imageArray.length;
        for (var i = 0; i < _length; i++)
        {
            var currentImageURL = _imageArray[i];
            var newSmartDiv = new SmartObject();
            var createImage = new Image();
            
            function imageLoaded(event)
            {
                // Remove the event listeners again
                event.currentTarget.removeEventListener(Event.LOAD, imageLoaded);
                event.currentTarget.removeEventListener(Event.LOAD, imageFailedToLoad);
                _imagesLoaded++;
                if (_imagesLoaded == _numberOfImages)
                {
                    // All Images loaded
                    if(callback)
                    {
                        callback();
                    }
                    
                    _allImagesLoaded = true;
                    instance.init();
                }
            }
            
            function imageFailedToLoad(event)
            {
                // Failed to load image
                trace("FrameByFrameAnim - Image Failed to Load: " + event.currentTarget.src);
            }
            
            createImage.addEventListener(Event.LOAD, imageLoaded, false);
            createImage.addEventListener(Event.ERROR, imageFailedToLoad, false);
            createImage.src = imageFolder + currentImageURL;
            newSmartDiv.appendChild(createImage);
            _imagesArray.push(newSmartDiv);
            
            if(addPngFix)
            {
                createImage.addEventListener(Event.ONLOAD, FixIE.imageCheckForPNGFix);
            }
        }
    }
    
    instance.init = function()
    {
        // All items loaded - add them to stage et their positions and start playing the animations
        var _length = _imagesArray.length;
        for (var i = 0; i < _length; i++)
        {
            var currImage = _imagesArray[i];
            element.appendChild(currImage);
        }
        
        instance.setFrame(0);
        
        if(_autoPlay)
        {
            if(_autoPlay === true)
            {
                instance.play();
            }
        }
    }
    
    /*
     * Goes to a specific frame 
     */
    instance.setFrame = function(frameNumber)
    {
        var i = 0;
        var l = _imagesArray.length;
        for (i; i < l; i++)
        {
            var currImage = _imagesArray[i];
            var currArray = animArray[i];
            var currObject = currArray[frameNumber];
            if (frameNumber == 0)
            {
                currImage.registrationPointPercentX = currObject.rx;
                currImage.registrationPointPercentY = currObject.ry;
            }
            if (currObject.x != null)
            {
                currImage.x = currObject.x;
            }
            if (currObject.y != null)
            {
                currImage.y = currObject.y;
            }
            if (currObject.sx != null)
            {
                currImage.scaleX = currObject.sx;
            }
            if (currObject.sy != null)
            {
                currImage.scaleY = currObject.sy;
            }
            if (currObject.r != null)
            {
                currImage.rotation = currObject.r;
            }
            if (currObject.o != null)
            {
                currImage.opacity = currObject.o;
            }
        }
        _frameCount = frameNumber;
    }
    
    instance.play = function(callback) 
    {
        clearTimeout(_globalTimeout);
        
        function playItem()
        {
            instance.setFrame(_frameCount);
            _frameCount++;
            if (_frameCount < _totalFrames - 1 && _isStopped == false)
            {
                _globalTimeout = setTimeout(playItem, _frameRate);
            }
            else
            {
                if(callback)
                {
                    callback();
                }
                
                _isPlaying = false;
            }
            
        }
        
        _isStopped = false;
        _isPlaying = true;

        playItem();
    }

    instance.animateTo = function(frame, callback)
    {
        trace("animateTo");
        clearTimeout(_globalTimeout);
        
        function playItem()
        {
            instance.setFrame(_frameCount);
            
            if(_direction == 'incr')
            {
                _frameCount++;
                if (_frameCount < frame && _isStopped == false)
                {
                    _globalTimeout = setTimeout(playItem, _frameRate);
                }
                else
                {
                    _isPlaying = false;
                    
                    if(callback)
                    {
                        callback();
                        callback = null;
                    }
                }
            }
            else
            {
                _frameCount--;
                if (_frameCount > frame && _isStopped == false)
                {
                    _globalTimeout = setTimeout(playItem, _frameRate);
                }
                else
                {
                    _isPlaying = false;
                    
                    if(_frameCount < 0)
                    {
                        _frameCount = 0;
                    }
                    
                    if(callback)
                    {
                        callback();
                        callback = null;
                    }
                }
            }
        }
        
        // Get animate direction
        var _direction = 'incr';
        if(frame <= _frameCount)
        {
            _direction = 'decr';
        }
        else if (frame == _frameCount) {
            // Do nothing since it is already at that frame
        }
        else {
            _isStopped = false;
            _isPlaying = true;
            
            playItem();
        }
        
    }

    instance.stop = function() 
    {
        clearTimeout(_globalTimeout);
        
        _isStopped = true;
    }
    
    instance.gotoAndStop = function(frame)
    {
        clearTimeout(_globalTimeout);
        
        _isStopped = true;
        _frameCount = frame;
        instance.setFrame(_frameCount);
    }
    
    instance.gotoAndPlay = function(frame, callback)
    {
        _isStopped = true;
        _frameCount = frame;
        instance.setFrame(_frameCount);
        
        instance.play(callback);
    }
    
    instance.getTotalFrames = function()
    {
        return _totalFrames;
    }
    
    instance.load();
    
    // Expose public methods
    element.play = instance.play;
    element.stop = instance.stop;
    element.gotoAndStop = instance.gotoAndStop;
    element.gotoAndPlay = instance.gotoAndPlay;
    element.animateTo = instance.animateTo;
    element.gotoFrame = instance.setFrame;
    element.getTotalFrames = instance.getTotalFrames;
    
    return element;
};

