/**
 * 
 * @author Torben
 * 
 */
(function(window)
{
    var _fontTypes;
    
    var _instance                   = {};
    var _initialized                = false;
    
    _instance.init = function(fontTypes)
    {
        _initialized = true;
        _fontTypes = fontTypes;
    }
    
    /**
     * @param {String} font type
     * @param {int} fontSize
     * @param {Boolean} selectable
     */
    _instance.getTextField = function(fontType, fontSize, selectable)
    {
        if(!_initialized)
        {
            throw new Error("TextLib: You need to initialized TextLib before using getTextField()");
        }
        
        var tf = document.createElement("div");
        
        if(selectable == null)
        {
            selectable = true;
        }
        
        if(!selectable)
        {
            _instance.disableSelection(tf);
        }
        
        tf.style.position = "absolute";
        tf.style.fontSize = fontSize + "px";

        if(_fontTypes[fontType] == null)
        {
            throw new Error("TextLib: fontType => " + fontType + " not defined");
        }

        tf.style.fontFamily = _fontTypes[fontType];
        
        return tf;
    }
    
    _instance.disableSelection = function(target)
    {
        target.onselectstart = function () { return false; } // ie
        target.onmousedown = function () { return false; } // webkit, mozilla
        
        target.style.cursor = "default";
    }
    
    _instance.getTextWidth = function(target)
    {
        var width = 0;
        
        if(target.parentNode == null)
        {
            document.body.appendChild(target);
            width = target.offsetWidth;
            document.body.removeChild(target);
        }
        else if(target.offsetWidth == 0)
        {
            var currentParentNode = target.parentNode;
            document.body.appendChild(target);
            width = target.offsetWidth;
            currentParentNode.appendChild(target);
        }
        else
        {
            width = target.offsetWidth;
        }
        
        return width;
    }

    _instance.getFontTypes = function(fontType)
    {
        return _fontTypes[fontType];
    }
    
    window.TextLib = _instance;
})(window);

