function AudioObject(path, source, buffer, mainBus, masterDry, masterWet, params)
{
	var _instance					= {};
	var _signalChain;
	
	var _gainNode;
	//var _panNode; Removed the pan node as it is better to insert it if needed (because of the extra overhead in the current panner implementation)
	
	var _outputs;
	
	function init()
	{
	    _instance.playing = false;
	    _instance.paused = false;
	    
	    _gainNode = mainBus.createGain();
	    //_panNode = mainBus.createPanner();
	    
	    //_gainNode.connect(_panNode);
	    
	    //_signalChain = new AudioSignalChain(_panNode, masterDry, masterWet);
	    _signalChain = new AudioSignalChain(_gainNode, masterDry, masterWet);
	    
	    _instance.path = path;
	    _instance.source = source;
	    _instance.buffer = buffer;
	    _instance.params = params;
	    
	    // Used to pause/resume playback
	    _instance.startTime = 0;
	    _instance.startOffset = 0;
	    _instance.paused = false;
	    _instance.pausePosition = 0;
	    
	    _instance.setSource(source);  
	}
	
	_instance.addEffect = function(effect)
	{
	    if(effect.outputType == "dry")
	    {
	        _signalChain.addInsert(effect);
	    }
	    else
	    {
	        _signalChain.addSend(effect);
	    }
	}
	
	_instance.removeEffect = function(effect)
	{
	    if(effect.outputType == "dry")
        {
            _signalChain.removeInsert(effect);
        }
        else
        {
            _signalChain.removeSend(effect);
        }
	}
	
	_instance.setSource = function(source)
    {
        _instance.source = source;
        source.connect(_gainNode);
        
        _signalChain.update();
    }
	
	Object.defineProperty(_instance, "volume", {get: getVolume, set: setVolume});
	function getVolume()
	{
	    return _gainNode.gain.value;
	}
	
	function setVolume(value)
	{
	    _gainNode.gain.value = value
	}
	
	init();

	return _instance;
}