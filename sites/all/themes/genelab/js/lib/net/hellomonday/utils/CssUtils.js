/**
 * @fileoverview A class to help with working with CSS using javascript.
 * @author Hello Monday (mike@hellomonday.com)
 */

/**
 * @constructor
 */
var CssUtils =
{
};

/**
 * Gets the browser's supported css property from an array.
 * @param {Array <String>} proparray The array of properties to test support for.
 */
CssUtils.getSupportedProperty = function(proparray)
{
    var root = document.documentElement;

    for (var i = 0; i < proparray.length; i++)
    {
        if ( typeof root.style[proparray[i]] == 'string')
        {
            return proparray[i];
        }
    }
}
/**
 * Gets the browser's supported box shadow css property from an array.
 * @return {String} The supported box shadow css property name.
 */
CssUtils.getSupportedBoxShadowProperty = function()
{
    var boxShadowProperty = CssUtils.getSupportedProperty(['boxShadow', 'MozBoxShadow', 'WebkitBoxShadow']);

    return boxShadowProperty;
}
/**
 * Sets the opacity of an element.
 *
 * @param {element} element An HTMLElement.
 * @param {Number} opacity A number between 1 and 100.
 */
CssUtils.setOpacity = function(element, opacity)
{
    element.style.opacity = (opacity / 100);
    element.style.MozOpacity = (opacity / 100);
    element.style.KhtmlOpacity = (opacity / 100);
    element.style.filter = 'alpha(opacity=' + opacity + ')';
}
/**
 * Gets the opacity of an element.
 *
 * @param {Object} element A jQuery DOM object.
 */
CssUtils.getOpacity = function(element)
{
    var opacity = 1;
    element.parents().andSelf().each(function()
    {
        opacity *= $(this).css('opacity') || 1
    });
    return opacity;
}

CssUtils.getCSSRule = function(ruleName, deleteFlag)
{
    ruleName = ruleName.toLowerCase();
    if (document.styleSheets)
    {
        for (var i = 0; i < document.styleSheets.length; i++)
        {
            var styleSheet = document.styleSheets[i];
            var ii = 0;
            var cssRule = false;
            do
            {
                if (styleSheet.cssRules)
                {
                    cssRule = styleSheet.cssRules[ii];
                }
                else if (styleSheet.rules)
                {
                    cssRule = styleSheet.rules[ii];
                }
                if (cssRule)
                {
                    if (cssRule.selectorText.toLowerCase() == ruleName)
                    {
                        if (deleteFlag == 'delete')
                        {
                            if (styleSheet.cssRules)
                            {
                                styleSheet.deleteRule(ii);
                            }
                            else
                            {
                                styleSheet.removeRule(ii);
                            }
                            return true;
                        }
                        else
                        {
                            return cssRule;
                        }
                    }
                }
                ii++;
            }
            while (cssRule)
        }
    }
    return false;
}

CssUtils.killCSSRule = function(ruleName)
{
    // Delete a CSS rule
    return CssUtils.getCSSRule(ruleName, 'delete');
    // just call getCSSRule w/delete flag.
}// end killCSSRule

CssUtils.addCSSRule = function(ruleName)
{
    // Create a new css rule
    if (document.styleSheets)
    {
        // Can browser do styleSheets?
        if (!CssUtils.getCSSRule(ruleName))
        {
            // if rule doesn't exist...
            if (document.styleSheets[0].addRule)
            {
                // Browser is IE?
                document.styleSheets[0].addRule(ruleName, null, 0);
                // Yes, add IE style
            }
            else
            {
                // Browser is IE?
                document.styleSheets[0].insertRule(ruleName + ' { }', 0);
                // Yes, add Moz style.
            } // End browser check
        } // End already exist check.
    }// End browser ability check.
    return CssUtils.getCSSRule(ruleName);
    // return rule we just created.
}                                                        