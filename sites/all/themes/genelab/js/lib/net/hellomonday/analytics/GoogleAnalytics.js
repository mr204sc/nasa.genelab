/**
 * @fileoverview Provides helper functions to Google Analytics
 <<<<<<< HEAD
 *   Usage Examples:
 *       Track page view:
 *           GoogleAnalytics.trackPage("myIntroPage");
 *
 *       Track an Event:
 *           GoogleAnalytics.trackEvent("Videos", "Play", "Gone With the Wind");
 =======
 *   Usage Examples:
 *       Track a Page:
 *           GoogleAnalytics.trackPage("Intro")
 *
 *       Track an Event:
 *           GoogleAnalytics.trackEvent("Videos", "Play", "Gone With the Wind");
 >>>>>>> Framework optimizations - v2
 *           GoogleAnalytics.trackEvent("Videos", "Pause", "Gone With the Wind");
 *           GoogleAnalytics.trackEvent("Videos", "Stop", "Gone With the Wind");
 *
 *       Track Social Event:
 *           GoogleAnalytics.trackSocialEvent("Facebook", "Like");
 *
 * @dependencies That Google Analytics tracking code is imported in the "head" section
 * @author Anders
 * @browsers: All (however only tested in: IE7+, FF3.6+, Chrome 11+, Safari 5+)
 *
 * Based on:
 * https://developers.google.com/analytics/devguides/collection/upgrade/reference/gajs-analyticsjs
 * https://developers.google.com/analytics/devguides/collection/analyticsjs/events
 */

var GoogleAnalytics = {};
/**
 * Tracks either a custom page - or if you do not pass in any parameters in it tracks the current page (including the hash)
 * @param {string} opt_trackPageName Optional: Pass in a specific page name - if not it tracks the current page you are on (including the hash tag)
 *
 * https://developers.google.com/analytics/devguides/collection/analyticsjs/pages
 */
GoogleAnalytics.trackPage = function(opt_trackPageName) {
	if (opt_trackPageName !== null) {
		ga('send', 'pageview', opt_trackPageName);
	} else {
		ga('send', 'pageview');
	}
};

/**
 * Track an Event
 * @param {string} category The name you supply for the group of objects you want to track.
 * @param {string} action A string that is uniquely paired with each category, and commonly used to define the type of user interaction for the web object.
 * @param {string} opt_label An optional string to provide additional dimensions to the event data.
 * @param {string} opt_value An integer that you can use to provide numerical data about the user event.
 * @param {string} opt_noninteraction A boolean that when set to true, indicates that the event hit will not be used in bounce-rate calculation.
 *
 * https://developers.google.com/analytics/devguides/collection/analyticsjs/events
 */
GoogleAnalytics.trackEvent = function(category, action, opt_label, opt_value, opt_noninteraction) {
	if (opt_noninteraction === null) {
		opt_noninteraction = "0";
	}
	// _gaq.push(['_trackEvent', category, action, opt_label, opt_value, opt_noninteraction]);
	ga('send', 'event', category, action, label, opt_value, {
		'nonInteraction' : opt_noninteraction
	});
};

/**
 * Track a Social Event
 * @param {string} network Required. A string representing the social network being tracked (e.g. Facebook, Twitter, LinkedIn) The name you supply for the group of objects you want to track.
 * @param {string} socialAction Required. A string representing the social action being tracked (e.g. Like, Share, Tweet)
 * @param {string} target Required. A string representing the URL (or resource) which receives the action. For example, if a user clicks the Like button on a page on a site, the the opt_target might be set to the title of the page, or an ID used to identify the page in a content management system. In many cases, the page you Like is the same page you are on. So if this parameter is undefined, or omitted, the tracking code defaults to using document.location.href.
 *
 * https://developers.google.com/analytics/devguides/collection/analyticsjs/social-interactions
 */
GoogleAnalytics.trackSocialEvent = function(network, socialAction, target) {
	//_gaq.push(['_trackSocial', network, socialAction, opt_target, opt_pagePath]);
	ga('send', 'social', network, socialAction, target);
};
