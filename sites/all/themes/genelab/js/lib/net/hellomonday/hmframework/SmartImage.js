/**
 *   
 * @author Anders, Torben
 *
 **/

/**   
 * @param {Object} url - Image path
 */
function SmartImage(url)
{
    var _instance = new Image();
    var xhr;

    function init()
    {
        if(window.XMLHttpRequest)
        {
            loadImage(url);
        }
        else
        {
            _instance.src = url;
            trace("SmartImage (" + url + "): Your browser does not support XMLHTTP");
        }
    }

    function onLoadDone(event)
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200)
            {
                _instance.src = "data:image/jpeg;base64," + encode64(xhr.responseText);
                destroyLoader();
            }
        }
    }

    function onLoadProgress(event)
    {
        var percentageLoaded = 0;
        
        if(event.lengthComputable)
        {
            percentageLoaded = (event.loaded / event.total) * 100;
        }
        
        var evt = new CustomEvent('progress');
        evt.percent = percentageLoaded;
        _instance.dispatchEvent(evt);
        evt = null;
    }

    function destroyLoader()
    {
        xhr.removeEventListener("progress", onLoadProgress, true);
        xhr.removeEventListener("loadend", onLoadDone, true);
        xhr = null;
    }

    function loadImage(url)
    {
        xhr = new XMLHttpRequest();
        xhr.addEventListener("progress", onLoadProgress, true);
        xhr.addEventListener("loadend", onLoadDone, true);
        
        xhr.open("GET", url, true);
        if(xhr.overrideMimeType)
		{
			xhr.overrideMimeType("text/plain; charset=x-user-defined");
		}

        xhr.send(null);
    }
    
    function encode64(inputStr)
    {
        var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        var outputStr = "";
        var i = 0;

        while (i < inputStr.length)
        {
            //all three "& 0xff" added below are there to fix a known bug
            //with bytes returned by xhr.responseText
            var byte1 = inputStr.charCodeAt(i++) & 0xff;
            var byte2 = inputStr.charCodeAt(i++) & 0xff;
            var byte3 = inputStr.charCodeAt(i++) & 0xff;

            var enc1 = byte1 >> 2;
            var enc2 = ((byte1 & 3) << 4) | (byte2 >> 4);

            var enc3, enc4;
            if (isNaN(byte2))
            {
                enc3 = enc4 = 64;
            }
            else
            {
                enc3 = ((byte2 & 15) << 2) | (byte3 >> 6);
                if (isNaN(byte3))
                {
                    enc4 = 64;
                }
                else
                {
                    enc4 = byte3 & 63;
                }
            }

            outputStr += b64.charAt(enc1) + b64.charAt(enc2) + b64.charAt(enc3) + b64.charAt(enc4);
        }

        return outputStr;
    }

    init();

    return _instance;
}