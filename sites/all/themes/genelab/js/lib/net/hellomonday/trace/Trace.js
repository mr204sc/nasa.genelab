/**
 * @fileoverview Provides Traces
 * 	Usage Examples:
 * 		Trace("Information you wish to Trace")
 * 			The information will displayed in the console
 * 		Trace.Trace_ACTIVE = false;   // Turns off all Traces
 * 		Trace.Trace_ON_SCREEN = true; // Shows the Traces on the Traces (good for
 * browsers that do not have a console)
 *
 * @dependencies none
 * @author Kasper, Torben, Anders
 * @browsers: All (however only tested in: IE7+, FF3.6+, Chrome 11+, Safari 5+)
 *
 * Change LOG:
 *  2012 31. MAY:
 * 	Moved Trace to its own function - also stores the Traces that have been made
 * and adds them to a Trace log (can be requested by other functions (for example
 * the BugTracker.js))
 *
 */
function Trace(what, from)
{
    if (Trace.isEnabled)
    {
        var TraceStr = what;
        if (trace._consoleAvailable === false)
        {
            //	TraceStr = getTraceValue(what);
        }

        if (from != null)
        {
            TraceStr = getTraceValue(from) + " => " + TraceStr;
        }

        if (Trace.displayOnScreen)
        {
            if (!Trace._onScreenDiv)
            {
                Trace._onScreenDiv = document.createElement("div");
                Trace._onScreenDiv.style.position = "absolute";
                //Trace._onScreenDiv.style.whiteSpace = "noWrap";
                Trace._onScreenDiv.style.width = "200px";
                Trace._onScreenDiv.style.height = "300px";
                Trace._onScreenDiv.style.overflowY = "scroll";
                Trace._onScreenDiv.style.color = "#00ff00";
                Trace._onScreenDiv.style.padding = "10px";
                Trace._onScreenDiv.style.zIndex = 20000000;
                Trace._onScreenDiv.style.left = "20px";
                Trace._onScreenDiv.style.top = "20px";
                Trace._onScreenDiv.style.fontFamily = "Courier New";
                Trace._onScreenDiv.style.fontSize = "10px";
                Trace._onScreenDiv.style.backgroundColor = "#000000";

                Trace._fieldLines = [];
            }

            Trace._fieldLines.push(TraceStr);
            if (Trace._fieldLines.length > Trace.onScreenMaxLines)
            {
                Trace._fieldLines.shift();
            }

            Trace._onScreenDiv.innerHTML = Trace._fieldLines.join("<br />");

            document.body.appendChild(Trace._onScreenDiv);
        }
        else if (trace._consoleAvailable === true)
        {
            console.log(TraceStr);
        }
        Trace._traceArray.push(TraceStr);
    }
    else
    {
        // Eventhough trace is disabled we still store it in an array - for the bugtracker to be able to get the data
        Trace._traceArray.push(TraceStr);
    }

    function getTraceValue(value)
    {
        var rValue = "";

        try
        {
            if ( typeof value == "Function")
            {
                rValue = value.name.toString() + "();";
            }
            else
            {
                rValue = value.toString();
            }
        }
        catch(error)
        {
            try
            {
                rValue = value.toString();
            }
            catch(e)
            {
                rValue = "null";
            }
        }

        return rValue;
    }

}

Trace.checkConsole = function()
{
    var _consoleAvailable = false;
    if ( typeof console !== "undefined")
    {
        _consoleAvailable = true;
    }
    return _consoleAvailable;
};

var trace = Trace;
// Make it ignore lower case and uppercase (so trace is the same as calling Trace

// public
Trace.isEnabled = true;
// Enabled of disable all Traces
Trace.displayOnScreen = false;
// Show the Traces on the screen
Trace.onScreenMaxLines = 50;
// Maximum number of lines to show "ON SCREEN"

// private
Trace._onScreenDiv = null;
// The ON SCREEN DIV that shows the log
Trace._fieldLines = null;
// Stores the lines that should be used when the Trace is "ON SCREEN"
Trace._traceArray = [];
// Stores ALL logs in an array (no matter if Traces are activ or not)
Trace._consoleAvailable = Trace.checkConsole();
// Stores if the console is available or not
