/**
 * @fileoverview @fileoverview Loads JSON file - supports loading JSON crossdomain
 * 	Usage Examples:
 * 		JSONP.load("http://www.example.com/loadJSON.php", doThisWhenTheDataIsReturned);
 *
 * @author Anders S Jessen
 * @dependencies none
 * @browsers: IE7+, FF3.6+, Chrome 11+, Safari 5+
 */

/**
 * Load JSON crossdomain - static class
 */
var JSONP = new Object();

/**
 * Loads a JSON document - crossdomain support aswell
 * @param {url} the URL to load the JSON document from
 * @param {callbackFunction} function to initialize when the data is loaded
 */
JSONP.load = function(url, callbackFunction)
{
    var uniqueIdentifier = "_" + new Date();
    var script = document.createElement("script");
    var head = document.getElementsByTagName("head")[0] || document.documentElement;

    window[uniqueIdentifier] = function(data)
    {
        head.removeChild(script);
        callbackFunction(data);
        // callbackFunction &&
    };

    // script.src = url.replace('callback=?', 'callback=' + uniqueIdentifier);
    script.src = url + "&callback=" + uniqueIdentifier;
    head.appendChild(script);
};

