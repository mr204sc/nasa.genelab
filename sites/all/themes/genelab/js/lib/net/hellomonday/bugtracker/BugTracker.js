/**
 * @fileoverview The bug tracker automatically reports JS errors.
 *    Usage Examples:
 *       The bug tracker automatically starts and report bugs
 *          To get stacktraces added to the Bug Tracking use:
 *             BugTracker.track(STRING);
 *               Example:
 *                 BugTracker.track("testFunctionName()");
 *
 * The bug tracker can run on its own (fully automatical).
 * However using BugTracker.track will help give more advanced analytics.
 * The BugTracker will also automatically report the lastest 5 trace commands that were made (using trace.js) or last 5 bugtracker.track items
 *
 * The Bugtracker will also push the errors into the Google Analytics as events
 *
 * Hold down SRC (desktop) - 3 finger "swipe down" (tablet / mobile) to take screenshots - need to set Projekt Key to enable this
 *
 * @dependencies  lib/net/hellomonday/system/BrowserDetect.js - optional but provides better code hinting and lib/net/hellomonday/trace/trace.js
 * @author Anders
 * @browsers: All (however only tested in: IE7+, FF3.6+, Chrome 11+, Safari 5+)
 *
 *
 */

function BugTracker()
{
}

/**
 * Bug Tracker settings
 */
// 5b07eff0 Is used for overall testing
BugTracker.bugKey = "5b07eff0";
// Set in a MAX of errors that each visit can send through (in order to avoid possible error spamming)
BugTracker.maxNumberOfErrorsPerSession = 10;
// Current version of the Application / website
BugTracker.appVersion = 0.1;
// Enter the name of the Application / website
BugTracker.appName = "default";
// Disable
BugTracker.isEnabled = true;
// Project Key - it null - no screenshots is possible
BugTracker.projectKey = null;
// mainColor: The color thats used in the screengrab tool
BugTracker.mainColor = "#a5e1bb";
// Post any error to Google Analytics
BugTracker.postErrorToAnalytics = false;

// Private
BugTracker._currentNumberOfErrorsSent = 0;
BugTracker._bugTrackerURL = "https://www.bugsense.com/api/errors";
BugTracker._traceTrackArray = [];
// Snap functionality
BugTracker._screenshotToolLoaded = false;
BugTracker._keyRDown = false;
BugTracker._keyCDown = false;
BugTracker._keySDown = false;

/**
 * Start the bugtrackers.
 */
BugTracker.init = function()
{
    var _previousError = "-";

    var _errorMessage = "None";
    var _errorURL = "None";
    var _errorLineNumber = 0;
    var _errorStackTrace = "";

    var _postToIframe = null;

    BugTracker.returnErrorDetails = returnErrorDetails;

    /**
     * If Google Analytics is implemented we store the error as an event
     */
    function postToAnalytics()
    {
        if (ga == true)
        {
            // _gaq.push(['_trackEvent', category, action, label, value]);
            var getBrowser = BrowserDetect.BROWSER_NAME + " " + BrowserDetect.BROWSER_VERSION;
            var getErrorDetails = "Error: " + _errorMessage + ", File: " + _errorURL + ", Line: " + _errorLineNumber;
           
       
           ga('send', 'event', "HM Bug Tracker", "JS Error", "Browser: " + getBrowser, getErrorDetails);
        }
    }

    /**
     * Gets the error data
     */
    function returnErrorDetails(makeReadable)
    {
        // Normalize the error messages in IE8:
        if (BrowserDetect.BROWSER_NAME == "Explorer" && BrowserDetect.BROWSER_VERSION <= 8)
        {
            var getIsUndefined = _errorMessage.indexOf("is undefined");
            if (getIsUndefined > 0)
            {
                // rename the error to Unncaught ReferenceError: ITEMNAME is not defined
                var getReference = _errorMessage.substr(1, getIsUndefined - 3);
                _errorMessage = "Uncaught ReferenceError: " + getReference + " is not defined";
            }
        }

        var _backTrackBugTracks = "";
        // Loop through the last 5 entries
        var _maxLengthTrackArray = BugTracker._traceTrackArray.length;
        if (_maxLengthTrackArray > 3)
        {
            _maxLengthTrackArray = 3;
        }
        if (BugTracker._traceTrackArray.length > 0)
        {

            for (var q = BugTracker._traceTrackArray.length - _maxLengthTrackArray; q < BugTracker._traceTrackArray.length; q++)
            {
                // Store all trace information
                var getraceStackArrayString = BugTracker._traceTrackArray[q];
                _backTrackBugTracks += getraceStackArrayString;
                if (q < BugTracker._traceTrackArray.length - 1)
                {
                    _backTrackBugTracks += ", ";
                }
            }
        }

        var _backtrackTraces = "";
        if ( typeof trace != "undefined")
        {
            // Loop through the last 5 entries
            var _maxLengthBackTrace = trace._traceArray.length;
            if (_maxLengthBackTrace > 5)
            {
                _maxLengthBackTrace = 5;
            }
            if (trace._traceArray.length > 0)
            {
                for (var k = trace._traceArray.length - _maxLengthBackTrace; k < trace._traceArray.length; k++)
                {
                    // Store all trace information
                    var getStringBackTrace = trace._traceArray[k];
                    if (getStringBackTrace.length > 20)
                    {
                        getStringBackTrace = getStringBackTrace.substr(0, 20);
                    }
                    _backtrackTraces += getStringBackTrace;
                    if (k < trace._traceArray.length - 1)
                    {
                        _backtrackTraces += ", ";
                    }
                }
            }
        }

        // Get user data
        var browserCodeName = navigator.appCodeName;
        var browserName = navigator.appName;
        var browserVersion = navigator.appVersion;
        var browserDeviceType = "Not set";
        // Desktop, Tablet or Smartphone

        if ( typeof BrowserDetect != undefined)
        {
            browserName = BrowserDetect.BROWSER_NAME;
            browserVersion = BrowserDetect.BROWSER_VERSION;
            if (BrowserDetect.TABLET == true)
            {
                browserDeviceType = "Tablet";
            }
            else if (BrowserDetect.MOBILE == true)
            {
                browserDeviceType = "Smartphone";
            }
            else
            {
                browserDeviceType = "Desktop";
            }
        }

        var cookiesisEnabled = navigator.cookieisEnabled;
        var platform = navigator.platform;
        var userAgentHeader = navigator.userAgent;
        // var osVer = navigator.oscpu || "unknown";
        //
        var browserWinWidth = 0;
        var browserWinHeight = 0;
        var getOrientation = window.orientation || "Not set";
        var pageURL = document.location.href;

        // debug data (backtracks)
        var traces = _backtrackTraces;
        var bugtracks = _backTrackBugTracks;
        // max char around 113

        // Get browser window size (cross browser compatible)
        function browserWindowSize()
        {
            if ( typeof (window.innerWidth) == "number")
            {
                //Non-IE
                browserWinWidth = window.innerWidth;
                browserWinHeight = window.innerHeight;
            }
            else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
            {
                //IE 6+ in 'standards compliant mode'
                browserWinWidth = document.documentElement.clientWidth;
                browserWinHeight = document.documentElement.clientHeight;
            }
            else if (document.body && (document.body.clientWidth || document.body.clientHeight))
            {
                //IE 4 compatible
                browserWinWidth = document.body.clientWidth;
                browserWinHeight = document.body.clientHeight;
            }
        }

        browserWindowSize();

        // Gather error details
        var errorDetails_string = '{"client":{"name":"bugsense-js", "version":"1.1"}, ';
        // Request data
        //errorDetails_string += '"request":{"":""}, ';
        errorDetails_string += '"request":{}, ';
        //"remote_ip":"0.0.0.0"
        // Exception data
        var backtrace = _errorStackTrace;
        if (_errorStackTrace == "")
        {
        	backtrace = 'Line number ' + _errorLineNumber + ' - File: ' + _errorURL + ' - BugTracker.track: ' + bugtracks + '", "breadcrumbs":"' + BugTracker._traceTrackArray;
        }
       // console.log("backtrace : " + backtrace)
       // errorDetails_string += '"exception":{"message":"' + _errorMessage + '", "where":"Line number ' + _errorLineNumber + ' in ' + _errorURL + '", "klass":"' + _errorURL + '", "backtrace":"' + backtrace + '"}, ';

        errorDetails_string += '"exception":{"message":"' + _errorMessage + '", "where":"Line number ' + _errorLineNumber + ' in ' + _errorURL + '", "klass":"' + _errorURL + '", "backtrace":"Line number ' + _errorLineNumber + ' - File: ' + _errorURL + ' - BugTracker.track: ' + bugtracks + '", "breadcrumbs":"' + BugTracker._traceTrackArray + '"}, ';
        // Basic data
        errorDetails_string += '"application_environment":{"phone":"' + platform + ' ' + browserName + ' ' + browserVersion + '", "appver":"' + BugTracker.appVersion + '", "appname":"' + BugTracker.appName + '", "osver":"' + browserName + ' ' + browserVersion + '", "user_agent":"' + userAgentHeader + '", "device:type":"' + browserDeviceType + '", "window.innerWidth":"' + browserWinWidth + '", "window.innerHeight":"' + browserWinHeight + '", "screen.width":"' + screen.width + '", "screen.height":"' + screen.height + '", "window.orientation":"' + getOrientation + '", "trace":"' + traces + '", "BugTracker.track":"' + bugtracks + '", "url":"' + pageURL + '", "error.stack":"' + backtrace + '"}}';
        if (makeReadable == true)
        {
            //  errorDetails_string = errorDetails_string.replace("{", "<br>");
            // errorDetails_string = errorDetails_string.replace("}", "<br>");
            /*    errorDetails_string = errorDetails_string.replace(/{/gi, ""); // %0A = new line
             errorDetails_string = errorDetails_string.replace(/}/gi, "");
             errorDetails_string = errorDetails_string.replace(/:/gi, ": ");
             errorDetails_string = errorDetails_string.replace(/,/gi, "");
             errorDetails_string = errorDetails_string.replace(/"/gi, "");*/

            errorDetails_string = "Last error reported (if any): " + _errorMessage + ", Line number " + _errorLineNumber + " in " + _errorURL + "%0A";
            errorDetails_string += "User Agent: " + userAgentHeader + "%0A";
            errorDetails_string += "Device: " + platform + " " + browserName + " " + browserVersion + "%0A";
            errorDetails_string += "Device Type: " + browserDeviceType + "%0A";
            errorDetails_string += "window.innerWidth: " + window.innerWidth + ", window.innerHeight: " + window.innerHeight + ", screen.width: " + screen.width + ", screen.height: " + screen.height + ", window.orientation: " + getOrientation + "%0A";
            errorDetails_string += "BugTracker.track: " + bugtracks + ", Trace (last ones):" + BugTracker._traceTrackArray + "%0A";

            //BugTracker.track: ' + bugtracks + '", "breadcrumbs":"' + BugTracker._traceTrackArray

        }
        return errorDetails_string;
        /*
         Official bugsense SDK
         var data = {
         // information about the bugsense client
         client: {
         // Obligatory
         *    'name'    : 'bugsense-js',
         // Optional
         *   'version' : '1.1'
         },
         // Optional
         // details & custom data about the exception including url, request, response,…
         request: {
         'custom_data' : custom_data
         },
         // basics about the exception
         exception: {
         // Obligatory
         *  'message'     : message,
         *  'where'       : [ url, line ].join( ':' ),
         *  'klass'       : message.split( ':' )[ 0 ],
         *  'backtrace'   : ( typeof(stack) === 'undefined' ) ? message : stack,
         *  'breadcrumbs': this.config.breadcrumbs
         },
         // basic data ( required )
         application_environment: {
         // Obligatory
         *    'phone'              : window.navigator.platform,
         *    'appver'             : ( this.config.appversion || 'unknown' ),
         *    'appname'            : ( this.config.appname || 'unknown' ),
         *    'osver'              : ( typeof window.device !== 'undefined' ) ? window.device.version : s.substr(s.indexOf('; ')+2,s.length).replace(')',';').split(';')[0] || 'unknown' ,
         // Optional
         'connection_type'    : connection_type,
         *    'user_agent'         : window.navigator.userAgent,
         'cordova'            : ( typeof window.device !== 'undefined' ) ? window.device.cordova : 'unknown',
         'device_name'        : ( typeof window.device !== 'undefined' ) ? window.device.name : 'unknown',
         'log_data'           : this.extraData
         }
         };
         */

    }

    function sendData(dataToSent)
    {

        var sendData_escaped = escape(dataToSent);

        var isXDomainRequest = false;
        var xhr;
        if (window.XDomainRequest)
        {

            xhr = new XDomainRequest();
            isXDomainRequest = true;
        }
        else if (window.XMLHttpRequest)
        {
            xhr = new XMLHttpRequest();
        }
        else
        {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var d = new Date();
        var n = d.getTime();

        if (isXDomainRequest == true)
        {
            BugTracker._bugTrackerURL = ('https:' == document.location.protocol ? 'https://www' : 'http://www') + ".bugsense.com/api/errors";
            // xhr.setRequestHeader('X-BugSense-Api-Key', BugTracker.bugKey);
            xhr.open("POST", BugTracker._bugTrackerURL + "?api_key=" + BugTracker.bugKey, true);
            // xhr.onload = function()
            // {
            // trace(this.responseText)
            // }
        }
        else
        {

            xhr.open("POST", BugTracker._bugTrackerURL, false);
            xhr.setRequestHeader('X-BugSense-Api-Key', BugTracker.bugKey);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            // xhr.onreadystatechange = function()
            // {
            // trace(this.responseText)
            // }
        }
        xhr.send("data=" + sendData_escaped);
    }

    /**
     * Automatically detect any JS Errors
     */
    window.onerror = function(msg, url, linenumber, colno, error)
    {
    	//console.log(error)
        if (BugTracker.isEnabled === true)
        {
            // Check to see if it is the same error as we had before - if it is dont send it
            if (msg == _errorMessage && _errorURL == url && _errorLineNumber == linenumber)
            {
               
            }
            else {
            	 // Store the error
                _errorMessage = msg;
                _errorURL = url;
                _errorLineNumber = linenumber;
                if (error != undefined)
                {
                	
                	 _errorStackTrace = String(error.stack);
                	_errorStackTrace = _errorStackTrace.replace(/\n/g, ',');
                //	 console.log(_errorStackTrace, "_errorStackTrace")
                }
               
                if (BugTracker._currentNumberOfErrorsSent < BugTracker.maxNumberOfErrorsPerSession)
                {
                    // Push the error to the bugtracker
                    var getErrorDetails = returnErrorDetails();
                    sendData(getErrorDetails);
                    // Post the error to analytics
                    if (BugTracker.postErrorToAnalytics == true)
                    {
                        postToAnalytics();
                    }
                }
                BugTracker._currentNumberOfErrorsSent++;
            }
        }
        //return true // will stop the error from being displayed
    };
};

/**
 * Track a bugtracker event
 * @param {string} trackString The string that you would like to store in the tracker
 */
BugTracker.track = function(trackString)
{
    BugTracker._traceTrackArray.push(trackString);
};

BugTracker.init();
