/**
 * Generates a random Hex Color
 *
 */

function RandomHexColor()
{
}

RandomHexColor.getColor = function(addHex)
{
    if (addHex === null)
    {
        addHex = "#";
    }
    else if (addHex === true)
    {
        addHex = "#";
    }
    else
    {
        addHex = "";
    }
    
    return addHex + Math.floor(Math.random()*16777215).toString(16);
}
