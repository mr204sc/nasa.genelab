

ArrayManipulation = new Object();

ArrayManipulation.unique = function(targetArray, removeEmpty)
{
	if(removeEmpty == null)
	{
		removeEmpty = true;
	}
	
	var i 			= 0;
	var l 			= targetArray.length;
	var currItem	;
	var rArray		= new Array();
	
	for(i = 0; i < l; i += 1)
	{
		currItem = targetArray[i];
		
		if(rArray.indexOf(currItem) == - 1)
		{
			if(!removeEmpty || currItem != "")
			{
				rArray.push(currItem);
			}
		}
	}
	
	return rArray;	
};



