/**
 * 
 * @author Torben
 * 
 */
(function()
{
	var _instance = {};
	
	_instance.getRandomInt = function(min, max)
	{
		 return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
	/**
	 * @returns {Array} - Array containing ints from range
	 * 
	 * @param {Int} count - How many ints to return. This count cannot exceed the range.
	 * @returns {Int} range - Max range
	 */
	_instance.getRandomIntsFromRange = function(count, range)
	{
		var i = 0;
		var stack = [];
		var randomImages = [];
		
		// Generate stack
		for(i; i < range; i++)
		{
			stack.push(i+1);
		}
		
		// Add random from stack
		i = 0;
		var tempTotal = range-1;
		var randomInt;
		for(i; i < count; i++)
		{
			randomInt = _instance.getRandomInt(0, tempTotal);
			randomImages.push(stack[randomInt]);
			
			stack.splice(randomInt, 1);
			
			tempTotal--;
		}
		
		return randomImages;
	}
	
	_instance.degreesToRadians = function(degrees)
	{
		return degrees * Math.PI / 180;
	}
	
	_instance.radiansToDegrees = function(radians)
	{
		return radians * 180 / Math.PI;
	}
	
	/**
	 * This method will calculate how big a circle is needed to make the square fit in it
	 * 
	 * @param {Number} width
	 * @returns {Number} radius
	 */
	_instance.squareToCircle = function(width)
	{
		var radius;
		
		var a = width;
		var b = width;
		var c = Math.pow(a, 2) + Math.pow(b, 2);
		
		radius = Math.sqrt(c);
		
		return radius;
	}
	
	/**
	 * This method will calculate the largest square that can fit in the circle
	 * 
	 * @param {Number} radius
	 * @returns {Number} width
	 */
	_instance.circleToSquare = function(radius)
	{
		var width;
		
		var c = radius;
		var ab = Math.pow(c, 2);
		var a = ab * .5;
		
		width = Math.sqrt(a);
		
		return width;
	}
	
	/*
	 * Calculate angles and side lengths of a right Triangle
	 */
	_instance.calculateRightTriangle = function(params)
	{
		var result = {};
		
		var a = params.a;
		var b = params.b;
		var c = 90;// params.c;
		var aa = params.aa;
		var bb = params.bb;
		var cc = params.cc;

		var error = 0
		var dummy = 0

		if(a > 0 && b > 0)
		{
			if(a - (-b) > 90)
			{
				throw new Error("MathUtils: The sum of angle A and B is greater than 90º");
				return false;
			}
			if(a - (-b) < 90)
			{
				throw new Error("MathUtils: The sum of angle A and B is less than 90º");
				return false;
			}
		}

		if (cc / aa <= 1 && cc > 0 && aa > 0)
		{
			throw new Error("MathUtils: c has to be bigger than a");
			return false;
		}
		if (cc / bb <= 1 && cc > 0 && bb > 0)
		{
			throw new Error("MathUtils: c has to be bigger than b");
			return false;
		}
		if (aa == 0 && bb == 0 && cc == 0 && dummy2 == 0)
		{
			throw new Error("MathUtils: Specify at least one of the sides a,b or c");
			return false;
		}
		if (aa > 0 && bb > 0 && cc > 0)
		{
			error = 1
			var test = aa * aa + bb * bb - cc * cc
			if (test > 0)
				throw new Error("MathUtils: The side lengths does not meet the requirement a²+b²=c²");
				return false;
			if (test > 0)
				throw new Error("MathUtils: The side lengths does not meet the requirement a²+b²=c²");
				return false;
		}

		if (a == 0)
		{
			if (aa > 0 && cc > 0)
				a = Math.round(Math.asin(aa / cc) * 180 / Math.PI * 10000) / 10000
			if (aa > 0 && bb > 0)
				a = Math.round(Math.atan(aa / bb) * 180 / Math.PI * 10000) / 10000
			if (bb > 0 && cc > 0)
				a = Math.round(Math.acos(bb / cc) * 180 / Math.PI * 10000) / 10000
			if (b > 0)
				a = Math.round((90 - b) * 10000) / 10000
			if (b == 0)
				b = Math.round((90 - a) * 10000) / 10000

			if (aa == 0 && bb > 0)
				aa = Math.round(Math.tan(a * Math.PI / 180) * bb * 10000) / 10000
			if (aa == 0 && cc > 0)
				aa = Math.round(Math.sin(a * Math.PI / 180) * cc * 10000) / 10000
			if (bb == 0 && aa > 0)
				bb = Math.round(aa / Math.tan(a * Math.PI / 180) * 10000) / 10000
			if (bb == 0 && cc > 0)
				bb = Math.round(Math.cos(a * Math.PI / 180) * cc * 10000) / 10000
			if (cc == 0 && bb > 0)
				cc = Math.round(bb / Math.cos(a * Math.PI / 180) * 10000) / 10000
			if (cc == 0 && aa > 0)
				cc = Math.round(aa / Math.sin(a * Math.PI / 180) * 10000) / 10000
		}
		else
		{
			if (b == 0)
			{
				b = Math.round((90 - a) * 10000) / 10000
			}
			if (aa > 0)
			{
				cc = Math.round(aa / Math.sin(a * Math.PI / 180) * 10000) / 10000
				bb = Math.round(aa / Math.tan(a * Math.PI / 180) * 10000) / 10000
				dummy = 1
			}
			if (bb > 0 && dummy == 0)
			{
				aa = Math.round(bb * Math.tan(a * Math.PI / 180) * 10000) / 10000
				cc = Math.round(bb / Math.cos(a * Math.PI / 180) * 10000) / 10000
				dummy = 1
			}
			if (cc > 0 && dummy == 0)
			{
				aa = Math.round(cc * Math.sin(a * Math.PI / 180) * 10000) / 10000
				bb = Math.round(cc * Math.cos(a * Math.PI / 180) * 10000) / 10000
			}
		}
		
		result.a = a;
		result.b = b;
		result.aa = aa;
		result.bb = bb;
		result.cc = cc;

		return result;
	}
	
	window.MathUtils = _instance;
}());
